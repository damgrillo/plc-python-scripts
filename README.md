# plc-python-scripts

In this repository we can find the following folders:

- `scripts/`: the Python and Bash scripts used for calculating the mechanical and structural properties for our simulated systems.
- `data/`: the output data for the different properties obtained as result of running the calculation scripts.
- `plots/`: the relevant plots in PDF for publication.

In the `scripts/` folder we can find: 
- The scripts for calculation of mechanical and structural properties under `scripts/calc/` folder.
- The scripts for generating the plots mentioned above under `scripts/plots/` folder.

In `scripts/calc/` folder, we can find the calculation scripts:

- For bilayers, under `scripts/calc/membranes/` folder:
    - `calc_memb_struct_props.py`: Calculation of structural properties.
        - Membrane area, thickness and volume.
        - PRIP encapsulation stats in lower and upper leaflets.
        - Density profiles.
    - `calc_memb_presprops.py`: Calculation of pressure profiles and moments.
        - Zero moment (`gamma`).
        - First moment (`tau`).
        - Second moment (`gau`).
    - `calc_memb_fourier_spectra.py`: Calculation of Fourier spectra needed for bending modulus.
    - `calc_memb_kbend.py`: Calculation of bending modululs.
    - `calc_memb_elastprops.py`: Calculation of elastic properties.
        - Bending modulus (`kc`) and gaussian modulus (`kg`).
        - Spontaneous curvature for monolayer (`c0m`) and bilayer (`c0b`).
        - Free energy of the bilayer (`eb`) and its derivative respect to mean curvature (`de`).

- For polymersomes, under `scripts/calc/polymersomes/` folder:
    - `calc_psome_struct_props.py`: Calculation of structural properties.
        - Polymersome radius, inner and outer area.
        - Membrane thickness and volume.
        - PRIP encapsulation stats in inner and outer leaflets.
        - Density profiles.
    - `calc_memb_presprops.py`: Calculation of pressure profiles and moments.
        - Pressure moments.
        - Surface of tension (`rs`).
        - Surface tension (`gs`).
        - Surface tension profile (`gamrad`) and bending moment profile (`bendrad`).
    - `calc_psome_spha_spectra.py`: Calculation of spherical harmonics spectra needed for bending modulus.
    - `calc_psome_kbend.py`: Calculation of bending modululs.
    - `calc_psome_elastprops.py`: Calculation of elastic properties.
        - Bending modulus (`kc`) and gaussian modulus (`kg`).
        - Spontaneous curvature (`c0`) and mean curvature of the membrane (`cb`).
        - Free energy of the membrane (`eb`) and its derivative respect to mean curvature (`de`).
