#       Time          mm1           m0           m1           m2           rs      gs_buff  gs_goodrich 
#       [ns]        [bar]     [bar*nm]   [bar*nm^2]   [bar*nm^3]         [nm]     [bar*nm]     [bar*nm] 
     2250.00       5.8426     105.2378    1173.4389   10556.2034      12.1796      71.1606      71.1606 
     2500.00       5.9670     106.4099    1193.3069   10857.7045      12.2084      72.8481      72.8481 
     2750.00       6.9723     108.7577    1212.2101   11122.2109      11.6844      81.4666      81.4666 
     3000.00       3.4790      96.0558    1090.1708    9681.3696      14.0657      48.9347      48.9347 
     3250.00       8.6322     106.1143    1151.9317   10305.6963      10.6084      91.5745      91.5745 
     3500.00       3.4179     100.5160    1145.8761   10245.2724      14.4185      49.2812      49.2812 
