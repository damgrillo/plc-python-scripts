#       Time        tau_low        tau_upp        tau_tot   tau_avg_mlay   tau_avg_mlay 
#       [ns]      [kb*T/nm]      [kb*T/nm]      [kb*T/nm]      [kb*T/nm]   [1e-20 J/nm] 
     2500.00        -6.1097         5.4750        -0.6335         5.7923         2.3992 
     3000.00        -5.7548         5.7974         0.0435         5.7761         2.3924 
     3500.00        -5.5987         5.2768        -0.3205         5.4378         2.2523 
     4000.00        -5.2480         4.7880        -0.4592         5.0180         2.0784 
     4500.00        -4.6842         4.4887        -0.1952         4.5865         1.8997 
     5000.00        -5.0609         5.7640         0.7033         5.4125         2.2418 
