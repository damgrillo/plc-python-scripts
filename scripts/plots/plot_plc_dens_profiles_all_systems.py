import os
import re
import sys
import math
import string
import numpy as np
from scipy import interpolate
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams, patches, lines

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'


####################################################################################
################################### FUNCTIONS ######################################
####################################################################################

# Interpolate data
def interpolate_data(xpos, xdata, ydata):
    finterp = interpolate.interp1d(xdata, ydata, fill_value='extrapolate') 
    return finterp(xpos)
    

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
plotfile  = str(sys.argv[1])
membddir  = str(sys.argv[2])
psomeddir = str(sys.argv[3])
membpdir  = str(sys.argv[4])
psomepdir = str(sys.argv[5])
memblns   = int(sys.argv[6])
psomelns  = int(sys.argv[7])

normalize = False

# Density and pressure files for membranes
membdsuff   = 'densprofs-last-' + str(memblns) + 'ns.txt'
membpsuff   = 'IKcont-REF-last-' + str(memblns) + 'ns.txt'
membdfiles  = [os.path.join(membddir, f) for f in os.listdir(membddir) if f.endswith(membdsuff)]
membpfiles  = [os.path.join(membpdir, f) for f in os.listdir(membpdir) if f.endswith(membpsuff)]

# Density and pressure files for polymersoms
psomedsuff  = 'densprofs-last-' + str(psomelns) + 'ns.txt'
psomepsuff  = 'lpres-last-' + str(psomelns) + 'ns.txt'
psomedfiles = [os.path.join(psomeddir, f) for f in os.listdir(psomeddir) if f.endswith(psomedsuff)]
psomepfiles = [os.path.join(psomepdir, f) for f in os.listdir(psomepdir) if f.endswith(psomepsuff)]

# Get pressure data for membranes
membpprofs = {}
for pf in membpfiles:
    fpres = open(pf, 'r')
    fread = [line.strip() for line in fpres]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    pgrps = {grp:num for num, grp in enumerate(fhead)}
    pprof = np.array(fdata)
    if re.search('PRIN', pf) and re.search('PRIP', pf):
        plabel  = 'N' + re.search("PRIN([0-9]+)", pf).group(1) + '-'
        plabel += 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    elif re.search('PRIN', pf):
        plabel = 'N' + re.search("PRIN([0-9]+)", pf).group(1)
    elif re.search('PRIP', pf):
        plabel = 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    else:
        plabel = 'Neat'
    if normalize:
        pprof[:, 0] /= pprof[-1, 0]
    membpprofs[plabel] = (pgrps, pprof) 
    fpres.close()

# Get density data for membranes
membdprofs = {}
for df in membdfiles:
    fdens = open(df, 'r')
    fread = [line.strip() for line in fdens]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    dgrps = {grp:num for num, grp in enumerate(fhead)}
    dprof = np.array(fdata)
    if re.search('PRIN', df) and re.search('PRIP', df):
        plabel  = 'N' + re.search("PRIN([0-9]+)", df).group(1) + '-'
        plabel += 'P' + re.search("PRIP([0-9]+)", df).group(1)
    elif re.search('PRIN', df):
        plabel += 'N' + re.search("PRIN([0-9]+)", df).group(1)
    elif re.search('PRIP', df):
        plabel += 'P' + re.search("PRIP([0-9]+)", df).group(1)
    else:
        plabel = 'Neat'
    if normalize:
        dprof[:, 0] /= dprof[-1, 0] 
    membdprofs[plabel] = (dgrps, dprof) 
    fdens.close()

# Get pressure data for polymersomes
psomepprofs = {}
for pf in psomepfiles:
    fpres = open(pf, 'r')
    fread = [line.strip() for line in fpres]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    pgrps = {('dPndr' if re.search('dPndr', grp) else grp):num for num, grp in enumerate(fhead)}
    pprof = np.array(fdata)
    if re.search('PRIN', pf) and re.search('PRIP', pf):
        slabel  = 'N' + re.search("PRIN([0-9]+)", pf).group(1) + '-'
        slabel += 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    elif re.search('PRIN', pf):
        slabel = 'N' + re.search("PRIN([0-9]+)", pf).group(1)
    elif re.search('PRIP', pf):
        slabel = 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    else:
        slabel = 'Neat'
    if normalize:
        pprof[:, 0] /= pprof[-1, 0] 
    psomepprofs[slabel] = (pgrps, pprof) 
    fpres.close()

# Get density data for polymersomes
psomedprofs = {}
for df in psomedfiles:
    fdens = open(df, 'r')
    fread = [line.strip() for line in fdens]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    dgrps = {grp:num for num, grp in enumerate(fhead)}
    dprof = np.array(fdata)
    if re.search('PRIN', df) and re.search('PRIP', df):
        slabel  = 'N' + re.search("PRIN([0-9]+)", df).group(1) + '-'
        slabel += 'P' + re.search("PRIP([0-9]+)", df).group(1)
    elif re.search('PRIN', df):
        slabel = 'N' + re.search("PRIN([0-9]+)", df).group(1)
    elif re.search('PRIP', df):
        slabel = 'P' + re.search("PRIP([0-9]+)", df).group(1)
    else:
        slabel = 'Neat'
    if normalize:
        dprof[:, 0] /= dprof[-1, 0]
    psomedprofs[slabel] = (dgrps, dprof) 
    fdens.close()

# Line color by number
numcolor = {0:'r', 1:'b', 2:'g', 3:'k'}

# Line color by density group
dgrpcolor = {'System':'k', 'PEO':'m', 'PBD':'salmon', 'PRIN':'g', 'PRIP':'r', 'CLIO':'b', 'PW':'c'}

# Density group labels
dgrplabel = {'System':'System', 'PLY':'Polymer', 'PEO':'PEO', 'PBD':'PBD', 
             'PW':'Water', 'PRIN':'nPLC', 'PRIP':'pPLC', 'CLIO':'Cl$^-$'}


# Line color by pressure group
pgrpcolor = {'Pt':'limegreen', 'Pn':'olive', 'Pt-Pn':'k', 'dPndr':'darkorange'}

# Pressure group labels
pgrplabel = {'Pt':'$P_T$', 'Pn':'$P_N$', 'Pt-Pn':'$P_T-P_N$', 'dPndr':'$\\frac{r}{2}\\frac{dP_N}{dr}$'}

# Legend properties
locs = {0:'upper left',  1:'upper center', 2:'upper right',
        3:'center left', 4:'center'      , 5:'center right',
        6:'lower left',  7:'lower center', 8:'lower right'}

fsize = {0:'xx-small', 1:'x-small', 2:'small', 3:'medium',
         4:'large',    5:'x-large', 6:'xx-large'}

# Set plot data
ns = -1
nr = -1
lwidth = 2.5
ldash1 = [2.0, 6.0]
ldash2 = [1.0, 4.0]
fig, axes = plt.subplots(nrows=2, ncols=1, sharex=False, sharey='row', squeeze=False)

# Add densities and pressure plots
membnoneat  = [sl for sl in membdprofs.keys() if sl != 'Neat']
psomenoneat = [sl for sl in psomedprofs.keys() if sl != 'Neat']
membsort    = ['Neat'] + sorted(membnoneat, key=lambda x: int(re.search('N([0-9]+)', x).group(1)))
psomesort   = ['Neat'] + sorted(psomenoneat, key=lambda x: int(re.search('N([0-9]+)', x).group(1)))

for systype in ['MEMB', 'PSOME']:
    nr += 1
    if systype == 'MEMB':
        ns = -1
        dgneat, dpneat = membdprofs['Neat']
        pgneat, ppneat = membpprofs['Neat']

        for sl in membsort:
            ns +=  1
            nc  = -1   

            # PRIP
            nc += 1
            grp = 'PRIP'
            dgrps, dprof = membdprofs[sl]
            if grp in dgrps:
                zpos, dens = dprof[:, 0], dprof[:, dgrps[grp]]
            else:
                zpos, dens = dprof[:, 0], dprof[:, 0] * 0.0           
            axes[nr, nc].plot(zpos, dens, c=numcolor[ns], ls='-', lw=lwidth, label=sl)

            # PRIN
            grp = 'PRIN'
            dgrps, dprof = membdprofs[sl]
            if grp in dgrps:
                zpos, dens = dprof[:, 0], dprof[:, dgrps[grp]]
            else:
                zpos, dens = dprof[:, 0], dprof[:, 0] * 0.0           
            axes[nr, nc].plot(zpos, dens, c=numcolor[ns], ls='--', lw=3.0, dashes=ldash1, dash_capstyle='round')

    else:
        ns = -1 
        dgneat, dpneat = psomedprofs['Neat']
        pgneat, ppneat = psomepprofs['Neat']

        for sl in psomesort:
            ns +=  1
            nc  = -1 

            # PRIP
            nc += 1
            grp = 'PRIP'
            dgrps, dprof = psomedprofs[sl]
            if grp in dgrps:
                rpos, dens = dprof[:, 0], dprof[:, dgrps[grp]]
            else:
                rpos, dens = dprof[:, 0], dprof[:, 0] * 0.0      
            axes[nr, nc].plot(rpos, dens, c=numcolor[ns], ls='-', lw=lwidth, label=sl)   

            # PRIN
            grp = 'PRIN'
            dgrps, dprof = psomedprofs[sl]
            if grp in dgrps:
                rpos, dens = dprof[:, 0], dprof[:, dgrps[grp]]
            else:
                rpos, dens = dprof[:, 0], dprof[:, 0] * 0.0      
            axes[nr, nc].plot(rpos, dens, c=numcolor[ns], ls='--', lw=3.0, dashes=ldash1, dash_capstyle='round')

# Set labels, ranges and title
fs = 16
ylabels  = ['$\mathsf{MDP}$ $\mathsf{(kg/m^3)}$']
letters  = {0:{0:'(a)'}, 1:{0:'(b)'}}

for nf in range(2):

    nr = nf % 2
    nc = nf / 2

    # y labels
    if nc == 0: 
        axes[nr, nc].set_ylabel('$\mathsf{MDP}$ $\mathsf{(kg/m^3)}$', labelpad=18, size=24)
        axes[nr, nc].get_yaxis().set_label_coords(-0.13, 0.50, transform=axes[nr, 0].transAxes)

    # y ranges and x labels
    axes[nr, nc].set_ylim(-10, 100)
    axes[nr, nc].set_yticks(np.arange(0, 95, 30)) 
    if nr == 0:
        axes[nr, nc].set_xlabel('$z$ $\\mathsf{(nm)}$', labelpad=10, size=24)
    else:
        axes[nr, nc].set_xlabel('$r$ $\\mathsf{(nm)}$', labelpad=10, size=24)

    # x ranges
    if nr == 0:
        axes[nr, nc].set_xlim(-9.3, 9.3)
        axes[nr, nc].set_xticks(np.arange(-9, 10, 3))
    else:
        axes[nr, nc].set_xlim(1.1, 18.0)
        axes[nr, nc].set_xticks(np.arange(2, 18, 3)) 

    # titles
    if nr == 0:
        #axes[nr, nc].set_title('PLC-loaded bilayers', size=20, va='bottom')
        axes[nr, nc].text(
            0.5, 1.19, 'PLC-loaded bilayers', size=20, 
            ha='center', va='bottom', transform=axes[nr, nc].transAxes
        ) 
    else:
        #axes[nr, nc].set_title('PLC-loaded polymersomes', size=20, va='bottom')  
        axes[nr, nc].text(
            0.5, 1.19, 'PLC-loaded polymersomes', size=20, 
            ha='center', va='bottom', transform=axes[nr, nc].transAxes
        )   

    # letters
    if nr == 0:
        axes[nr, nc].text(
            0.04, 0.95, letters[nr][nc], ha='left', va='top', size=24, 
            transform=axes[nr, nc].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3}
        )
    else:
        axes[nr, nc].text(
            0.04, 0.93, letters[nr][nc], ha='left', va='top', size=24, 
            transform=axes[nr, nc].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3}
        )

    # legends
    bp = (-0.015, 1.0, 1.03, 0.2)
    axes[nr, nc].add_artist(axes[nr, nc].legend(
        loc=locs[6], bbox_to_anchor=bp, fontsize=15, ncol=3, 
        framealpha=1.0, mode='expand', handlelength=2.0, handletextpad=0.3)
    )
    axes[nr, nc].add_artist(
        axes[nr, nc].legend(loc=locs[6], bbox_to_anchor=bp, fontsize=15, ncol=3, 
        framealpha=1.0, mode='expand', handlelength=2.0, handletextpad=0.3)
    )

    ln = []
    lb = ['pPLC', 'nPLC']
    lp = axes[nr, 0].get_lines()
    for i in range(2):
        l = lines.Line2D([0], [0])
        l.update_from(lp[i])
        l.set_color('k')   
        ln.append(l)  

    if nr == 0:
        bp = (0.02, 0.78) #(0.98, 0.95)   
        axes[nr, nc].legend(
            ln, lb, loc=locs[0], bbox_to_anchor=bp, fontsize=15, 
            ncol=1, framealpha=1.0, handlelength=2.0, handletextpad=0.3
        )
    else:
        bp = (0.02, 0.78)   
        axes[nr, nc].legend(
            ln, lb, loc=locs[0], bbox_to_anchor=bp, fontsize=15, 
            ncol=1, framealpha=1.0, handlelength=2.0, handletextpad=0.3
        )

    # margins and grids
    axes[nr, nc].margins(0.05)
    axes[nr, nc].grid()

height = 10
width  = 6.5
fig.set_dpi(200)
fig.set_size_inches(width, height)
fig.tight_layout(h_pad=7.5, w_pad=1.0)
fig.savefig(plotfile, bbox_inches='tight', pad_inches=0.20)


