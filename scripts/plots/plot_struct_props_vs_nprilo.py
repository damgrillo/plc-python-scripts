import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams, patches

rcParams['xtick.labelsize'] = 18
rcParams['ytick.labelsize'] = 18
#rcParams['mathtext.default'] = 'regular'

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
plotfile  = str(sys.argv[1])
membddir  = str(sys.argv[2])
psomeddir = str(sys.argv[3])
membnch   = int(sys.argv[4])
psomench  = int(sys.argv[5])
memblns   = int(sys.argv[6])
psomelns  = int(sys.argv[7])

# Struct props files
membsuff   = 'structprops.txt'
psomesuff  = 'structprops.txt'
membfiles  = [os.path.join(membddir, f) for f in os.listdir(membddir) if f.endswith(membsuff)]
membfiles  = [f for f in membfiles if not re.search('large', f)]
membfiles  = [f for f in membfiles if not re.search('PRIN27', f) or re.search('newrun', f)]
psomefiles = [os.path.join(psomeddir, f) for f in os.listdir(psomeddir) if f.endswith(psomesuff)]

# Get struct props
areaprin = {'MEMB':[]}
ainnprin = {'PSOME':[]}
aoutprin = {'PSOME':[]}
mthkprin = {'MEMB':[], 'PSOME':[]}
mvolprin = {'MEMB':[], 'PSOME':[]}
strfiles = {'MEMB':membfiles, 'PSOME':psomefiles}
lastns   = {'MEMB':memblns, 'PSOME':psomelns}
nchains  = {'MEMB':membnch, 'PSOME':psomench}
slabel   = {'MEMB':'Bilayer', 'PSOME':'Polymersome'}

for stype in ['MEMB', 'PSOME']:
    for txt in strfiles[stype]:

        # Read data and extract last nanosecs
        ftxt  = open(txt, 'r')
        fread = [line.strip() for line in ftxt]
        fhead = fread[0].strip('#').split()
        ncols = {grp:num for num, grp in enumerate(fhead)}
        fdata = np.array([map(float, line.split()) for line in fread if not re.match('#|@', line.strip())])
        fdata = fdata[-lastns[stype]:, :] 

        # Read priloc concentration
        nprin = 0
        nprip = 0
        if re.search('PRIN([0-9]+)', txt):
            nprin = int(re.search('PRIN([0-9]+)', txt).group(1))
        if re.search('PRIP([0-9]+)', txt):
            nprip = int(re.search('PRIP([0-9]+)', txt).group(1))
        nplc = nprin + nprip

        # Extract properties
        if stype == 'MEMB':
            area = fdata[:, ncols['MembArea']]
            mthk = fdata[:, ncols['MembThick']]
            mvol = fdata[:, ncols['MembVol']]
            areaprin[stype].append((nplc, nprin, np.mean(area), np.std(area)))
            mthkprin[stype].append((nplc, nprin, np.mean(mthk), np.std(mthk)))
            mvolprin[stype].append((nplc, nprin, np.mean(mvol), np.std(mvol)))
        else:
            ainn = fdata[:, ncols['Ainn']]
            aout = fdata[:, ncols['Aout']]
            mthk = fdata[:, ncols['MembThick']]
            mvol = fdata[:, ncols['MembVol']]
            ainnprin[stype].append((nplc, nprin, np.mean(ainn), np.std(ainn)))
            aoutprin[stype].append((nplc, nprin, np.mean(aout), np.std(aout)))
            mthkprin[stype].append((nplc, nprin, np.mean(mthk), np.std(mthk)))
            mvolprin[stype].append((nplc, nprin, np.mean(mvol), np.std(mvol)))

    # Convert to arrays
    if stype == 'MEMB':
        areaprin[stype] = np.array(areaprin[stype])
        areaprin[stype] = areaprin[stype][np.argsort(areaprin[stype][:, 0])]
    else:
        ainnprin[stype] = np.array(ainnprin[stype])
        aoutprin[stype] = np.array(aoutprin[stype])
        ainnprin[stype] = ainnprin[stype][np.argsort(ainnprin[stype][:, 0])]
        aoutprin[stype] = aoutprin[stype][np.argsort(aoutprin[stype][:, 0])]
    mthkprin[stype] = np.array(mthkprin[stype])
    mvolprin[stype] = np.array(mvolprin[stype])


# Line properties
cls = {0:'r', 1:'b', 2:'g', 3:'y', 4:'k', 5:'m', 6:'c'}
mks = {0:'s', 1:'o', 2:'^', 3:'D', 4:'^', 5:'h'}
lst = {0:'-', 1:':', 2:'--', 3:'-.'}
fst = {0:'full', 1:'left'}

# Legend properties
locs = {0:'upper left',  1:'upper center', 2:'upper right',
        3:'center left', 4:'center'      , 5:'center right',
        6:'lower left',  7:'lower center', 8:'lower right'}

fsize = {0:'xx-small', 1:'x-small', 2:'small', 3:'medium',
         4:'large',    5:'x-large', 6:'xx-large'}

######################## Using stacked plots ##################################
# Add plots
lwidth = 2
fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=False, squeeze=False)

# Area
nc = -1
scale  = 100
mksize = 6
for stype in ['MEMB', 'PSOME']:
    nch  = nchains[stype]
    if stype == 'MEMB':
        nc  += 1
        arr  = areaprin[stype]
        pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
        avg, std  = avg/avg[0]-1, std/avg[0]
        axes[0, 0].errorbar(
            pn/nch, avg*scale, std*scale, c=cls[0], marker=mks[0], 
            ls='', ms=mksize, lw=lwidth, label=slabel[stype]
        )
    else:
        nc  += 1
        arr  = ainnprin[stype]
        pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
        avg, std  = avg/avg[0]-1, std/avg[0]
        axes[0, 0].errorbar(
            pn/nch, avg*scale, std*scale, c=cls[2], marker=mks[2], 
            ls='', ms=mksize+1, lw=lwidth, label='Polymersome (Inner Surf.)'
        )
        nc  += 1
        arr  = aoutprin[stype]
        pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
        avg, std  = avg/avg[0]-1, std/avg[0]
        axes[0, 0].errorbar(
            pn/nch, avg*scale, std*scale, c=cls[3], marker=mks[3], 
            ls='', ms=mksize, lw=lwidth, label='Polymersome (Outer Surf.)'
        )

# Membrane thickness
nc = -1
mksize = 6
for stype in ['MEMB', 'PSOME']:
    nc  += 1
    nch  = nchains[stype]
    arr  = mthkprin[stype]
    pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
    axes[1, 0].errorbar(
        pn/nch, avg, std, c=cls[nc], marker=mks[nc], 
        ls='', ms=mksize, lw=lwidth, label=slabel[stype]
    )

# Membrane volume per chain
nc = -1
mksize = 6
for stype in ['MEMB', 'PSOME']:
    nc  += 1
    nch  = nchains[stype]
    arr  = mvolprin[stype]
    pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
    axes[2, 0].errorbar(
        pn/nch, avg/nch, std/nch, c=cls[nc], marker=mks[nc], 
        ls='', ms=mksize, lw=lwidth, label=slabel[stype]
    )

# Calculate delta volume per PLC molecule using theoretical density and MW 
mwpr = 220.31               # MW(prilo) in Dalton
dspr = 1.03                 # Dens(prilo) in g/cm^3
fd2g = 1.66053e-24          # Dalton to gram
fc2m = 1e21                 # cm^3 to nm^3
dvpp = 220.31 * fd2g / dspr * fc2m

# Calculate theoretical membrane volume expansion due to nPLC addition
mvmemb  = mvolprin['MEMB']/nchains['MEMB']
mvpsome = mvolprin['PSOME']/nchains['PSOME']
mv_all  = np.concatenate((mvmemb, mvpsome))
mv0_all = np.mean(mv_all[mv_all[:, 1] < 1e-5][:, 2])
pn_teo  = np.linspace(-0.2, 1.0, 500)
vm_teo  = dvpp * pn_teo * (1.0/2.6) + mv0_all 
axes[2, 0].plot(pn_teo, vm_teo, c=cls[2], ls='-', lw=lwidth, zorder=0)

# Set plot legend and save figure
fs = 14
ll = locs[5]
ylabels = ['$\Delta A/A_0$ $\mathsf{(\%)}$', '$d$ $\mathsf{(nm)}$', '$V_{hc}$ $\mathsf{(nm^3)}$']
for nr in range(3):

    # Ranges and text labels
    if nr == 0:
        #axes[nr,0].set_ylim(0.79, 0.98)
        #axes[nr,0].set_yticks(np.arange(0.81, 0.98, 0.05))
        #axes[nr,0].set_ylim(0.76, 1.14)
        #axes[nr,0].set_yticks(np.arange(0.80, 1.11, 0.10))
        axes[nr,0].text(
            0.95, 0.92, '(a)', ha='right', va='top', size=21, 
            transform=axes[nr,0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':4}
        )
    elif nr == 1:
        axes[nr,0].set_ylim(4.97, 6.2)
        axes[nr,0].set_yticks(np.arange(5.1, 6.2, 0.3))
        axes[nr,0].text(
            0.95, 0.88, '(b)', ha='right', va='top', size=21, 
            transform=axes[nr,0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':4}
        )
    elif nr == 2:
        axes[nr,0].set_xlim(-0.1, 0.93)
        axes[nr,0].set_ylim(2.44, 2.61)
        axes[nr,0].set_xticks(np.arange(0, 0.9, 0.2))
        axes[nr,0].set_yticks(np.arange(2.46, 2.61, 0.04))
        axes[nr,0].set_xlabel('$N_\mathrm{PLC} / N_{c}$', size=20, labelpad=10)
        axes[nr,0].text(
            0.95, 0.08, '(c)', ha='right', va='bottom', size=21, 
            transform=axes[nr,0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':4}
        )

    axes[nr,0].set_ylabel(ylabels[nr], size=20, labelpad=10)
    axes[nr,0].margins(0.03)
    axes[nr,0].grid()
    axes[nr,0].get_yaxis().set_label_coords(-0.22, 0.50, transform=axes[nr,0].transAxes)

# Handles and labels
hndlist  = []
lablist  = []
hl1, ll1 = axes[1, 0].get_legend_handles_labels()
hl2, ll2 = axes[0, 0].get_legend_handles_labels()
hl2, ll2 = zip(*[[hl2[i], ll2[i]] for i in range(len(hl2)) if ll2[i] != 'Bilayer'])

# Legend
bp = (-0.025, 1.0, 1.05, 0.2)
axes[0, 0].legend(
    hl1+list(hl2), ll1+list(ll2), loc=locs[6], bbox_to_anchor=bp, fontsize=14, 
    numpoints=1, ncol=1, framealpha=1.0, mode='expand', handlelength=1.5, handletextpad=0.5
)

fig.set_dpi(200)
fig.set_size_inches(5.5, 9)
fig.tight_layout(h_pad=0.0)
fig.savefig(plotfile, bbox_inches='tight', pad_inches=0.20) 

