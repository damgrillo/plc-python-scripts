#!/bin/bash

# Working dir, should point to plc_polymersomes dir
WORKDIR=$(cd $(dirname $0)/../.. && pwd)

# Scripts and plot dirs
SCRIPTSDIR=$WORKDIR/scripts/plots
PLOTDIR=$WORKDIR/plots

# Membrane/polymersomes data dirs
MEMB_DATA_DIR=$WORKDIR/data/membranes
PSOME_DATA_DIR=$WORKDIR/data/polymersomes

# Pressure profiles dirs
MEMB_PRES_PROFILES=$MEMB_DATA_DIR/pressure_profiles
PSOME_PRES_PROFILES=$PSOME_DATA_DIR/pressure_profiles

# Structural properties dirs
MEMB_STRUCT_PROPS_DIR=$MEMB_DATA_DIR/structural_properties
PSOME_STRUCT_PROPS_DIR=$PSOME_DATA_DIR/structural_properties

# Mechanical properties dirs
MEMB_MECHANIC_PROPS_DIR=$MEMB_DATA_DIR/mechanical_properties
PSOME_MECHANIC_PROPS_DIR=$PSOME_DATA_DIR/mechanical_properties

# Plots to be computed
PLOTS_TO_COMPUTE=( 
    "PLOT_DENSPRES_NEAT_SYSTEMS"
    "PLOT_PLC_DENS_ALL_SYSTEMS"
    "PLOT_PRIP_PARTITION"
    "PLOT_DENSPRES_ALL_SYSTEMS"
    "PLOT_STRUCT_PROPS_VS_NPRILO"
    "PLOT_ELAST_PROPS_VS_NPRILO"
)

# Set figure width for one and two column format, in inches
ONECOLWIDTH=3.37
TWOCOLWIDTH=6.69

# Number of polymer chains for each system type
NUM_CHAINS_MEMB=200
NUM_CHAINS_PSOME=3141

# Last ns considered for measuring properties for each system type
LASTNS_PROPS_MEMB=3000
LASTNS_PROPS_PSOME=1500

########################################################################################
# PLOTS
########################################################################################

# Plot density and pressure profiles for neat systems
PLOT="PLOT_DENSPRES_NEAT_SYSTEMS"
if [[ "${PLOTS_TO_COMPUTE[*]}" =~ "${PLOT}" ]]; then
    PLOTFILE=$PLOTDIR/PRILO-denspres-neat-systems.pdf
    /usr/bin/python $SCRIPTSDIR/plot_denspres_profiles_neat_systems.py \
        $PLOTFILE \
        $MEMB_STRUCT_PROPS_DIR $PSOME_STRUCT_PROPS_DIR \
        $MEMB_PRES_PROFILES $PSOME_PRES_PROFILES \
        $LASTNS_PROPS_MEMB $LASTNS_PROPS_PSOME

    # # Rescale pdf size for publication
    # PDFWIDTH=$TWOCOLWIDTH
    # PDFHWRAT=$(identify -verbose $PLOTFILE | grep 'Print size' | tr 'x' ' ' | awk '{printf "%.4f", $4/$3}')
    # PDFHEIGH=$(echo "$PDFHWRAT * $PDFWIDTH" | bc -l | awk '{printf "%.2f", $0}')
    # pdfjam --papersize "{${PDFWIDTH}in,${PDFHEIGH}in}" --outfile $PLOTFILE $PLOTFILE
fi

# Plot PRIN/PRIP density profiles for all systems
PLOT="PLOT_PLC_DENS_ALL_SYSTEMS"
if [[ "${PLOTS_TO_COMPUTE[*]}" =~ "${PLOT}" ]]; then
    PLOTFILE=$PLOTDIR/PRILO-plc-dens-profiles-all-systems.pdf
    /usr/bin/python $SCRIPTSDIR/plot_plc_dens_profiles_all_systems.py \
        $PLOTFILE \
        $MEMB_STRUCT_PROPS_DIR $PSOME_STRUCT_PROPS_DIR \
        $MEMB_PRES_PROFILES $PSOME_PRES_PROFILES \
        $LASTNS_PROPS_MEMB $LASTNS_PROPS_PSOME

    # # Rescale pdf size for publication
    # PDFWIDTH=$ONECOLWIDTH
    # PDFHWRAT=$(identify -verbose $PLOTFILE | grep 'Print size' | tr 'x' ' ' | awk '{printf "%.4f", $4/$3}')
    # PDFHEIGH=$(echo "$PDFHWRAT * $PDFWIDTH" | bc -l | awk '{printf "%.2f", $0}')
    # pdfjam --papersize "{${PDFWIDTH}in,${PDFHEIGH}in}" --outfile $PLOTFILE $PLOTFILE
fi

# Plot PRIN/PRIP partition
PLOT="PLOT_PRIP_PARTITION"
if [[ "${PLOTS_TO_COMPUTE[*]}" =~ "${PLOT}" ]]; then
    PLOTFILE=$PLOTDIR/PRILO-prip-partition.pdf
    MEMB_DENSPROF_FILE=$MEMB_STRUCT_PROPS_DIR/PBD22-PEO14-PRIN54-PRIP86-densprofs-last-${LASTNS_PROPS_MEMB}ns.txt
    PSOME_DENSPROF_FILE=$PSOME_STRUCT_PROPS_DIR/PBD22-PEO14-PRIN1000-PRIP1600-densprofs-last-${LASTNS_PROPS_PSOME}ns.txt
    /usr/bin/python $SCRIPTSDIR/plot_prip_partition.py $PLOTFILE $MEMB_DENSPROF_FILE $PSOME_DENSPROF_FILE

    # # Rescale pdf size
    # PDFWIDTH=$TWOCOLWIDTH
    # PDFHWRAT=$(identify -verbose $PLOTFILE | grep 'Print size' | tr 'x' ' ' | awk '{printf "%.4f", $4/$3}')
    # PDFHEIGH=$(echo "$PDFHWRAT * $PDFWIDTH" | bc -l | awk '{printf "%.2f", $0}')
    # pdfjam --papersize "{${PDFWIDTH}in,${PDFHEIGH}in}" --outfile $PLOTFILE $PLOTFILE
fi

# Plot density and pressure profiles for all systems
PLOT="PLOT_DENSPRES_ALL_SYSTEMS"
if [[ "${PLOTS_TO_COMPUTE[*]}" =~ "${PLOT}" ]]; then
    PLOTFILE=$PLOTDIR/PRILO-denspres-all-systems.pdf
    /usr/bin/python $SCRIPTSDIR/plot_denspres_profiles_all_systems.py \
        $PLOTFILE \
        $MEMB_STRUCT_PROPS_DIR $PSOME_STRUCT_PROPS_DIR \
        $MEMB_PRES_PROFILES $PSOME_PRES_PROFILES \
        $LASTNS_PROPS_MEMB $LASTNS_PROPS_PSOME

    # # Rescale pdf size
    # PDFWIDTH=$TWOCOLWIDTH
    # PDFHWRAT=$(identify -verbose $PLOTFILE | grep 'Print size' | tr 'x' ' ' | awk '{printf "%.4f", $4/$3}')
    # PDFHEIGH=$(echo "$PDFHWRAT * $PDFWIDTH" | bc -l | awk '{printf "%.2f", $0}')
    # pdfjam --papersize "{${PDFWIDTH}in,${PDFHEIGH}in}" --outfile $PLOTFILE $PLOTFILE

fi

# Plot structural properties vs number of prilo molecules (normalized by number of polymer chain)
PLOT="PLOT_STRUCT_PROPS_VS_NPRILO"
if [[ "${PLOTS_TO_COMPUTE[*]}" =~ "${PLOT}" ]]; then
    PLOTFILE=$PLOTDIR/PRILO-structprops-vs-nprilo.pdf
    /usr/bin/python $SCRIPTSDIR/plot_struct_props_vs_nprilo.py \
        $PLOTFILE \
        $MEMB_STRUCT_PROPS_DIR $PSOME_STRUCT_PROPS_DIR \
        $NUM_CHAINS_MEMB $NUM_CHAINS_PSOME \
        $LASTNS_PROPS_MEMB $LASTNS_PROPS_PSOME

    # # Rescale pdf size
    # PDFWIDTH=$ONECOLWIDTH
    # PDFHWRAT=$(identify -verbose $PLOTFILE | grep 'Print size' | tr 'x' ' ' | awk '{printf "%.4f", $4/$3}')
    # PDFHEIGH=$(echo "$PDFHWRAT * $PDFWIDTH" | bc -l | awk '{printf "%.2f", $0}')
    # #pdfjam --papersize "{${PDFWIDTH}in,${PDFHEIGH}in}" --outfile $PLOTFILE $PLOTFILE
fi

# Plot elastic properties vs number of prilo molecules (normalized by number of polymer chain)
PLOT="PLOT_ELAST_PROPS_VS_NPRILO"
if [[ "${PLOTS_TO_COMPUTE[*]}" =~ "${PLOT}" ]]; then
    PLOTFILE=$PLOTDIR/PRILO-elastprops-vs-nprilo.pdf
    MEMB_ELASTPROPS_FILE=$MEMB_MECHANIC_PROPS_DIR/PRILO-elastprops-avgwin-500ns.txt
    PSOME_DENSPROF_FILE=$PSOME_MECHANIC_PROPS_DIR/PRILO-elastprops-avgwin-250ns.txt
    /usr/bin/python $SCRIPTSDIR/plot_elast_props_vs_nprilo.py \
        $PLOTFILE \
        $MEMB_ELASTPROPS_FILE $PSOME_DENSPROF_FILE \
        $NUM_CHAINS_MEMB $NUM_CHAINS_PSOME

    # # Rescale pdf size
    # PDFWIDTH=$ONECOLWIDTH
    # PDFHWRAT=$(identify -verbose $PLOTFILE | grep 'Print size' | tr 'x' ' ' | awk '{printf "%.4f", $4/$3}')
    # PDFHEIGH=$(echo "$PDFHWRAT * $PDFWIDTH" | bc -l | awk '{printf "%.2f", $0}')
    # pdfjam --papersize "{${PDFWIDTH}in,${PDFHEIGH}in}" --outfile $PLOTFILE $PLOTFILE
fi
