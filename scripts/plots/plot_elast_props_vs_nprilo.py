import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams, patches

rcParams['xtick.labelsize'] = 18
rcParams['ytick.labelsize'] = 18
#rcParams['mathtext.default'] = 'regular'

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
plotfile    = str(sys.argv[1])
membelastf  = str(sys.argv[2])
psomeelastf = str(sys.argv[3])
membnch     = int(sys.argv[4])
psomench    = int(sys.argv[5])

# Mechanic props files
elastfile   = {'MLAY':membelastf, 'BLAY':membelastf, 'PSOME':psomeelastf}
nchains     = {'MLAY':membnch, 'BLAY':membnch, 'PSOME':psomench}

# Get elastic props
sname  = {'MLAY':[], 'BLAY':[], 'PSOME':[]}
denc   = {'MLAY':[], 'BLAY':[], 'PSOME':[]}
c0     = {'MLAY':[], 'BLAY':[], 'PSOME':[]}
en     = {'MLAY':[], 'BLAY':[], 'PSOME':[]}

# Prop labels
dlabel = {'MLAY':'upp_enc', 'BLAY':'diff_enc', 'PSOME':'diff_enc'}
clabel = {'MLAY':'c0m', 'BLAY':'c0b', 'PSOME':'c0'}
elabel = {'MLAY':'em', 'BLAY':'eb', 'PSOME':'eb'}
slabel = {'MLAY':'Monolayer', 'BLAY':'Bilayer', 'PSOME':'Polymersome'}

for stype in ['MLAY', 'BLAY', 'PSOME']:

    # Read data from file
    ftxt  = open(elastfile[stype], 'r')
    fread = [line.strip() for line in ftxt]
    fhead = fread[0].strip('#').split()
    ncols = {grp:num for num, grp in enumerate(fhead)}
    fdata = [line.split() for line in fread if not re.match('#|@', line)]

    # Get properties
    for row in fdata:
        nprin = 0
        nprip = 0
        if re.search('N([0-9]+)', row[0]):
            nprin = int(re.search('N([0-9]+)', row[0]).group(1))
        if re.search('P([0-9]+)', row[0]):
            nprip = int(re.search('P([0-9]+)', row[0]).group(1))
        if nprin != 27 or re.search('NEW', row[0]): 
            nplc       = nprin + nprip
            denc_stat  = map(float, row[ncols[dlabel[stype]]].split('/'))
            c0_stat    = map(float, row[ncols[clabel[stype]]].split('/'))
            en_stat    = map(float, row[ncols[elabel[stype]]].split('/'))
            denc[stype].append((nplc, nprip, denc_stat[0], denc_stat[1]))
            c0[stype].append((nplc, nprip, c0_stat[0], c0_stat[1]))
            en[stype].append((nplc, nprip, en_stat[0], en_stat[1]))
            sname[stype].append(('Neat' if nplc == 0 else 'N{0:d}\nP{1:d}'.format(nprin, nprip)))  

    # Convert to arrays
    denc[stype]  = np.array(denc[stype])
    c0[stype]    = np.array(c0[stype])
    en[stype]    = np.array(en[stype])


# Line properties
cls = {0:'r', 1:'b', 2:'g', 3:'k', 4:'m', 5:'y', 6:'c'}
mks = {0:'o', 1:'^', 2:'^', 3:'*', 4:'d', 5:'h'}
lst = {0:'-', 1:':', 2:'--', 3:'-.'}
fst = {0:'full', 1:'left'}

# Legend properties
locs = {0:'upper left',  1:'upper center', 2:'upper right',
        3:'center left', 4:'center'      , 5:'center right',
        6:'lower left',  7:'lower center', 8:'lower right'}

fsize = {0:'xx-small', 1:'x-small', 2:'small', 3:'medium',
         4:'large',    5:'x-large', 6:'xx-large'}

######################## Using stacked plots ##################################
# Add plots
lwidth = 2
fig, axes = plt.subplots(nrows=3, ncols=1, sharex='col', sharey=False, squeeze=False)

# Encapsulation difference
nr = 0 
nc = 0
mk = 6
for stype in ['BLAY', 'PSOME']:
    nch  = nchains[stype]
    arr  = denc[stype]
    pn, avg, std  = arr[:, 0], arr[:, 2], arr[:, 3]
    axes[nr, 0].errorbar(pn/nch, avg, std, c=cls[nc], marker=mks[nc], ls='', ms=mk, lw=lwidth, label=slabel[stype], zorder=3-nc)
    nc += 1
    mk += 2

# Spontaneous curvature
nr += 1
nc  = 0
mk  = 6
for stype in ['BLAY', 'PSOME']:
    nch  = nchains[stype]
    arr  = c0[stype]
    pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
    axes[nr, 0].errorbar(pn/nch, avg, std, c=cls[nc], marker=mks[nc], ls='', ms=mk, lw=lwidth, label=slabel[stype])
    nc += 1
    mk += 2

# Bending energy
nr += 1
nc  = 0
mk  = 6
for stype in ['BLAY', 'PSOME']:
    nch  = nchains[stype]
    arr  = en[stype]
    pn, avg, std = arr[:, 0], arr[:, 2], arr[:, 3]
    axes[nr, 0].errorbar(pn/nch, avg, std, c=cls[nc], marker=mks[nc], ls='', ms=mk, lw=lwidth, label=slabel[stype])
    nc += 1
    mk += 2

# Set plot legend and save figure
fs = 14
ll = locs[5]

ymlabels = ['$\Delta \Gamma$ $\mathsf{(nm^{-2})}$', 
            '$H_{0}$ $\mathsf{(nm^{-1})}$', 
            '$f$ $\mathsf{(k_{B}T/nm^2)}$  ']

yplabels = ['$\Delta \Gamma$ $\mathsf{(nm^{-2})}$', 
            '$H_{0}$ $\mathsf{(nm^{-1})}$', 
            '$f$ $\mathsf{(k_{B}T/nm^2)}$  ']

for nr in range(3):

    # Ranges and text labels
    if nr == 0:
        axes[nr, 0].set_ylim(-0.045, 0.22)
        axes[nr, 0].set_yticks(np.arange(0.0, 0.21, 0.05))
        axes[nr,0].text(0.95, 0.08, '(a)', ha='right', va='bottom', size=21, 
                      transform=axes[nr,0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':4})

    elif nr == 1:
        axes[nr, 0].set_ylim(-0.12, 0.77)
        axes[nr, 0].set_yticks(np.arange(0, 0.7, 0.2))
        axes[nr,0].text(0.95, 0.88, '(b)', ha='right', va='top', size=21, 
                      transform=axes[nr,0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':4})

    elif nr == 2:
        axes[nr,0].set_xlim(-0.1, 0.93)
        axes[nr, 0].set_ylim(-5, 51)
        axes[nr,0].set_xticks(np.arange(0, 0.9, 0.2))
        axes[nr, 0].set_yticks(np.arange(0, 50, 15))
        axes[nr, 0].set_xlabel('$N_\mathrm{PLC} / N_{c}$', size=20, labelpad=10)
        axes[nr,0].text(0.95, 0.92, '(c)', ha='right', va='top', size=21, 
                      transform=axes[nr,0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':4})

    # Legend
    if nr == 0:
        bp = (-0.025, 1.0, 1.05, 0.2)
        axes[nr, 0].legend(loc=locs[6], bbox_to_anchor=bp, fontsize=14, numpoints=1, ncol=3, 
                           framealpha=1.0, mode='expand', handlelength=1.5, handletextpad=0.5)

    # Labels and margins
    axes[nr, 0].set_ylabel(ymlabels[nr], size=20, labelpad=10)
    axes[nr, 0].get_yaxis().set_label_coords(-0.22, 0.50, transform=axes[nr, 0].transAxes)
    axes[nr, 0].margins(0.03)
    axes[nr, 0].grid()

fig.set_dpi(200)
fig.set_size_inches(5.5, 9)
fig.tight_layout(h_pad=0.0)
fig.savefig(plotfile, bbox_inches='tight', pad_inches=0.20) 

