import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams, patches

rcParams['xtick.labelsize'] = 19
rcParams['ytick.labelsize'] = 19
#rcParams['mathtext.default'] = 'regular'

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
plotfile  = str(sys.argv[1])
membddir  = str(sys.argv[2])
psomeddir = str(sys.argv[3])
membpdir  = str(sys.argv[4])
psomepdir = str(sys.argv[5])
memblns   = int(sys.argv[6])
psomelns  = int(sys.argv[7])

# Density and pressure files for membranes
membdsuff   = 'densprofs-last-' + str(memblns) + 'ns.txt'
membpsuff   = 'IKcont-REF-last-' + str(memblns) + 'ns.txt'
membdfiles  = [os.path.join(membddir, f) for f in os.listdir(membddir) if f.endswith(membdsuff)]
membpfiles  = [os.path.join(membpdir, f) for f in os.listdir(membpdir) if f.endswith(membpsuff)]

# Density and pressure files for polymersoms
psomedsuff  = 'densprofs-last-' + str(psomelns) + 'ns.txt'
psomepsuff  = 'lpres-last-' + str(psomelns) + 'ns.txt'
psomedfiles = [os.path.join(psomeddir, f) for f in os.listdir(psomeddir) if f.endswith(psomedsuff)]
psomepfiles = [os.path.join(psomepdir, f) for f in os.listdir(psomepdir) if f.endswith(psomepsuff)]

# Get density data for membranes
membdprofs = {}
for df in membdfiles:
    fdens = open(df, 'r')
    fread = [line.strip() for line in fdens]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    dgrps = {grp:num for num, grp in enumerate(fhead)}
    dprof = np.array(fdata)
    if re.search('PRIN', df) and re.search('PRIP', df):
        plabel  = 'N' + re.search("PRIN([0-9]+)", df).group(1) + '-'
        plabel += 'P' + re.search("PRIP([0-9]+)", df).group(1)
    elif re.search('PRIN', df):
        plabel += 'N' + re.search("PRIN([0-9]+)", df).group(1)
    elif re.search('PRIP', df):
        plabel += 'P' + re.search("PRIP([0-9]+)", df).group(1)
    else:
        plabel = 'Neat'
    membdprofs[plabel] = (dgrps, dprof) 
    fdens.close()

# Get pressure data for membranes
membpprofs = {}
for pf in membpfiles:
    fpres = open(pf, 'r')
    fread = [line.strip() for line in fpres]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    pgrps = {grp:num for num, grp in enumerate(fhead)}
    pprof = np.array(fdata)
    if re.search('PRIN', pf) and re.search('PRIP', pf):
        plabel  = 'N' + re.search("PRIN([0-9]+)", pf).group(1) + '-'
        plabel += 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    elif re.search('PRIN', pf):
        plabel = 'N' + re.search("PRIN([0-9]+)", pf).group(1)
    elif re.search('PRIP', pf):
        plabel = 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    else:
        plabel = 'Neat'
    membpprofs[plabel] = (pgrps, pprof) 
    fpres.close()

# Get density data for polymersomes
psomedprofs = {}
for df in psomedfiles:
    fdens = open(df, 'r')
    fread = [line.strip() for line in fdens]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    dgrps = {grp:num for num, grp in enumerate(fhead)}
    dprof = np.array(fdata)
    if re.search('PRIN', df) and re.search('PRIP', df):
        slabel  = 'N' + re.search("PRIN([0-9]+)", df).group(1) + '-'
        slabel += 'P' + re.search("PRIP([0-9]+)", df).group(1)
    elif re.search('PRIN', df):
        slabel = 'N' + re.search("PRIN([0-9]+)", df).group(1)
    elif re.search('PRIP', df):
        slabel = 'P' + re.search("PRIP([0-9]+)", df).group(1)
    else:
        slabel = 'Neat'
    psomedprofs[slabel] = (dgrps, dprof) 
    fdens.close()

# Get pressure data for polymersomes
psomepprofs = {}
for pf in psomepfiles:
    fpres = open(pf, 'r')
    fread = [line.strip() for line in fpres]
    fhead = [col for col in fread[0].strip('#').split()]
    fdata = [line.split() for line in fread if not re.match('#|@', line)]
    fdata = [map(float, row) for row in fdata]
    pgrps = {('dPndr' if re.search('dPndr', grp) else grp):num for num, grp in enumerate(fhead)}
    pprof = np.array(fdata)
    if re.search('PRIN', pf) and re.search('PRIP', pf):
        slabel  = 'N' + re.search("PRIN([0-9]+)", pf).group(1) + '-'
        slabel += 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    elif re.search('PRIN', pf):
        slabel = 'N' + re.search("PRIN([0-9]+)", pf).group(1)
    elif re.search('PRIP', pf):
        slabel = 'P' + re.search("PRIP([0-9]+)", pf).group(1)
    else:
        slabel = 'Neat'
    psomepprofs[slabel] = (pgrps, pprof) 
    fpres.close()

# Line color by density group
dgrpcolor = {'System':'dimgrey', 'PEO':'m', 'PBD':'salmon', 'PRIN':'g', 'PRIP':'r', 'CLIO':'b', 'PW':'c'}

# Density group labels
dgrplabel = {'System':'System', 'PLY':'Polymer', 'PEO':'PEO', 'PBD':'PBD', 
             'PW':'Water', 'PRIN':'nPLC', 'PRIP':'pPLC', 'CLIO':'Cl$^-$'}


# Line color by pressure group
pgrpcolor = {'Pt':'r', 'Pn':'b', 'Pt-Pn':'olive', 'dPndr':'darkorange'}

# Pressure group labels
pgrplabel = {'Pt':'$P_T$', 'Pn':'$P_N$', 'Pt-Pn':'$P_T-P_N$', 'dPndr':'$\\frac{r}{2}\\frac{dP_N}{dr}$'}

# Legend properties
locs = {0:'upper left',  1:'upper center', 2:'upper right',
        3:'center left', 4:'center'      , 5:'center right',
        6:'lower left',  7:'lower center', 8:'lower right'}

fsize = {0:'xx-small', 1:'x-small', 2:'small', 3:'medium',
         4:'large',    5:'x-large', 6:'xx-large'}

# Set plot data
nc = -1
lwidth = 2.5
fig, axes = plt.subplots(nrows=2, ncols=2, sharex='col', sharey='row', squeeze=False)

# Add densities and pressure plots
membnoneat  = [sl for sl in membdprofs.keys() if sl != 'Neat']
psomenoneat = [sl for sl in psomedprofs.keys() if sl != 'Neat']
membsort    = ['Neat'] + sorted(membnoneat, key=lambda x: int(re.search('N([0-9]+)', x).group(1)))
psomesort   = ['Neat'] + sorted(psomenoneat, key=lambda x: int(re.search('N([0-9]+)', x).group(1)))

mshf  = 0 #11
pshf  = 0
ldash = [1.5, 6.0] 

dgrps, dprof = membdprofs['Neat'] 
for grp, num in sorted(dgrps.items(), key=itemgetter(1)):
    zpos, dens = dprof[:, 0], dprof[:, num]
    if grp in ['System', 'PBD', 'PEO', 'PW']: 
        axes[0, 0].plot(zpos+mshf, dens, c=dgrpcolor[grp], ls='-', lw=lwidth, label=dgrplabel[grp])      

dgrps, dprof = psomedprofs['Neat'] 
for grp, num in sorted(dgrps.items(), key=itemgetter(1)):
    rpos, dens = dprof[:, 0], dprof[:, num]
    if grp in ['System', 'PBD', 'PEO', 'PW']:
        axes[0, 1].plot(rpos+pshf, dens, c=dgrpcolor[grp], ls='-', lw=lwidth, label=dgrplabel[grp]) #ls='--', lw=3.0, dashes=ldash, dash_capstyle='round')    

pgrps, pprof = membpprofs['Neat'] 
zpos, ptan, pnor  = pprof[:, 0], pprof[:, pgrps['Ptan']], pprof[:, pgrps['Pnor']]
#axes[1, 0].plot(zpos+mshf, ptan-pnor, c=pgrpcolor['Pt-Pn'], ls='-', lw=lwidth, label=pgrplabel['Pt-Pn'])
axes[1, 0].plot(zpos+mshf, ptan, c=pgrpcolor['Pt'], ls='-', lw=lwidth, label=pgrplabel['Pt'])
axes[1, 0].plot(zpos+mshf, pnor, c=pgrpcolor['Pn'], ls='-', lw=lwidth, label=pgrplabel['Pn'])

pgrps, pprof = psomepprofs['Neat'] 
rpos, ptan, pnor  = pprof[:, 0], pprof[:, pgrps['Pt']], pprof[:, pgrps['Pn']]
#axes[1, 1].plot(rpos+pshf, ptan-pnor, c=pgrpcolor['Pt-Pn'], ls='-', lw=lwidth, label=pgrplabel['Pt-Pn']) #ls='--', lw=3.0, dashes=ldash, dash_capstyle='round')
axes[1, 1].plot(rpos+pshf, ptan, c=pgrpcolor['Pt'], ls='-', lw=lwidth, label=pgrplabel['Pt']) #ls='--', lw=3.0, dashes=ldash, dash_capstyle='round')
axes[1, 1].plot(rpos+pshf, pnor, c=pgrpcolor['Pn'], ls='-', lw=lwidth, label=pgrplabel['Pn']) #ls='--', lw=3.0, dashes=ldash, dash_capstyle='round')


# Set labels, ranges and title
nc = 0
fs = 16
xlabels  = ['$z$ $\mathsf{(nm)}$', '$r$ $\mathsf{(nm)}$']
ylabels  = ['$\mathsf{MDP}$ $\mathsf{(kg/m^3)}$', '$\mathsf{Pressure}$ $\mathsf{(bar)}$']
letters  = {0:{0:'(a)', 1:'(b)'}, 1:{0:'(c)', 1:'(d)'}}

for nf in range(4):

    nr = nf % 2
    nc = nf / 2

    # y labels
    if nc == 0:
        axes[nr, nc].set_ylabel(ylabels[nr], labelpad=18, size=22)
        axes[nr, nc].get_yaxis().set_label_coords(-0.22, 0.50, transform=axes[nr, 0].transAxes)

    # y ranges and x labels
    if nr == 0:
        axes[nr, nc].set_ylim(-100, 1200)
        axes[nr, nc].set_yticks(np.arange(0, 1100, 250)) 
    else:
        axes[nr, nc].set_xlabel(xlabels[nc], labelpad=10, size=23)
        axes[nr, nc].set_ylim(-280, 150)
        axes[nr, nc].set_yticks(np.arange(-200, 120, 100)) 

    # x ranges
    if nc == 0:
        axes[nr, nc].set_xlim(-10, 10)
        axes[nr, nc].set_xticks(np.arange(-9, 10, 3))
    else:
        axes[nr, nc].set_xlim(1.1, 18.0)
        axes[nr, nc].set_xticks(np.arange(2, 19, 3)) 

    # titles
    if nr == 0 and nc == 0:
        axes[nr, nc].set_title('Neat bilayer', size=19, va='bottom')
    elif nr == 0 and nc == 1:
        axes[nr, nc].set_title('Neat polymersome', size=19, va='bottom')

    # letters
    if nr == 0:
        axes[nr, nc].text(
            0.04, 0.82, letters[nr][nc], ha='left', va='top', size=22, 
            transform=axes[nr, nc].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3}
        )
    elif nr == 1:
        axes[nr, nc].text(
            0.04, 0.93, letters[nr][nc], ha='left', va='top', size=22, 
            transform=axes[nr, nc].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3}
        )
    elif nr == 2:
        axes[nr, nc].text(
            0.04, 0.87, letters[nr][nc], ha='left', va='top', size=22, 
            transform=axes[nr, nc].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3}
        )

    # legends
    if nc == 1:
        lfs = (15 if nr == 0 else 17)
        axes[nr, nc].legend(
            loc=locs[3], bbox_to_anchor=(1.02, 0.5), fontsize=lfs, 
            ncol=1, framealpha=1.0, handlelength=1.5, handletextpad=0.5
        )

    # margins and grids
    axes[nr, nc].margins(0.05)
    axes[nr, nc].grid()


height = 6
width  = 10
fig.set_dpi(200)
fig.set_size_inches(width, height)
fig.tight_layout(h_pad=0.0, w_pad=0.6)
fig.savefig(plotfile, bbox_inches='tight', pad_inches=0.20)


