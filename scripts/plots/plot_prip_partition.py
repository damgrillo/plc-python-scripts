#!/usr/bin/python

import os
import re
import sys
import math
import string
import random
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams, patches
from matplotlib.gridspec import GridSpec

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
plotfile   = str(sys.argv[1])
membdfile  = str(sys.argv[2])
psomedfile = str(sys.argv[3])

# Get density data for membranes
fdens = open(membdfile, 'r')
fread = [line.strip() for line in fdens]
fhead = [col for col in fread[0].strip('#').split()]
fdata = [line.split() for line in fread if not re.match('#|@', line)]
fdata = [map(float, row) for row in fdata]
dgrps = {grp:num for num, grp in enumerate(fhead)}
dprof = np.array(fdata)
membdprof = dprof[:, [0, dgrps['PBD'], dgrps['PRIP']]] 
fdens.close()

# Get density data for polymersomes
fdens = open(psomedfile, 'r')
fread = [line.strip() for line in fdens]
fhead = [col for col in fread[0].strip('#').split()]
fdata = [line.split() for line in fread if not re.match('#|@', line)]
fdata = [map(float, row) for row in fdata]
dgrps = {grp:num for num, grp in enumerate(fhead)}
dprof = np.array(fdata)
psomedprof = dprof[:, [0, dgrps['PBD'], dgrps['PRIP']]] 
fdens.close()

# Split density in interfase/water regions for bilayers
dprof = membdprof
ihalf = np.argwhere(dprof[:, 0] < 0).flatten()[-1]
dlcut = dprof[(-7 < dprof[:, 0]) & (dprof[:, 0] < -3)]
ducut = dprof[( 3 < dprof[:, 0]) & (dprof[:, 0] <  7)]
zlw   = dlcut[np.argmin(dlcut[:, 2]), 0]
zuw   = ducut[np.argmin(ducut[:, 2]), 0]
ilw   = np.argwhere(dprof[:, 0] == zlw).flatten()[-1]
iuw   = np.argwhere(dprof[:, 0] == zuw).flatten()[-1]
dlwat = dprof[:ilw+1, :]
dlint = dprof[ilw:ihalf+1, :]
duint = dprof[ihalf:iuw+1, :]
duwat = dprof[iuw:, :]
dmemb = (dlwat, dlint, duint, duwat)

# Split density in interfase/water regions for polymersomes
dprof  = psomedprof
rp     = dprof[:, 0]
dpbd   = dprof[:, 1]
rmean  = np.trapz(y=rp*dpbd, x=rp) / np.trapz(y=dpbd, x=rp)
ihalf  = np.argwhere(dprof[:, 0] < rmean).flatten()[-1]
dicut  = dprof[( 4 < dprof[:, 0]) & (dprof[:, 0] <  8)]
docut  = dprof[(14 < dprof[:, 0]) & (dprof[:, 0] < 18)]
riw    = dicut[np.argmin(dicut[:, 2]), 0]
row    = docut[np.argmin(docut[:, 2]), 0]
iiw    = np.argwhere(dprof[:, 0] == riw).flatten()[-1]
iow    = np.argwhere(dprof[:, 0] == row).flatten()[-1]
diwat  = dprof[:iiw+1, :]
diint  = dprof[iiw:ihalf+1, :]
doint  = dprof[ihalf:iow+1, :]
dowat  = dprof[iow:, :]
dpsome = (diwat, diint, doint, dowat)

# Add vertical lines for membranes
zlw       = dmemb[0][-1, 0]
zm        = dmemb[1][-1, 0]
zuw       = dmemb[2][-1, 0]
zlwline   = np.array([(zlw, d) for d in np.linspace(-10, 110, 300)])
zmeanline = np.array([( zm, d) for d in np.linspace(-10, 110, 300)])
zuwline   = np.array([(zuw, d) for d in np.linspace(-10, 110, 300)])
plmemb    = (zlwline, zmeanline, zuwline)

# Add vertical lines for polymersomes
rlw       = dpsome[0][-1, 0]
rm        = dpsome[1][-1, 0]
ruw       = dpsome[2][-1, 0]   
rlwline   = np.array([(rlw, d) for d in np.linspace(-10, 110, 300)])
rmeanline = np.array([(rm,  d) for d in np.linspace(-10, 110, 300)])
ruwline   = np.array([(ruw, d) for d in np.linspace(-10, 110, 300)])
plpsome   = (rlwline, rmeanline, ruwline)

# Line color by number
linecolor = {0:'k', 1:'r', 2:'g', 3:'k', 4:'y'}
dashcolor = {0:'r', 1:'m', 2:'g'}

# Legend properties
locs = {0:'upper left',  1:'upper center', 2:'upper right',
        3:'center left', 4:'center'      , 5:'center right',
        6:'lower left',  7:'lower center', 8:'lower right'}

fsize = {0:'xx-small', 1:'x-small', 2:'small', 3:'medium',
         4:'large',    5:'x-large', 6:'xx-large'}

# Set plot data
nc = -1
lwidth = 2.5
ldash1 = [2.0, 5.0]
ldash2 = [1.0, 4.0]
fig, axes = plt.subplots(nrows=1, ncols=2, sharex='col', sharey='row', squeeze=False)

# Add densities and pressure plots
for systype in ['MEMB', 'PSOME']:
    nc +=  1
    cn  = -1
    if systype == 'MEMB':
        dprof = dmemb
        pline = plmemb
        llab  = ('$z_{lw}$', '$z_m$', '$z_{uw}$')
        plab  = ('$\mathrm{low}$', '$\mathrm{upp}$')
        ppos  = (-3.03, 3.0)
    else:
        dprof = dpsome
        pline = plpsome
        llab  = ('$R_{iw}$', '$R_m$', '$R_{ow}$')
        plab  = ('$\mathrm{inn}$', '$\mathrm{out}$')
        ppos  = (8.15, 13.85)
    for dp in dprof:
        cn += 1
        axes[0, nc].plot(dp[:, 0], dp[:, 2], c=linecolor[cn], ls='-', lw=lwidth, label='') 
        if 0 < cn < 3:     
            axes[0, nc].fill_between(dp[:, 0], dp[:, 2], facecolor=linecolor[cn], alpha=0.22, interpolate=False)
            axes[0, nc].text(
                ppos[cn-1], 8, plab[cn-1], ha='center', va='center', size=21, color=linecolor[cn],
                transform=axes[0, nc].transData, bbox={'color':'white', 'alpha':0.0, 'pad':3}
            )
        if cn < 3:
            lpos = pline[cn][0, 0] + 0.3
            axes[0, nc].plot(
                pline[cn][:, 0], pline[cn][:, 1], c=dashcolor[cn], 
                ls='-', lw=2.0, dashes=ldash1, dash_capstyle='round', label=''
            ) 
            axes[0, nc].text(
                lpos, 100, llab[cn], ha='left', va='top', size=22, color=dashcolor[cn],
                transform=axes[0, nc].transData, bbox={'color':'white', 'alpha':0.0, 'pad':3}
            )

# Set labels, ranges and title
fs = 16
ylabel   = '$\mathsf{pPLC\ MDP \; (kg/m^3)}$'
letters  = {0:{0:'(a)', 1:'(b)'}, 1:{0:'(b)', 1:'(e)'}, 2:{0:'(c)', 1:'(f)'}}

# labels
axes[0, 0].set_xlabel('$z$ $\\mathsf{(nm)}$', labelpad=10, size=24)
axes[0, 1].set_xlabel('$r$ $\\mathsf{(nm)}$', labelpad=10, size=24)
axes[0, 0].set_ylabel(ylabel, labelpad=18, size=24)
axes[0, 0].get_yaxis().set_label_coords(-0.18, 0.50, transform=axes[0, 0].transAxes)

# ranges
axes[0, 0].set_ylim(-5, 105)
axes[0, 0].set_yticks(np.arange(0, 101, 20)) 
axes[0, 0].set_xlim(-8.5, 8.5)
axes[0, 0].set_xticks(np.arange(-8, 9, 2))
axes[0, 1].set_xlim(2.5, 18.2)
axes[0, 1].set_xticks(np.arange(3, 19, 2)) 

# titles
axes[0, 0].text(0.5, 1.05, 'Bilayer', size=22, 
                ha='center', va='bottom', transform=axes[0, 0].transAxes)   
axes[0, 1].text(0.5, 1.05, 'Polymersome', size=22, 
                ha='center', va='bottom', transform=axes[0, 1].transAxes)   

# letters
axes[0, 0].text(0.03, 0.95, letters[0][0], ha='left', va='top', size=26, 
                transform=axes[0, 0].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3})
axes[0, 1].text(0.03, 0.95, letters[0][1], ha='left', va='top', size=26, 
                transform=axes[0, 1].transAxes, bbox={'color':'white', 'alpha':1.0, 'pad':3})

# margins and grids
axes[0, 0].margins(0.05)
axes[0, 1].margins(0.05)

# plot size
height = 4.5
width  = 12
fig.set_dpi(200)
fig.set_size_inches(width, height)
fig.tight_layout(h_pad=0.0, w_pad=1.0)
fig.savefig(plotfile, bbox_inches='tight', pad_inches=0.20)

