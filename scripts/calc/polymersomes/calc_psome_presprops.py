import os
import re
import sys
import math
import string
import numpy as np


####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
resdir  = str(sys.argv[1])
presdir = str(sys.argv[2])
pfxname = str(sys.argv[3])
twin    = int(sys.argv[4])
pmethod = str(sys.argv[5])

# Units conversion
# 1 mN/m     = 10 bar*nm
# 1 bar*nm3  = 1e-2 * 1e-20 J
# 1 kb*T     = 1e-3 * 1.3806 * Temp * 1e-20 J 
# 1 kb*T     = 1e-1 * 1.3806 * Temp * bar*nm^3

# Temperature and normal pressure
temp       = 300
kbt2barnm3 = 1.3806488 * 1e-1 * temp # Convert kbt to bar*nm^3
kbt2mNm    = 1.3806488 * 1e-2 * temp # Convert kbT to mN*m
kbt2J      = 1.3806488 * 1e-3 * temp # Convert kbT to 1e-20 J

# Get pressure files for corresponding time window
presfiles = []
mintime   = None
for f in sorted([os.path.join(presdir, f) for f in os.listdir(presdir)]):
    if re.search("presprof-job", f) and re.search(pmethod, f):
        stime, etime = map(int, re.search('job-([0-9]+)-([0-9]+)', f).groups())
        if etime - stime + 1 == twin:
            presfiles.append(f)
        if (mintime == None) or (stime < mintime):
            mintime = stime

# Get pressure profiles 
presprofs = []
for txt in presfiles:  
    finp  = open(txt, 'r')
    fread = [line.strip() for line in finp] 
    fdata = [line.split() for line in fread if not re.match('#|@', line)] 
    pprof = np.array([[float(x) for x in d] for d in fdata])
    presprofs.append(pprof)
    finp.close()

# Calculate pressure moments
rads = {}
pmm1 = {}
pm0  = {}
pm1  = {}
pm2  = {}
t    = mintime + twin
for prof in presprofs:
    pf = prof[prof[:, 0] > 1.0]
    r  = pf[:, 0]
    pt = pf[:, 1]
    pn = pf[:, 2]
    rads[t] = r
    pmm1[t] = np.trapz(y=(pn-pt)/r, x=r)
    pm0[t]  = np.trapz(y=(pn-pt), x=r)
    pm1[t]  = np.trapz(y=(pn-pt)*r, x=r)
    pm2[t]  = np.trapz(y=(pn-pt)*r*r, x=r)
    t += twin

# Calculate pressure properties
#
# Surface of tension (Reference: Ollila, Rowlinson and Widom) 
# rs^3 = int(r^2 * (Pn-Pt) * dr) / int(r^-1 * (Pn-Pt) * dr) = presmoment(2)/presmoment(-1) 
#
# Surface tension (only valid at rs) (Reference: Gurkov, Ollila, Rowlinson and Widom)
# gs = int((r/rs)^2 * (Pn-Pt) * dr) = presmoment(2)/rs^2 (Buff)
# gs = int(rs/r * (Pn-Pt) * dr) = presmoment(-1)*rs      (Goodrich)
#
# Surface tension (valid at any dividing surface R) (Reference: Gurkov)
# gamma(R) = (1/3) * int((2*R/r + (r/R)^2) * (Pn-Pt) * dr) = (1/3) * (2*presmoment(-1)*R + presmoment(2)/R^2)
#
# Bending moment  (valid at any dividing surface R) (Reference: Gurkov)
# bendm(R) = (2/3) * int((R^2/r - r^2/R) * (Pn-Pt) * dr) = (2/3) * (presmoment(-1)*R^2 - presmoment(2)/R)
rs      = {}
gsbuff  = {}
gsgood  = {}
gamrad  = {}
bendrad = {}
for t in sorted(pmm1.keys()):
    rs[t]      = np.cbrt(pm2[t]/pmm1[t])
    gsbuff[t]  = pm2[t]/rs[t]**2
    gsgood[t]  = pmm1[t]*rs[t]
    gamrad[t]  = (1.0/3)*(2*pmm1[t]*rads[t] + pm2[t]/rads[t]**2)
    bendrad[t] = (2.0/3)*(pmm1[t]*rads[t]**2 - pm2[t]/rads[t])

# Gamma and bending moment profiles
avg_rads    = np.mean([x for x in rads.values()], axis=0)
avg_gamrad  = np.mean([x for x in gamrad.values()], axis=0)
std_gamrad  = np.std([x for x in gamrad.values()], axis=0)
avg_bendrad = np.mean([x for x in bendrad.values()], axis=0)
std_bendrad = np.std([x for x in bendrad.values()], axis=0)

###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write pressure moments
propsdir = resdir + '/mechprops/' + pfxname
if not os.path.isdir(propsdir):
    os.makedirs(propsdir)

outfile = propsdir + '/presmoments-' + pmethod + '-avgwin-' + str(twin) + 'ns.txt'
fout = open(outfile, 'w')
sout  = '#{:>11s} '.format('Time')
sout += '{:>12s} '.format('mm1')
sout += '{:>12s} '.format('m0')
sout += '{:>12s} '.format('m1')
sout += '{:>12s} '.format('m2')
sout += '{:>12s} '.format('rs')
sout += '{:>12s} '.format('gs_buff')
sout += '{:>12s} '.format('gs_goodrich')
sout += '\n'
sout += '#{:>11s} '.format('[ns]')
sout += '{:>12s} '.format('[bar]')
sout += '{:>12s} '.format('[bar*nm]')
sout += '{:>12s} '.format('[bar*nm^2]')
sout += '{:>12s} '.format('[bar*nm^3]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[bar*nm]')
sout += '{:>12s} '.format('[bar*nm]')
sout += '\n'
fout.write(sout)

for t in sorted(pmm1.keys()):
    sout  = '{0:>12.2f} '.format(t)
    sout += '{0:>12.4f} '.format(pmm1[t])
    sout += '{0:>12.4f} '.format(pm0[t])
    sout += '{0:>12.4f} '.format(pm1[t])
    sout += '{0:>12.4f} '.format(pm2[t])
    sout += '{0:>12.4f} '.format(rs[t])
    sout += '{0:>12.4f} '.format(gsbuff[t])
    sout += '{0:>12.4f} '.format(gsgood[t])
    sout += '\n'
    fout.write(sout)
fout.close()

# Write pressure properties
for t in sorted(rads.keys()): 

    outfile = propsdir + '/presprops-' + pmethod + '-avgwin-' + str(t-twin) + '-' + str(t-1) + 'ns.txt'
    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('R')
    sout += '{:>12s} '.format('gamma')
    sout += '{:>12s} '.format('bendm')
    sout += '\n'
    sout += '#{:>11s} '.format('[nm]')
    sout += '{:>12s} '.format('[bar*nm]')
    sout += '{:>12s} '.format('[bar*nm^2]')
    sout += '\n'
    fout.write(sout)

    for row in np.arange(rads[t].shape[0]):
        sout  = '{0:>12.2f} '.format(rads[t][row])
        sout += '{0:>12.4f} '.format(gamrad[t][row])
        sout += '{0:>12.4f} '.format(bendrad[t][row])
        sout += '\n'
        fout.write(sout)
    fout.close()

