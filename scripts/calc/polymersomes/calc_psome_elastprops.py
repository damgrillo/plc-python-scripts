import os
import re
import sys
import math
import string
import random
import numpy as np
from operator import itemgetter
from itertools import izip
from matplotlib import pyplot as plt, rcParams
from scipy.optimize import leastsq

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'


###########################################################################################################
################################################## FUNCTIONS ##############################################
###########################################################################################################

# Get density profiles
def get_densprofs(dnsfile):
    fdens   = open(dnsfile, 'r')
    fread   = [line.strip() for line in fdens] 
    fdata   = [line.split() for line in fread if not re.match('#|@', line)]
    fhead   = [col for col in fread[0].lstrip('#').split()]
    dnscols = {label:col for col, label in enumerate(fhead)}     
    dnsdata = np.array([map(float, d) for d in fdata]) 
    fdens.close()
    return {label:dnsdata[:, col] for label, col in dnscols.items()}

# Get number of encapsulated molecules per area
def get_nenc(encfile):
    fenc   = open(encfile, 'r')
    fread  = [line.strip() for line in fenc] 
    fdata  = [line.split() for line in fread if not re.match('#|@', line)]
    fhead  = [col for col in fread[0].lstrip('#').split()]
    ncols  = {label:col for col, label in enumerate(fhead)}     
    nedata = np.array([map(float, d) for d in fdata])
    fenc.close()
    return nedata[:, ncols['ne=Ne/Area']]

# Get mean radius
def get_mean_radius(rp, dp):    
    rm = np.trapz(y=dp*rp, x=rp) / np.trapz(y=dp, x=rp)
    return rm

# Get inner radius
def get_inner_radius(rpos, dpbd, dpw, rmean):
    ddif = np.column_stack((rpos, np.absolute(dpbd-dpw)))
    dinn = ddif[(1.0 < ddif[:, 0]) & (ddif[:, 0] < rmean)]
    rinn = dinn[np.argmin(dinn[:, 1]), 0]
    return rinn   

# Get outer radius
def get_outer_radius(rpos, dpbd, dpw, rmean):
    ddif = np.column_stack((rpos, np.absolute(dpbd-dpw)))
    dout = ddif[ddif[:, 0] > rmean]
    rout = dout[np.argmin(dout[:, 1]), 0]   
    return rout

# Get kbend
def get_kbend(kbfile):
    fk = open(kbfile, 'r')
    kcpars = np.array([map(float, line.split()) for line in fk if not re.match('#|@', line)]) 
    kcavg  = np.mean(kcpars[:, 1], axis=0)
    kcstd  = np.mean(kcpars[:, 2], axis=0)
    fk.close()
    return kcavg, kcstd

# Get pressure moments info
def get_pminfo(pmfile):
    fpm    = open(pmfile, 'r')
    pmread = [line.strip() for line in fpm] 
    pmcols = {label:col for col, label in enumerate(pmread[0].lstrip('#').split())}     
    pmdata = [map(float, line.split()) for line in pmread if not re.match('#|@', line)] 
    pmdata = np.array(pmdata)
    fpm.close()
    return {label:pmdata[:, [0, col]] for label, col in pmcols.items()}

# Get ideal radius r0
def get_rzero(ravgfile):
    fr    = open(ravgfile, 'r')
    rdata = [line.split() for line in fr if not re.match('#|@', line)]
    rdata = [(re.search('job-([0-9]+)', rd[0]).group(1), rd[-1]) for rd in rdata]
    rzero = np.array([map(float, rd) for rd in rdata]) 
    fr.close()
    return rzero


###########################################################################################################
################################################# MAIN PROGRAM ############################################
###########################################################################################################

# Input variables
dnsfile   = str(sys.argv[1])
pmfile    = str(sys.argv[2])
ravgfile  = str(sys.argv[3])
kbfile    = str(sys.argv[4])
iencfile  = str(sys.argv[5])
oencfile  = str(sys.argv[6])
elastfile = str(sys.argv[7])
twin      = int(sys.argv[8])
lastns    = int(sys.argv[9])

# Units conversion
# 1 mN/m     = 10 bar*nm
# 1 bar*nm3  = 1e-2 * 1e-20 J
# 1 kb*T     = 1e-3 * 1.3806 * Temp * 1e-20 J 
# 1 kb*T     = 1e-1 * 1.3806 * Temp * bar*nm^3

# Global parameters
kbt        = 1
lcut       = 60
temp       = 300
mNm2barnm  = 10                      # Convert mN/m to bar*nm
kbt2barnm3 = 1.3806488 * 1e-1 * temp # Convert kbT to bar*nm^3
kbt2J      = 1.3806488 * 1e-3 * temp # Convert kbT to 1e-20 J

# Set system label
if re.search('PRIN', dnsfile) and re.search('PRIP', dnsfile):
    slabel  = 'N' + re.search("PRIN([0-9]+)", dnsfile).group(1) + '-'
    slabel += 'P' + re.search("PRIP([0-9]+)", dnsfile).group(1)
elif re.search('PRIN', dnsfile):
    slabel += 'N' + re.search("PRIN([0-9]+)", dnsfile).group(1)
elif re.search('PRIP', dnsfile):
    slabel += 'P' + re.search("PRIP([0-9]+)", dnsfile).group(1)
else:
    slabel = 'Neat'

# Get density profiles, inner and outer radius
dprof = get_densprofs(dnsfile)
rpos  = dprof['R']
dpbd  = dprof['PBD']
dpw   = dprof['PW']
rmean = get_mean_radius(rpos, dpbd)
rinn  = get_inner_radius(rpos, dpbd, dpw, rmean)
rout  = get_outer_radius(rpos, dpbd, dpw, rmean)

# Get inner/outer encapsulated PRIP
ienc = (get_nenc(iencfile) if os.path.isfile(iencfile) else np.zeros(1))
oenc = (get_nenc(oencfile) if os.path.isfile(oencfile) else np.zeros(1))
denc = oenc-ienc

ienc = [n for n in ienc if n == n]
oenc = [n for n in oenc if n == n]
denc = [n for n in denc if n == n]

# Get pressure moments info
pminfo  = get_pminfo(pmfile)
mm1     = pminfo['mm1'][:, 1]
m0      = pminfo['m0'][:, 1]
m2      = pminfo['m2'][:, 1]
rs      = pminfo['rs'][:, 1]
r0      = get_rzero(ravgfile)

# Get bending modulus
kcavg, kcstd = get_kbend(kbfile)

# Calculate gamma and bendm at inner, outer and surface radius
gamma_inn = (1.0/3)*(2*mm1*rinn + m2/rinn**2)
gamma_out = (1.0/3)*(2*mm1*rout + m2/rout**2)
gamma_rs  = (1.0/3)*(2*mm1*rs + m2/rs**2)
bendm_inn = (2.0/3)*(mm1*rinn**2 - m2/rinn)
bendm_out = (2.0/3)*(mm1*rout**2 - m2/rout)
bendm_rs  = (2.0/3)*(mm1*rs**2 - m2/rs)
 
# Calculate kg and c0
m0 /= kbt2barnm3
m2 /= kbt2barnm3
cb  = -1.0/rs
kg  = (1.0/3)*m2 - m0*rs*rs
c0  = cb + 0.5*m0*rs/kcavg
eb  = 2*kcavg*(cb-c0)**2 + kg*cb*cb
de  = 4*kcavg*(cb-c0) + 2*kg*cb 
cm  = c0 / (1 + 0.5*kg/kcavg)


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write elastic parameters
if not os.path.exists(elastfile):
    fout  = open(elastfile, 'w')
    sout  = '#{:>13s} '.format('system')
    sout += '{:>13s} '.format('inn_enc')
    sout += '{:>13s} '.format('out_enc')
    sout += '{:>13s} '.format('diff_enc')
    #sout += '{:>8s} '.format('rinn')
    sout += '{:>10s} '.format('rs')
    #sout += '{:>8s} '.format('rout')
    #sout += '{:>13s} '.format('gamma_inn')
    sout += '{:>10s} '.format('gamma_rs')
    #sout += '{:>13s} '.format('gamma_out')
    sout += '{:>13s} '.format('c0')
    sout += '{:>11s} '.format('kc')
    #sout += '{:>11s} '.format('kc')
    sout += '{:>10s} '.format('kg')
    #sout += '{:>10s} '.format('kg')
    sout += '{:>11s} '.format('kg/kc')
    sout += '{:>11s} '.format('eb')
    sout += '{:>11s} '.format('de/dc')
    sout += '{:>13s} '.format('cm')

    sout += '\n'
    sout += '#{:>13s} '.format(' ')
    sout += '{:>13s} '.format('[1/nm2]')
    sout += '{:>13s} '.format('[1/nm2]')
    sout += '{:>13s} '.format('[1/nm2]')
    #sout += '{:>8s} '.format('[nm]')
    sout += '{:>10s} '.format('[nm]')
    #sout += '{:>8s} '.format('[nm]')
    #sout += '{:>13s} '.format('[bar*nm]')
    sout += '{:>10s} '.format('[bar*nm]')
    #sout += '{:>13s} '.format('[bar*nm]')
    sout += '{:>13s} '.format('[1/nm]')
    sout += '{:>11s} '.format('[kbT]')
    #sout += '{:>11s} '.format('[1e-20 J]')
    sout += '{:>10s} '.format('[kbT]')
    #sout += '{:>10s} '.format('[1e-20 J]')
    sout += '{:>11s} '.format('[adim]')
    sout += '{:>11s} '.format('[kbT/nm2]')
    sout += '{:>11s} '.format('[kbT/nm3]')
    sout += '{:>13s} '.format('[1/nm]')
    sout += '\n'
    fout.write(sout)
else:
    fout  = open(elastfile, 'a')    

sout  = slabel.rjust(14) + ' '
sout += '{0:.3f}/{1:.3f}'.format(np.mean(ienc), np.std(ienc)).rjust(13) + ' '
sout += '{0:.3f}/{1:.3f}'.format(np.mean(oenc), np.std(oenc)).rjust(13) + ' '
sout += '{0:.3f}/{1:.3f}'.format(np.mean(denc), np.std(denc)).rjust(13) + ' '
#sout += '{0:.3f}'.format(rinn).rjust(8) + ' '
sout += '{0:.1f}/{1:.1f}'.format(np.mean(rs), np.std(rs)).rjust(10) + ' '
#sout += '{0:.3f}'.format(rout).rjust(8) + ' '
#sout += '{0:.0f}/{1:.0f}'.format(np.mean(gamma_inn), np.std(gamma_inn)).rjust(13) + ' '
sout += '{0:.0f}/{1:.0f}'.format(np.mean(gamma_rs), np.std(gamma_rs)).rjust(10) + ' '
#sout += '{0:.0f}/{1:.0f}'.format(np.mean(gamma_out), np.std(gamma_out)).rjust(13) + ' '
sout += '{0:.3f}/{1:.3f}'.format(np.mean(c0), np.std(c0)).rjust(13) + ' '
sout += '{0:.1f}/{1:.1f}'.format(kcavg, kcstd).rjust(11) + ' '
#sout += '{0:.1f}/{1:.1f}'.format(kcavg*kbt2J, kcstd*kbt2J).rjust(11) + ' '
sout += '{0:.0f}/{1:.0f}'.format(np.mean(kg), np.std(kg)).rjust(10) + ' '
#sout += '{0:.0f}/{1:.0f}'.format(np.mean(kg)*kbt2J, np.std(kg)*kbt2J).rjust(10) + ' '
sout += '{0:.1f}/{1:.1f}'.format(np.mean(kg/kcavg), np.std(kg/kcavg)).rjust(11) + ' '
sout += '{0:.1f}/{1:.1f}'.format(np.mean(eb), np.std(eb)).rjust(11) + ' '
sout += '{0:.1f}/{1:.1f}'.format(np.mean(de), np.std(de)).rjust(11) + ' '
sout += '{0:.3f}/{1:.3f}'.format(np.mean(cm), np.std(cm)).rjust(13) + ' '
sout += '\n'
fout.write(sout)

fout.close()


