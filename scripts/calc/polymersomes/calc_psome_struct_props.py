import os
import re
import sys
import math
import string
import numpy as np
from itertools import izip
from operator import itemgetter

###############################################################################################
########################################## FUNCTIONS ##########################################
###############################################################################################

# Get parameters for equation of line y = m * x + b
def get_line_params(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    m = (y2 - y1) / (x2 - x1)
    b = y2 - m * x2
    return (m, b)

# Solve equation y = m * x + b for a given y
def solve_line(y, m, b):
    return (y - b) / m

# Get intersections at fraction height
def get_intersections(dprof, col, frac):    

    # Calculate half-height density value
    cprof     = dprof[:, [0, col]]
    max_dens  = np.amax(cprof[:, 1])
    high_dens = [dns for dns in cprof[:, 1] if dns > 0.9 * max_dens]
    frac_dens = frac * sum(high_dens) / len(high_dens)

    # Calculate center of the profile
    pos    = cprof[:, 0]
    dens   = cprof[:, 1]
    center = np.trapz(y=dens*pos, x=pos) / np.trapz(y=dens, x=pos) 

    # Calculate zcoord on the left side corresponding to the half-height density 
    lwleft = max((p for p in cprof if (p[0] < center and p[1] < frac_dens)), key=itemgetter(1))
    upleft = min((p for p in cprof if (p[0] < center and p[1] > frac_dens)), key=itemgetter(1))
    ml, bl = get_line_params(lwleft, upleft)
    fl     = solve_line(frac_dens, ml, bl)

    # Calculate zcoord on the right side corresponding to the half-height density 
    lwright = max((p for p in cprof if (p[0] > center and p[1] < frac_dens)), key=itemgetter(1))
    upright = min((p for p in cprof if (p[0] > center and p[1] > frac_dens)), key=itemgetter(1))
    mr, br  = get_line_params(lwright, upright)
    fr      = solve_line(frac_dens, mr, br)
    return fl, fr

# Get mean radius
def get_mean_radius(dprof, col): 
    rp = dprof[:, 0]
    dp = dprof[:, col]
    rm = np.trapz(y=dp*rp, x=rp) / np.trapz(y=dp, x=rp) 
    return rm

# Get mean value
def get_mean_value(dprof, col, frac):
    dmax  = np.amax(dprof[:, col]) 
    cprof = dprof[dprof[:, col] >= dmax*frac]
    rp    = cprof[:, 0]
    dp    = cprof[:, col]
    rmin  = np.amin(rp)
    rmax  = np.amax(rp)
    mval  = np.trapz(y=dp, x=rp) / (rmax-rmin) 
    return rm

# Get maximum density radius
def get_maxdens_radius(dprof, col): 
    return dprof[np.argmax(dprof[:, col]), 0]

# Get layer area
def get_layer_area(dprof, col): 
    rp  = dprof[:, 0]
    dp  = dprof[:, col]
    rm  = np.trapz(y=dp*rp, x=rp) / np.trapz(y=dp, x=rp) 
    sd  = np.trapz(y=dp*(rp-rm)**2, x=rp) / np.trapz(y=dp, x=rp)
    vol = (4.0/3)*np.pi*((rm+sd)**3 - (rm-sd)**3)
    am  = vol/(2*sd)
    return am
    
# Get membrane thickness
def get_memb_thick(dprof, col):    
    hl, hr = get_intersections(dprof, col, 0.5) 
    return hr-hl

# Get membrane volume
def get_memb_vol(ri, ro):
    return (4.0/3)*np.pi*(ro**3 - ri**3) 

# Get number of beads from number density profile
def get_nbeads(rp, dp):
    return np.trapz(y=4*np.pi*rp*rp*dp, x=rp) 

# Get inner radius
def get_inner_radius_pbdpw(rpos, dpbd, dpw, rmean):
    ddif = np.column_stack((rpos, np.absolute(dpbd-dpw)))
    dinn = ddif[(1.0 < ddif[:, 0]) & (ddif[:, 0] < rmean)]
    rinn = dinn[np.argmin(dinn[:, 1]), 0]
    return rinn   

# Get inner radius
def get_inner_radius_prip(rp, dp, rmean):
    rcut = rp[(5.5 < rp) & (rp < 7.5)]
    dcut = dp[(5.5 < rp) & (rp < 7.5)]
    rint = rcut[dcut.argmin()]
    renc = rp[(rint < rp) & (rp < rmean)]
    denc = dp[(rint < rp) & (rp < rmean)]
    return get_mean_radius(np.column_stack((renc, denc)), 1)   

# Get outer radius
def get_outer_radius_pbdpw(rpos, dpbd, dpw, rmean):
    ddif = np.column_stack((rpos, np.absolute(dpbd-dpw)))
    dout = ddif[ddif[:, 0] > rmean]
    rout = dout[np.argmin(dout[:, 1]), 0]    
    return rout   

# Get outer radius
def get_outer_radius_prip(rp, dp, rmean):
    rcut = rp[(14.5 < rp) & (rp < 16.5)]
    dcut = dp[(14.5 < rp) & (rp < 16.5)]
    rint = rcut[dcut.argmin()]
    renc = rp[(rmean < rp) & (rp < rint)]
    denc = dp[(rmean < rp) & (rp < rint)]  
    return get_mean_radius(np.column_stack((renc, denc)), 1)      

# Get number of encapsulated pPLC molecules in inner leaflet
def get_pplc_inner(rp, dp, nbmol):
    rcut = rp[(5.5 < rp) & (rp < 7.5)]
    dcut = dp[(5.5 < rp) & (rp < 7.5)]
    rint = rcut[dcut.argmin()]
    renc = rp[rp > rint]
    denc = dp[rp > rint]
    nenc = get_nbeads(renc, denc)/nbmol
    return nenc

# Get number of encapsulated pPLC molecules in outer leaflet
def get_pplc_outer(rp, dp, nbmol):
    rcut = rp[(14.5 < rp) & (rp < 16.5)]
    dcut = dp[(14.5 < rp) & (rp < 16.5)]
    rint = rcut[dcut.argmin()]
    renc = rp[rp < rint]
    denc = dp[rp < rint]
    nenc = get_nbeads(renc, denc)/nbmol
    return nenc
  

###########################################################################################################
################################################ MAIN PROGRAM #############################################
###########################################################################################################

# Input variables
densdir = str(sys.argv[1])
apchdir = str(sys.argv[2])
resdir  = str(sys.argv[3])
nmprin  = int(sys.argv[4])
nmprip  = int(sys.argv[5])
nmwat   = int(sys.argv[6])
pfxname = str(sys.argv[7])
lastns  = int(sys.argv[8])
ncpinn  = int(sys.argv[9])
ncpout  = int(sys.argv[10])
ncpoly  = ncpinn + ncpout

# Get mass and number density files
massfiles = sorted([os.path.join(densdir, f) for f in os.listdir(densdir) if f.endswith("mass.xvg")])
numbfiles = sorted([os.path.join(densdir, f) for f in os.listdir(densdir) if f.endswith("numb.xvg")])

# Get nchains files
nchafiles = sorted([os.path.join(apchdir, f) for f in os.listdir(apchdir) if f.endswith("ncharea.xvg")])

# Number of beads per molecule
nbmol = {'PLY':36, 'PEO':14, 'PBD':22, 'PRIN':5, 'PRIP':5, 'PW':1,'UPPLAYER':36, 'LOWLAYER':36}

# Number of molecules in the system
nmols = {'PLY':ncpoly, 'PEO':ncpoly, 'PBD':ncpoly, 'UPPLAYER':ncpoly, 'LOWLAYER':ncpoly,
         'PRIN':nmprin, 'PRIP':nmprip, 'PW':nmwat}

# Membrane properties
structprops = []
massdens    = []
numbdens    = []
nchadens    = []
alayers     = []
pplcinn     = []
pplcout     = []
grplist     = ['PEO', 'PBD', 'PW', 'UPPLAYER', 'LOWLAYER'] + (['PRIP'] if re.search('PRIP', pfxname) else [])
nmolinn     = {grp:[] for grp in grplist}
nmolout     = {grp:[] for grp in grplist}
for mtxt, ntxt, ctxt in izip(massfiles, numbfiles, nchafiles):

    # Read mass density     
    finp   = open(mtxt, 'r')
    fread  = [line.strip() for line in finp] 
    fdata  = [line.split() for line in fread if not re.match('#|@', line)] 
    lgrps  = [re.search('\"(.*)\"', line).group(1) for line in fread if re.match('@\s+s[0-9]+', line)]
    mgrps  = {grp:num for num, grp in enumerate(lgrps, 1)}
    mdprof = np.array([map(float, d) for d in fdata])
    massdens.append(mdprof)
    finp.close()

    # Read number density
    finp   = open(ntxt, 'r')
    fread  = [line.strip() for line in finp] 
    fdata  = [line.split() for line in fread if not re.match('#|@', line)] 
    lgrps  = [re.search('\"(.*)\"', line).group(1) for line in fread if re.match('@\s+s[0-9]+', line)]
    ngrps  = {grp:num for num, grp in enumerate(lgrps, 1)}
    ndprof = np.array([map(float, d) for d in fdata])
    numbdens.append(ndprof)
    finp.close()

    # Read number of chains per area
    #finp   = open(ctxt, 'r')
    #fread  = [line.strip() for line in finp] 
    #fdata  = [line.split() for line in fread if not re.match('#|@', line)] 
    #lgrps  = [re.search('\"(.*)\"', line).group(1) for line in fread if re.match('@\s+s[0-9]+', line)]
    #ncgrps = {grp:num for num, grp in enumerate(lgrps, 1)}
    #ncprof = np.array([map(float, d) for d in fdata])
    #nchadens.append(ncprof)
    #finp.close()

    # Get mean radius, inner/outer/total area per chain, thickness and volume
    #rpos = mdprof[:, 0]
    #rm   = get_mean_radius(mdprof, mgrps['PBD'])
    #ri   = get_mean_radius(ndprof, mgrps['LOWLAYER'])
    #ro   = get_mean_radius(ndprof, mgrps['UPPLAYER'])
    #thi  = get_memb_thick(ndprof, mgrps['LOWLAYER'])
    #tho  = get_memb_thick(ndprof, mgrps['UPPLAYER'])
    #vi   = get_memb_vol(ri, thi)
    #vo   = get_memb_vol(ro, tho)
    #ai   = vi/thi
    #ao   = vo/tho
    #ri   = np.sqrt(0.25*ai/np.pi)
    #ro   = np.sqrt(0.25*ao/np.pi)
    #apci = 1/get_mean_value(ncprof, ncgrps['Lower'], 0.5)
    #apco = 1/get_mean_value(ncprof, ncgrps['Upper'], 0.5)
    #apct = 1/get_mean_value(ncprof, ncgrps['Total'], 0.5)
    #thk  = get_memb_thick(mdprof, mgrps['PBD'])
    #vol  = get_memb_vol(rm, thk)
    #structprops.append((ri, rm, ro, ai/ncpinn, apct, ao/ncpout, thk, vol)) 

    # Get mean radius, inner/outer/total area per chain, thickness and volume
    rpos   = mdprof[:, 0]
    rm     = get_mean_radius(mdprof, mgrps['PBD'])
    ri, ro = get_intersections(mdprof, mgrps['PBD'], 0.5)
    ai     = 4*np.pi*ri*ri
    ao     = 4*np.pi*ro*ro
    thk    = get_memb_thick(mdprof, mgrps['PBD'])
    vol    = get_memb_vol(ri, ro)
    structprops.append((ri, rm, ro, ai, ao, thk, vol)) 

    # Get area per bead profile for each leaflet
    aprof  = ndprof[:, [0, ngrps['UPPLAYER'], ngrps['LOWLAYER']]]
    zslice = aprof[1, 0] - aprof[0, 0]
    uppcol = aprof[:, 1]
    lowcol = aprof[:, 2]
    aprof[:, 1] = np.reciprocal(uppcol * zslice, out=np.zeros_like(uppcol), where=[(a > 0) for a in uppcol])
    aprof[:, 2] = np.reciprocal(lowcol * zslice, out=np.zeros_like(uppcol), where=[(a > 0) for a in lowcol])
    alayers.append(aprof)

    # Get inner/outer number density profiles
    ndinner = {}
    ndouter = {}
    rpos    = ndprof[:, 0]
    for grp in grplist:
        if grp == 'PBD':
            dinn = ndprof[:, ngrps['LOWLAYER']] - ndprof[:, ngrps['PEO']]
            dout = ndprof[:, ngrps['UPPLAYER']] - ndprof[:, ngrps['PEO']]
            ndinner[grp] = np.column_stack((rpos, dinn))[dinn >= 0]
            ndouter[grp] = np.column_stack((rpos, dout))[dout >= 0]
        else:
            dinn = ndprof[:, ngrps[grp]]
            dout = ndprof[:, ngrps[grp]]
            ndinner[grp] = np.column_stack((rpos, dinn))[rpos < rm]
            ndouter[grp] = np.column_stack((rpos, dout))[rpos > rm]

    # Calculate number of inner/outer molecules
    for grp in grplist:  
        nmolinn[grp].append(get_nbeads(ndinner[grp][:, 0], ndinner[grp][:, 1])/nbmol[grp])
        if grp == 'PW' or grp == 'PRIP': 
            nmolout[grp].append(nmols[grp]-nmolinn[grp][-1])
        else:
            nmolout[grp].append(get_nbeads(ndouter[grp][:, 0], ndouter[grp][:, 1])/nbmol[grp])
    
    # Calculate number of encapsulated pPLC in inner/outer leaflets and corresponding mean radii on each region
    if 'PRIP' in grplist:
        dprip = mdprof[:, mgrps['PRIP']]
        npi   = get_pplc_inner(ndinner['PRIP'][:, 0], ndinner['PRIP'][:, 1], nbmol['PRIP'])
        npo   = get_pplc_outer(ndouter['PRIP'][:, 0], ndouter['PRIP'][:, 1], nbmol['PRIP'])
        rpi   = get_inner_radius_prip(rpos, dprip, rm)
        rpo   = get_outer_radius_prip(rpos, dprip, rm)
        pplcinn.append((npi, rpi)) 
        pplcout.append((npo, rpo))

# Calculate average mass densities for last nanosecs
count    = 0
start    = max(0, len(massdens) - lastns)
avgmdens = np.copy(massdens[start])
for dens in massdens[start+1:]:
    avgmdens += dens
    count    += 1
avgmdens /= count

# Calculate average number densities for last nanosecs
count    = 0
start    = max(0, len(numbdens) - lastns)
avgndens = np.copy(numbdens[start])
for dens in numbdens[start+1:]:
    avgndens += dens
    count    += 1
avgndens /= count

## Calculate average area per bead for last nanosecs
#count   = 0
#start  = max(0, len(alayers) - lastns)
#avglay = np.copy(alayers[start])
#for lay in alayers[start+1:]:
#    avglay += lay
#    count  += 1
#avglay /= count

## Calculate area fluctuation for last nanosecs
#count   = 0
#start   = max(0, len(alayers) - lastns)
#rmsdlay = np.zeros_like(avglay[:, [1, 2]])
#for lay in alayers[start:]:
#    diff = lay[:, [1, 2]] - avglay[:, [1, 2]]
#    rmsdlay += diff * diff
#    count   += 1
#rmsdlay = np.sqrt(rmsdlay / count)


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write membrane area, thickness and volume
propsdir = resdir + '/structprops'
if not os.path.isdir(propsdir):
    os.makedirs(propsdir)
outfile = propsdir + '/' + pfxname + '-structprops.txt'

fout = open(outfile, 'w')
sout  = '#{:>11s} '.format('Time')
sout += '{:>12s} '.format('Rinn')
sout += '{:>12s} '.format('Rmean')
sout += '{:>12s} '.format('Rout')
sout += '{:>12s} '.format('Ainn')
sout += '{:>12s} '.format('Aout')
sout += '{:>12s} '.format('MembThick')
sout += '{:>12s} '.format('MembVol')
sout += '\n'
sout += '#{:>11s} '.format('[ns]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[nm^2]')
sout += '{:>12s} '.format('[nm^2]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[nm^3]')
sout += '\n'
fout.write(sout)

time = 0.0
for ri, rm, ro, ai, ao, thk, vol in structprops:
    time += 1.0
    sout  = '{0:>12.2f} '.format(time)
    sout += '{0:>12.4f} '.format(ri)
    sout += '{0:>12.4f} '.format(rm)
    sout += '{0:>12.4f} '.format(ro)
    sout += '{0:>12.4f} '.format(ai)
    sout += '{0:>12.4f} '.format(ao)
    sout += '{0:>12.4f} '.format(thk)
    sout += '{0:>12.4f} '.format(vol)
    sout += '\n'
    fout.write(sout)
fout.close()

# Write number of encapsulated PRIP molecules in inner leaflet
T = 300
R = 8.314472* 1e-3
if re.search('PRIP', pfxname):
    outfile = propsdir + '/' + pfxname + '-encapsulated-PRIP-inner.txt'
    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('Nt')
    sout += '{:>12s} '.format('Ne')
    sout += '{:>12s} '.format('Nw')
    sout += '{:>12s} '.format('PWt')
    sout += '{:>12s} '.format('Rad')
    sout += '{:>12s} '.format('Area')
    sout += '{:>12s} '.format('ne=Ne/Area')
    sout += '{:>12s} '.format('ct=Nt/PWt')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[nm]')
    sout += '{:>12s} '.format('[nm^2]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '{:>12s} '.format('[adim]')
    sout += '\n'
    fout.write(sout)

    time = 0.0
    for n in range(len(nmolinn['PRIP'])):
        time += 1.0
        nwtot = nmolinn['PW'][n]
        nptot = nmolinn['PRIP'][n]
        npenc = pplcinn[n][0]
        rpenc = pplcinn[n][1]
        npwat = nptot-npenc
        apenc = 4*np.pi*rpenc*rpenc
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.2f} '.format(nptot)
        sout += '{0:>12.2f} '.format(npenc)
        sout += '{0:>12.2f} '.format(npwat)
        sout += '{0:>12.2f} '.format(nwtot)
        sout += '{0:>12.4f} '.format(rpenc)
        sout += '{0:>12.4f} '.format(apenc)
        sout += '{0:>12.4f} '.format(npenc/apenc)
        sout += '{0:>12.6f} '.format(1.0*nptot/nwtot) 
        sout += '\n'
        fout.write(sout)
    fout.close()

# Write number of encapsulated PRIP molecules in outer leaflet
T = 300
R = 8.314472* 1e-3
if re.search('PRIP', pfxname):
    outfile = propsdir + '/' + pfxname + '-encapsulated-PRIP-outer.txt'
    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('Nt')
    sout += '{:>12s} '.format('Ne')
    sout += '{:>12s} '.format('Nw')
    sout += '{:>12s} '.format('PWt')
    sout += '{:>12s} '.format('Rad')
    sout += '{:>12s} '.format('Area')
    sout += '{:>12s} '.format('ne=Ne/Area')
    sout += '{:>12s} '.format('ct=Nt/PWt')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[nm]')
    sout += '{:>12s} '.format('[nm^2]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '{:>12s} '.format('[adim]')
    sout += '\n'
    fout.write(sout)

    time = 0.0
    for n in range(len(nmolout['PRIP'])):
        time += 1.0
        nwtot = nmolout['PW'][n]
        nptot = nmolout['PRIP'][n]
        npenc = pplcout[n][0]
        rpenc = pplcout[n][1]
        npwat = nptot-npenc
        apenc = 4*np.pi*rpenc*rpenc
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.2f} '.format(nptot)
        sout += '{0:>12.2f} '.format(npenc)
        sout += '{0:>12.2f} '.format(npwat)
        sout += '{0:>12.2f} '.format(nwtot)
        sout += '{0:>12.4f} '.format(rpenc)
        sout += '{0:>12.4f} '.format(apenc)
        sout += '{0:>12.4f} '.format(npenc/apenc)
        sout += '{0:>12.6f} '.format(1.0*nptot/nwtot) 
        sout += '\n'
        fout.write(sout)
    fout.close()

## Write difference of encapsulated PRIP molecules between outer and inner leaflets
#T = 300
#R = 8.314472* 1e-3
#if re.search('PRIP', pfxname):
#    outfile = propsdir + '/' + pfxname + '-encapsulated-PRIP-diff.txt'
#    fout  = open(outfile, 'w')
#    sout  = '#{:>11s} '.format('Time')
#    sout += '{:>12s} '.format('Ne')
#    sout += '{:>12s} '.format('ne=Ne/Area')
#    sout += '\n'
#    sout += '#{:>11s} '.format('[ns]')
#    sout += '{:>12s} '.format('[adim]')
#    sout += '{:>12s} '.format('[nm^2]')
#    sout += '\n'
#    fout.write(sout)

#    time = 0.0
#    for n in range(len(nmolout['PRIP'])):
#        time += 1.0
#        outenc  = pplcout[n]
#        outarea = 4*np.pi*radinn[n]*radinn[n]
#        innenc  = pplcinn[n]
#        innarea = 4*np.pi*radinn[n]*radinn[n]
#        sout  = '{0:>12.2f} '.format(time)
#        sout += '{0:>12.1f} '.format(outenc-innenc)
#        sout += '{0:>12.3f} '.format(outenc/outarea - innenc/innarea)
#        sout += '\n'
#        fout.write(sout)
#    fout.close()


# Write mass density profiles
outfile = propsdir + '/' + pfxname + '-densprofs-last-' + str(lastns) + 'ns.txt'

fout = open(outfile, 'w')
grpsort = sorted(mgrps.items(), key=lambda x: x[1])
sout = '#{:>11s} '.format('R')
for grp, num in grpsort:
    sout += '{:>12s} '.format(grp)
sout += '\n'
fout.write(sout)

for row in np.arange(avgmdens.shape[0]):
    sout  = '{0:>12.4f} '.format(avgmdens[row, 0])
    for grp, num in grpsort:
        sout += '{0:>12.4f} '.format(avgmdens[row, num])
    sout += '\n'
    fout.write(sout)
fout.close()

# Write num density profiles
outfile = propsdir + '/' + pfxname + '-numbprofs-last-' + str(lastns) + 'ns.txt'

fout = open(outfile, 'w')
grpsort = sorted(ngrps.items(), key=lambda x: x[1])
sout = '#{:>11s} '.format('R')
for grp, num in grpsort:
    sout += '{:>12s} '.format(grp)
sout += '\n'
fout.write(sout)

for row in np.arange(avgndens.shape[0]):
    sout  = '{0:>12.4f} '.format(avgndens[row, 0])
    for grp, num in grpsort:
        sout += '{0:>12.4f} '.format(avgndens[row, num])
    sout += '\n'
    fout.write(sout)
fout.close()



#######################################################################################################
############################################## BACKUP #################################################
#######################################################################################################

## Write number of molecules and radius for each leaflet
#T = 300
#R = 8.314472* 1e-3
#for grp in ['PEO', 'PBD', 'PRIP']:
#    if not re.search('PRI[NP]', grp) or re.search(grp, pfxname):
#        outfile = propsdir + '/' + pfxname + '-leaflets-' + grp + '.txt'
#        fout = open(outfile, 'w')
#        if grp != 'PRIP':
#            sout  = '#{:>11s} '.format('Time')
#            sout += '{:>12s} '.format('Ni')
#            sout += '{:>12s} '.format('Ri')
#            sout += '{:>12s} '.format('Ni/Ai=ni')
#            sout += '{:>12s} '.format('No')
#            sout += '{:>12s} '.format('Ro')
#            sout += '{:>12s} '.format('No/Ao=no')
#            sout += '{:>12s} '.format('no/ni')
#        else:
#            sout  = '#{:>11s} '.format('Time')
#            sout += '{:>12s} '.format('NTi')
#            sout += '{:>12s} '.format('NEi')
#            sout += '{:>12s} '.format('NWi')
#            sout += '{:>12s} '.format('Ri')
#            sout += '{:>12s} '.format('ni=NEi/Ai')
#            sout += '{:>12s} '.format('ci=NTi/PWi')
#            sout += '{:>12s} '.format('NTo')
#            sout += '{:>12s} '.format('NEo')
#            sout += '{:>12s} '.format('NWo')
#            sout += '{:>12s} '.format('Ro')
#            sout += '{:>12s} '.format('no=NEo/Ao')
#            sout += '{:>12s} '.format('co=NTo/PWo')
#            sout += '{:>12s} '.format('no/ni')
#            sout += '{:>12s} '.format('DG_Ni')
#            sout += '{:>12s} '.format('DG_No')
#        sout += '\n'
#        if grp != 'PRIP':
#            sout += '#{:>11s} '.format('[ns]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[nm]')
#            sout += '{:>12s} '.format('[nm^-2]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[nm]')
#            sout += '{:>12s} '.format('[nm^-2]')
#            sout += '{:>12s} '.format('[adim]')
#        else:
#            sout += '#{:>11s} '.format('[ns]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[nm]')
#            sout += '{:>12s} '.format('[nm^-2]')
#            sout += '{:>12s} '.format('[%]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[nm]')
#            sout += '{:>12s} '.format('[nm^-2]')
#            sout += '{:>12s} '.format('[%]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[adim]')
#            sout += '{:>12s} '.format('[KJ/mol]')
#            sout += '{:>12s} '.format('[KJ/mol]')
#        sout += '\n'
#        fout.write(sout)

#        time = 0.0
#        for n in range(len(nmolinn[grp])):
#            time += 1.0
#            if grp != 'PRIP':
#                ni    = nmolinn[grp][n]
#                ri    = radinn[n]
#                ai    = 4*np.pi*ri*ri
#                no    = nmolout[grp][n]
#                ro    = radout[n]
#                ao    = 4*np.pi*ro*ro
#                sout  = '{0:>12.2f} '.format(time)
#                sout += '{0:>12.0f} '.format(round(ni))
#                sout += '{0:>12.3f} '.format(ri)
#                sout += '{0:>12.3f} '.format(ni/ai)
#                sout += '{0:>12.0f} '.format(round(no))
#                sout += '{0:>12.3f} '.format(ro)
#                sout += '{0:>12.3f} '.format(no/ao)
#                sout += '{0:>12.3f} '.format((no/ao)*(ai/ni))
#            else:
#                ni    = nmolinn[grp][n]
#                wi    = nmolinn['PW'][n]
#                pi    = pplcinn[n]
#                ri    = radinn[n]
#                ai    = 4*np.pi*ri*ri
#                no    = nmolout[grp][n]
#                wo    = nmolout['PW'][n]
#                po    = pplcout[n]
#                ro    = radout[n]
#                ao    = 4*np.pi*ro*ro
#                sout  = '{0:>12.2f} '.format(time)
#                sout += '{0:>12.0f} '.format(round(ni))
#                sout += '{0:>12.0f} '.format(round(pi))
#                sout += '{0:>12.0f} '.format(round(ni-pi))
#                sout += '{0:>12.3f} '.format(ri)
#                sout += '{0:>12.3f} '.format(pi/ai)
#                sout += '{0:>12.3f} '.format(100*ni/wi)
#                sout += '{0:>12.0f} '.format(round(no))
#                sout += '{0:>12.0f} '.format(round(po))
#                sout += '{0:>12.0f} '.format(round(no-po))
#                sout += '{0:>12.3f} '.format(ro)
#                sout += '{0:>12.3f} '.format(po/ao)
#                sout += '{0:>12.3f} '.format(100*no/wo)
#                sout += '{0:>12.3f} '.format((po/ao)*(ai/pi))          
#                sout += '{0:>12.3f} '.format(-R*T*np.log(1.0*pi/(ni-pi)))
#                sout += '{0:>12.3f} '.format(-R*T*np.log(1.0*no/(no-po)))
#            sout += '\n'
#            fout.write(sout)
#        fout.close()

## Write density profiles
#outfile = propsdir + '/' + pfxname + '-densprofs-last-' + str(lastns) + 'ns.txt'

#fout = open(outfile, 'w')
#grpsort = sorted(mgrps.items(), key=lambda x: x[1])
#sout  = '#{:>11s} | '.format(' ')
#sout += '{:>12s} '.format(' ')
#sout += '{:^25s} '.format(' Number density')
#sout += '{:>12s} | '.format(' ')
#sout += 'Mass density'.center(sum([13 for grp, num in grpsort]))
#sout += '\n'
#sout += '#{:>11s} | '.format('R')
#sout += '{:>12s} '.format('AREA_UPP')
#sout += '{:>12s} '.format('RMSD_UPP')
#sout += '{:>12s} '.format('AREA_LOW')
#sout += '{:>12s} | '.format('RMSD_LOW')
#for grp, num in grpsort:
#    sout += '{:>12s} '.format(grp)
#sout += '\n'
#fout.write(sout)

#for row in np.arange(avglay.shape[0]):
#    sout  = '{0:>12.4f} | '.format(avglay[row, 0])
#    sout += '{0:>12.4f} '.format(avglay[row, 1])
#    sout += '{0:>12.4f} '.format(rmsdlay[row, 0])
#    sout += '{0:>12.4f} '.format(avglay[row, 2])
#    sout += '{0:>12.4f} | '.format(rmsdlay[row, 1])
#    for grp, num in grpsort:
#        sout += '{0:>12.4f} '.format(avgdens[row, num])
#    sout += '\n'
#    fout.write(sout)
#fout.close()

