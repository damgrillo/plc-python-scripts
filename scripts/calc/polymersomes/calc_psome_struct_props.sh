#!/bin/bash

# Global vars
SCRIPTS_DIR=$(cd $(dirname $0) && pwd)

# Set prefix name
POLYNAME='PBD22-PEO14'
INPDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/chpc/psome/inp # Input GRO files containing the systems coordinates
RAWDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/chpc/psome/res # Path to store the raw data from the simulations (trajectories, density and pressure profiles, etc.)
RESDIR=/Users/damiangrillo/Documents/PhD/Repos/plc-python-scripts/data/polymersomes # Path to store the results of the different properties calculated for the simulations
LASTNS=1500 # Use last nanoseconds of trajectory

# Membrane structural properties
for PFX in $(ls $RAWDIR/densprofs | grep -v 'txt' | sort -V); do
    echo "Calculating $PFX"  
    GRO=$INPDIR/$PFX.gro
    DENSDIR=$RAWDIR/densprofs/$PFX # Density profiles for current system (raw data)
    APCHDIR=$RAWDIR/apchainprofs/$PFX # Area per chain profiles for current system (raw data)
    NCINN=815 # Number of polymer chains in inner leaflet
    NCOUT=2326 # Number of polymer chains in outer leaflet
    NMPRIN=$(cat $GRO | grep '[0-9]\+PRIN' | awk '{print $1}' | uniq | wc -l)
    NMPRIP=$(cat $GRO | grep '[0-9]\+PRIP' | awk '{print $1}' | uniq | wc -l)
    NMWAT=$(cat $GRO  | grep '[0-9]\+PW'   | awk '{print $1}' | uniq | wc -l)
    python $SCRIPTS_DIR/calc_psome_struct_props.py \
           $DENSDIR $APCHDIR $RESDIR $NTMOLS $NMPRIN $NMPRIP $NMWAT $PFX $LASTNS $NCINN $NCOUT
done


