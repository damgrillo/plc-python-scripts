import os
import re
import sys
import math
import string
import random
import numpy as np
from operator import itemgetter
from itertools import izip
from matplotlib import pyplot as plt, rcParams
from scipy.optimize import curve_fit, minimize

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'


###########################################################################################################
################################################## FUNCTIONS ##############################################
###########################################################################################################

# Get density profiles
def get_densprofs(dnsfile):
    fdens   = open(dnsfile, 'r')
    fread   = [line.strip() for line in fdens] 
    fdata   = [line for line in fread if not re.match('#|@', line)]
    fhead   = [col for col in fread[0].lstrip('#').split()]
    dnscols = {label:col for col, label in enumerate(fhead)}     
    dnsdata = [map(float, line.split()) for line in fdata] 
    dnsdata = np.array(dnsdata)
    fdens.close()
    return {label:dnsdata[:, col] for label, col in dnscols.items()}

# Get mean radius
def get_mean_radius(rp, dp):    
    rm = np.trapz(y=dp*rp, x=rp) / np.trapz(y=dp, x=rp)
    return rm

# Get spherical harmonics spectrum
def get_spha_spectrum(ssfile):
    fs = open(ssfile, 'r')
    ss = [map(float, line.split()) for line in fs if not re.match('#|@', line)] 
    ss = np.array(ss)
    fs.close()
    return ss[ss[:, 0] >= 2]

# Get pressure moments info
def get_pminfo(pmfile):
    fpm    = open(pmfile, 'r')
    pmread = [line.strip() for line in fpm] 
    pmcols = {label:col for col, label in enumerate(pmread[0].lstrip('#').split())}     
    pmdata = [map(float, line.split()) for line in pmread if not re.match('#|@', line)] 
    pmdata = np.array(pmdata)
    fpm.close()
    return {label:pmdata[:, [0, col]] for label, col in pmcols.items()}

# Get ideal radius r0
def get_rzero(ravgfile):
    fr    = open(ravgfile, 'r')
    rdata = [line.split() for line in fr if not re.match('#|@', line)]
    rdata = [(re.search('job-([0-9]+)', rd[0]).group(1), rd[-1]) for rd in rdata]
    rzero = np.array([map(float, rd) for rd in rdata]) 
    fr.close()
    return rzero

# Calculate spontaneous curvature for Helfrich model
# NOTE: Mean curvature is defined as c=c1+c2 in the Helfrich model, but c=(c1+c2)/2 in Nakamura one
#       The spontaneous curvatures follows the same definitions, 
#       but with opposite sign due to the chosen convention ==> cs(Helf) = -2*cs(Nak)
#       From Nakamura, cs(Nak) = -2/rs + (m0*rs)/(2*kc) ==> cs(Helf) = 2/rs - m0*rs/kc
def calc_cs_helf(kc, m0, rs):
    return 2/rs - m0*rs/kc

# Logaritmic spherical harmonics spectrum
# NOTE: If kbt = 1 ==> [kc] = kbT
def logsl(pars, l, m0, r0, rs, kbt, use_cs=True):
    u  = l*(l+1)
    du = u*u - 2*u
    kc = pars[0]
    cs = calc_cs_helf(pars[0], m0, rs)
    if use_cs:
        du += (2-u)*cs*r0
    return np.log(kbt) - np.log(kc) - np.log(du) 

# Logaritmic spherical harmonics spectrum difference
# NOTE: If kbt = 1 ==> [kc] = kbT
def logsl_sqdiff(pars, l, lslobs, m0, r0, rs, kbt, use_cs=True):
    ldiff = logsl(pars, l, m0, r0, rs, kbt, use_cs) - lslobs
    return np.dot(ldiff, ldiff)

# Calculate bending modulus from spherical harmonics spectrum
def calc_kc_cs(ssavg, ssstd, m0, r0, rs, kbt, lcut):
    ssa   = ssavg[ssavg[:, 0] < lcut]
    sss   = ssstd[ssavg[:, 0] < lcut]
    l     = ssa[:, 0]
    lsla  = np.log(ssa[:, 1])
    lsls  = np.log(sss[:, 1])
    #cons  = {'type': 'eq', 'fun': lambda pars:  pars[1] - calc_cs_helf(pars[0], m0, rs)}
    #pfit  = minimize(logsl_sqdiff, x0=(1.0), args=(l, lsla, m0, r0, rs, kbt), method='SLSQP') #, constraints=cons) #, sigma=lsls)
    pfit  = curve_fit(lambda l, kc: logsl([kc], l, m0, r0, rs, kbt), l, lsla, p0=(1.0))
    kcfit = (pfit[0][0], np.sqrt(pfit[1][0, 0]))
    csfit = calc_cs_helf(kcfit[0], m0, rs)
    return kcfit, csfit

# Plot fitted spectrum
def plot_fitted_spha_spectrum(pltfile, pltdata, kbt):
    nf = -1
    xlabel = '$l$'
    ylabel = '$S(l)$'
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True, squeeze=False)
    for t, ssa, sss, pfit, m0, r0, rs in pltdata:
        nf    += 1
        nr     = nf%2
        nc     = nf/2
        l      = ssa[:, 0]
        slobs  = ssa[:, 1]
        slerr  = sss[:, 1]
        slfit  = np.exp(logsl(pfit[0], l, m0, r0, rs, kbt))
        #finfo  = '$t\;\;$ = ${{{0:0.0f}}}$'.format(t) + ' $\mathsf{ns}$' + '\n'
        finfo  = '$\kappa$ = ${{{0:0.1f}}} \pm {{{1:0.1f}}}$'.format(*pfit[0]) + ' $k_{B}T$' #+ '\n' 
        #finfo += '$c_s$ = ${{{0:0.1f}}}$'.format(pfit[1]) + ' $\mathsf{nm^{-1}}$'
        trax   = axes[nr, nc].transAxes
        bbdic  = {'edgecolor':'k', 'facecolor':'wheat', 'boxstyle':'square', 'alpha':0.9, 'pad':0.3}
        axes[nr, nc].set_xscale("log")
        axes[nr, nc].set_yscale("log")
        axes[nr, nc].errorbar(l, slobs, slerr, color='k', ls='', marker='o', ms=5.0, elinewidth=1.2, capsize=1.8, label='Datos') 
        axes[nr, nc].plot(l, slfit, color='r', ls='-', marker='', lw=2.0, label='Ajuste')
        axes[nr, nc].legend(fontsize=18, numpoints=1)
        axes[nr, nc].text(0.06, 0.06, finfo, ha='left', va='bottom', size=22, transform=trax, bbox=bbdic)
        axes[nr, nc].grid(which='major', axis='both')
        axes[nr, nc].grid(which='minor', axis='x')
        axes[nr, nc].margins(0.03)
    axes[-1, 0].set_xlim(1.8, 100)
    #axes[-1, 0].set_ylim(0.3e-4, 8e0)
    axes[-1, 0].set_xlabel(xlabel, size=23, labelpad=12)
    axes[ 0, 0].set_ylabel(ylabel, size=23, labelpad=15)
    #axes[ 1, 0].set_ylabel(ylabel, size=23, labelpad=15)
    #axes[ 2, 0].set_ylabel(ylabel, size=23, labelpad=15)
    fig.set_dpi(200)
    fig.set_size_inches(9, 7)
    fig.tight_layout(h_pad=0.0, w_pad=0.0)
    fig.subplots_adjust()
    fig.savefig(pltfile)
    return


###########################################################################################################
################################################# MAIN PROGRAM ############################################
###########################################################################################################

# Input variables
dnsfile   = str(sys.argv[1])
ssdir     = str(sys.argv[2])
pmfile    = str(sys.argv[3])
ravgfile  = str(sys.argv[4])
kbfile    = str(sys.argv[5])
pltfile   = str(sys.argv[6])
twin      = int(sys.argv[7])

# Units conversion
# 1 mN/m     = 10 bar*nm
# 1 bar*nm3  = 1e-2 * 1e-20 J
# 1 kb*T     = 1e-3 * 1.3806 * Temp * 1e-20 J 
# 1 kb*T     = 1e-1 * 1.3806 * Temp * bar*nm^3

# Global parameters
kbt        = 1
lcut       = 50
temp       = 300
kbt2barnm3 = 1.3806488 * 1e-1 * temp # Convert kbT to bar*nm^3
kbt2J      = 1.3806488 * 1e-3 * temp # Convert kbT to 1e-20 J

# Get density profiles, inner and outer radius
densprofs = get_densprofs(dnsfile)
densrad   = densprofs['R']
densinner = densprofs['LOWLAYER']
densouter = densprofs['UPPLAYER']
rinn      = get_mean_radius(densrad, densinner)
rout      = get_mean_radius(densrad, densouter)

# Get spherical harmonics spectra and pressure moments info
ssfiles = [os.path.join(ssdir, sf) for sf in sorted(os.listdir(ssdir)) if re.search('sphaspectrum', sf)]
sspecs  = [get_spha_spectrum(sf) for sf in ssfiles]
times   = [int(re.search('job-([0-9]+)', sf).group(1)) for sf in ssfiles]
pminfo  = get_pminfo(pmfile)
m0list  = pminfo['m0']
rslist  = pminfo['rs']
r0list  = get_rzero(ravgfile)

# Convert m0 to kbT/nm^2
m0list[:, 1] /= kbt2barnm3

# Average properties for each time window
tstart   = times[0]
avgtimes = np.arange(0, len(sspecs), twin)
avgspecs = [np.mean(sspecs[t:t+twin], axis=0) for t in avgtimes]
stdspecs = [np.std(sspecs[t:t+twin], axis=0) for t in avgtimes]
avgm0    = [np.mean(m0list[(t < m0list[:, 0]) & (m0list[:, 0] <= t+twin)][:, 1], axis=0) for t in avgtimes+tstart]
avgr0    = [np.mean(r0list[(t < r0list[:, 0]) & (r0list[:, 0] <= t+twin)][:, 1], axis=0) for t in avgtimes+tstart]
avgrs    = [np.mean(rslist[(t < rslist[:, 0]) & (rslist[:, 0] <= t+twin)][:, 1], axis=0) for t in avgtimes+tstart]

# Calculate kbend from Fourier spectra
kbends  = []
pltdata = []
samples = set(random.sample(avgtimes, 1))
#samples = [150.0, 424.0, 430.0, 1151.0, 1486.0, 1925.0]
for t, ssa, sss, m0, r0, rs in izip(avgtimes, avgspecs, stdspecs, avgm0, avgr0, avgrs):
    pfit = calc_kc_cs(ssa, sss, m0, r0, rs, kbt, lcut)
    if t in samples:
        pltdata.append((t, ssa, sss, pfit, m0, r0, rs))
    kbends.append((t, pfit)) 

# Plot fitted spectra
plot_fitted_spha_spectrum(pltfile, pltdata, kbt)


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write kbends
fout = open(kbfile, 'w')
sout  = '#{:>11s} '.format('Time')
sout += '{:>12s} '.format('kbend_avg')
sout += '{:>12s} '.format('kbend_std')
sout += '{:>12s} '.format('kbend_avg')
sout += '{:>12s} '.format('kbend_std')
sout += '\n'
sout += '#{:>11s} '.format('[ns]')
sout += '{:>12s} '.format('[kb*T]')
sout += '{:>12s} '.format('[kb*T]')
sout += '{:>12s} '.format('[1e-20 J]')
sout += '{:>12s} '.format('[1e-20 J]')
sout += '\n'
fout.write(sout)

for t, pfit in kbends:
    kcfit = pfit[0]
    sout  = '{0:>12.2f} '.format(t)
    sout += '{0:>12.3f} '.format(kcfit[0])
    sout += '{0:>12.3f} '.format(kcfit[1])
    sout += '{0:>12.3f} '.format(kcfit[0]*kbt2J)
    sout += '{0:>12.3f} '.format(kcfit[1]*kbt2J)
    sout += '\n'
    fout.write(sout)

fout.close()

