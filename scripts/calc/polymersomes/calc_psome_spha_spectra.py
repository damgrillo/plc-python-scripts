import re
import sys
import math
import cmath
import string
import numpy as np
import matplotlib.pyplot as plt
from itertools import izip
from operator import itemgetter
from pyshtools import SHGrid
from pyshtools.shtools import GLQGridCoord, MakeGridPoint, SHCilmToVector
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D

###########################################################################################################
################################################# FUNCTIONS ###############################################
###########################################################################################################

# Cartesian to spherical coordinates
def cart2sph(ccoords):
    x   = ccoords[:, 0]
    y   = ccoords[:, 1]
    z   = ccoords[:, 2]
    l   = np.sqrt(x*x + y*y)
    r   = np.sqrt(x*x + y*y + z*z) 
    tht = np.arcsin(z/r) * (180/np.pi)
    phi = np.arccos(x/l) * (180/np.pi)
    phi = np.where(y < 0, 360-phi, phi) 
    return np.column_stack((tht, phi, r))

# Spherical to cartesian coordinates
def sph2cart(scoords):
    tht = scoords[:, 0] * (np.pi/180)
    phi = scoords[:, 1] * (np.pi/180)
    r   = scoords[:, 2]
    x   = r*np.cos(tht)*np.cos(phi) 
    y   = r*np.cos(tht)*np.sin(phi) 
    z   = r*np.sin(tht)
    return np.column_stack((x, y, z)) 

# Interpolate surface
def interpolate_surface(surf3d, grid2d, itype):
    xysurf  = surf3d[:, :2]
    zsurf   = surf3d[:, 2]
    zmean   = np.mean(zsurf)
    zgrid   = griddata(xysurf, zsurf, grid2d, method=itype, fill_value=zmean)
    return np.column_stack((grid2d, zgrid))

# Write xvg file
def write_xvg_spha_spectrum(outfile, sphaspec):
    fout  = open(outfile, 'w')
    sout  = '#{0:>11s} {1:>12s}\n'.format('l', 'Slm')
    sout += '#{0:>11s} {1:>12s}\n'.format('[nm^-1]', '[nm^2]')
    fout.write(sout)
    for l, slm in sphaspec:
        sout  = '{0:>12.4f} {1:>12.4e}\n'.format(l, slm)
        fout.write(sout)
    fout.close()

# Write gro file
def write_gro_file(outfile, outorig, innorig, outsurf, innsurf, rundsurf, rundspha, boxlen):

    # Write header
    fout  = open(outfile, 'w')
    sout  = 'Evaluate spherical harmonics\n'
    fout.write(sout)

    # Write original coordinates 
    ncrd = 0
    sout = ''
    for rx, ry, rz in outorig:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('1ORI', 'O', ncrd, rx, ry, rz)
    for rx, ry, rz in innorig:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('2ORI', 'O', ncrd, rx, ry, rz)

    # Write interpolated coordinates
    for rx, ry, rz in outsurf:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('3OIP', 'I', ncrd, rx.real, ry.real, rz.real)
    for rx, ry, rz in innsurf:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('4IIP', 'I', ncrd, rx.real, ry.real, rz.real)
    for rx, ry, rz in rundsurf:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('5MIP', 'I', ncrd, rx.real, ry.real, rz.real)

    # Write spherical harmonics coordinates
    for rx, ry, rz in rundspha:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('8MSH', 'S', ncrd, rx.real, ry.real, rz.real)
    fout.write(str(ncrd) + '\n')
    fout.write(sout)

    # Write box length
    sout  = '{0:>10.5f}{1:>10.5f}{2:>10.5f}\n'.format(boxlen[0], boxlen[1], boxlen[2])
    fout.write(sout)
    fout.close()

# Write average radius file
def write_ravg_file(outfile, jobname, ravg, r0):
    fout  = open(outfile, 'a')
    sout  = '{0:>12s} {1:>12.3f} {2:>12.3f}\n'.format(jobname, ravg, r0)
    fout.write(sout)
    fout.close()


# Plot surfaces
def plot_surfaces(outfile, origcrds, intpcrds, sphacrds,  nrows, ncols):
    fig = plt.figure()
    intxgrid = np.reshape(intpcrds[:, 0], (nrows, ncols))
    intygrid = np.reshape(intpcrds[:, 1], (nrows, ncols))
    intzgrid = np.reshape(intpcrds[:, 2], (nrows, ncols))
    ax  = fig.add_subplot(111, projection='3d')
    ax.plot_wireframe(intxgrid, intygrid, intzgrid, linewidths=0.5, colors='y', rstride=1, cstride=1)
    ax.scatter(origcrds[:, 0], origcrds[:, 1], origcrds[:, 2], s=5, marker='o', facecolors='none', edgecolors='r', depthshade=False)
    ax.scatter(sphacrds[:, 0], sphacrds[:, 1], sphacrds[:, 2], s=5, marker='o', facecolors='none', edgecolors='g', depthshade=False)
    ax.view_init(elev=20, azim=30)
    fig.set_dpi(200)
    fig.set_size_inches(10, 10)
    fig.tight_layout(h_pad=0.0)
    fig.savefig(outfile) 


###########################################################################################################
############################################# MAIN PROGRAM ################################################
###########################################################################################################

# Input parameters
inpfile  = str(sys.argv[1])
specfile = str(sys.argv[2])
ravgfile = str(sys.argv[3])
gridfac  = float(sys.argv[4])
itype    = str(sys.argv[5])

# Patterns
boxpatt = 'box\[\s*([0-2])\]=\{(.*), (.*), (.*)\}'
crdpatt = 'x\[.*\]=\{(.*),(.*),(.*)\}'

# Read file
frames = []
finp   = open(inpfile, 'r')
for line in finp:

    # Read new frame
    if re.search('step=', line):
        frames.append({'boxlen':{}, 'coords':[]}) 
        boxlen = frames[-1]['boxlen']
        coords = frames[-1]['coords']

    # Read box length
    elif re.search('box\[', line):
        bgrps = re.search(boxpatt, line).groups()
        axis  = int(bgrps[0])
        blen  = float(bgrps[axis+1])
        boxlen[axis] = blen

    # Read coordinates
    elif re.search('x\[', line):
        cgrps = re.search(crdpatt, line).groups()  
        coords.append(map(float, cgrps))

# Calculate Fourier spectra for each frame
for fr in frames:

    # Initialize frame data
    fr['coords'] = np.array(fr['coords'])
    fr['boxlen'] = np.array([fr['boxlen'][i] for i in range(3)])

    # Boxlen and coordinates
    boxlen  = fr['boxlen']
    coords  = fr['coords']
    coords -= np.mean(coords, axis=0)

    # Fit all molecules in the box using pbc and recenter coords
    coords += 0.5*boxlen
    coords -= np.floor_divide(coords, boxlen) * boxlen
    coords -= np.mean(coords, axis=0) 

    # Calculate radii and radial density
    nsh   = 200
    radii = np.sqrt(np.sum(coords*coords, axis=1))
    rlen  = radii.shape[0]
    rmax  = np.amax(radii)
    wsh   = rmax / nsh
    rsh   = np.arange(nsh) * wsh
    dsh   = np.zeros(nsh)    
    for r in radii:
        ish = int(min(r/wsh, nsh-1))
        vsh = (4.0/3) * np.pi * wsh**3 * (3*ish*ish + 3*ish + 1)  
        dsh[ish] += 1.0/vsh       

    # Separate into inner and outer interfaces
    rmean         = np.sum(rsh*dsh) / np.sum(dsh)
    fr['inncrds'] = coords[radii < rmean]
    fr['outcrds'] = coords[radii > rmean]
    inncrds       = fr['inncrds']
    outcrds       = fr['outcrds']

    # Calculate grid points for theta and phi coordinates
    # We assume equal grid spacing in theta and phi: dtht = dphi = dang
    # Since dtht = 180/ntht, dphi = 360/nphi => nphi = 2*ntht 
    # Then, narea = ntht*nphi = 2*ntht^2 => ntht = sqrt(narea/2)
    gtype = 'GLQ'
    narea = max(inncrds.shape[0], outcrds.shape[0])
    ntht  = int(np.sqrt(0.5*narea)*gridfac)
    if gtype == 'GLQ':
        tgrid, pgrid = GLQGridCoord(ntht)
    else:
        dang     = (180.0/(ntht+1) if ntht%2 == 1 else 180.0/ntht)
        tgrid    = np.arange(90, -90, -dang)
        pgrid    = np.arange(0, 360, dang)

    # Set theta-phi grid and interpolate surfaces to obtain the undulating surface rund(tht, phi)
    tpgrid         = np.array([(tht, phi) for tht in tgrid for phi in pgrid])
    innsurf        = interpolate_surface(cart2sph(inncrds), tpgrid, itype)
    outsurf        = interpolate_surface(cart2sph(outcrds), tpgrid, itype)
    rundsurf       = 0.5*(innsurf+outsurf)
    fr['outsurf']  = sph2cart(outsurf)
    fr['innsurf']  = sph2cart(innsurf) 
    fr['rundsurf'] = sph2cart(rundsurf)

    # Calculate normalized undulating surface fnorm(tht, phi) using the average radius rund0
    rund0      = np.mean(rundsurf[:, 2])
    fnormsurf  = np.column_stack((rundsurf[:, :2], (rundsurf[:, 2]-rund0)/rund0))    
    fr['ravg'] = rund0

    # Calculate spherical harmonics coefficients and spectrum for fnorm(tht, phi)
    ntht           = tgrid.shape[0]
    nphi           = pgrid.shape[0]
    fnormgrid      = SHGrid.from_array(np.reshape(fnormsurf[0:, 2], (ntht, nphi)), grid='GLQ')
    fnormcoeffs    = fnormgrid.expand()
    ldegrees       = fnormcoeffs.degrees()
    sphaspec       = fnormcoeffs.spectrum(unit='per_lm')
    fr['sphaspec'] = np.column_stack((ldegrees, sphaspec))

#    # Evaluate spherical harmonics on original points
#    rundspha  = []
#    coefs2arr = fnormcoeffs.to_array()
#    for tht, phi, r in cart2sph(outcrds):
#        rspha = MakeGridPoint(coefs2arr, tht, phi)*rund0 + rund0
#        rundspha.append((tht, phi, rspha))
#    fr['rundspha'] = sph2cart(np.array(rundspha))


###########################################################################################################
############################################## OUTPUT FILES ###############################################
###########################################################################################################

# Jobname, average spectra, average radius and ideal radius
jobname = re.search('(job-[0-9]+)', inpfile).group(1)
spavg   = np.mean([fr['sphaspec'] for fr in frames], axis=0)
ravg    = np.mean([fr['ravg'] for fr in frames])
l, slm  = spavg[1:, 0], spavg[1:, 1]
r0      = ravg * np.sqrt(1 + 1/(4*np.pi) * np.sum((2*l+1) * (1 + 0.5*l*(l+1))*slm))

# Write average spherical harmonics spectrum
write_xvg_spha_spectrum(specfile, np.absolute(spavg))
write_ravg_file(ravgfile, jobname, ravg, r0)

## Test interpolated surface and reconstructed surface from spherical harmonics
#fr       = frames[0]
#outorig  = fr['outcrds']
#innorig  = fr['inncrds']
#outsurf  = fr['outsurf']
#innsurf  = fr['innsurf']
#rundsurf = fr['rundsurf']
#rundspha = fr['rundspha']
#outf     = outfile.replace("sphaspectrum.xvg", "eval-spha.gro")
#pdff     = outfile.replace("sphaspectrum.xvg", "eval-spha.pdf")
#write_gro_file(outf, outorig, innorig, outsurf, innsurf, rundsurf, rundspha, boxlen) 
##plot_surfaces(pdff, outorig, outsurf, outspha, len(outgrid.lats()), len(outgrid.lons()))

## Test coefficients array and spectrum
#fcfarray = fnormcoeffs.to_array()
#for l in np.arange(fcfarray.shape[1]):
#    clm = fcfarray[0][l]
#    slm = fcfarray[1][l]
#    plm = np.hstack((clm*clm, slm*slm))
#    print "Slm_calc = ", np.mean(plm[plm > 0]), "; nm =", plm[plm > 0].shape, "; l =", l
#    print "Slm_spec = ", sphaspec[l], "; nm =", plm[plm > 0].shape, "; l =", l
#    print



