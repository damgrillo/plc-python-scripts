#!/bin/bash

# Global vars
SCRIPTS_DIR=$(cd $(dirname $0) && pwd)
PSOME_OUTDIR= # Path to store output raw data from polymersomes simulations
SPHA_SPECTRA_DIR= # Path to store output Spherical harmonics spectra calculated in this script

# Set poly name and dir name
POLYNAME='PBD22-PEO14'
TMPDIR=$SPHA_SPECTRA_DIR/tmp
mkdir -p $TMPDIR

# Get Spherical harmonics spectra of membrane fluctuations
for PFX in $(ls $WORKDIR/$IDIR/out | grep "$POLYNAME" | sort -V); do

    # Create temp dir
    for GRIDFAC in 2.5; do
        for ITYPE in linear; do

            OUTDIR=$PSOME_OUTDIR/$PFX
            SPHADIR=$SPHA_SPECTRA_DIR/$PFX
            RAVG=$SPHADIR/$PFX-ravg.txt
            mkdir -p $SPHADIR
            rm $TMPDIR/*
            echo $PFX-$ITYPE-interp-ngrid-${GRIDFAC}x

            # Header for average radii file
            if [ ! -f $RAVG  ]; then 
                printf "#%11s %12s %12s\n" "jobname" "ravg" "r0" > $RAVG
            fi

            # Loop on xtc files
            NPJOBS=0
            FNAMES=$(ls $OUTDIR | grep 'pbdsurf' | grep -o 'job-[0-9]\+' | sort -V | uniq)
            for FNM in $FNAMES; do

                # Set files
                XTC=$OUTDIR/$FNM-pbdsurf.xtc
                DMP=$TMPDIR/$FNM-dump.txt
                XVG=$SPHADIR/$FNM-sphaspectrum.xvg

                # Calculate amplitudes of spherical harmonics spectra in parallel jobs
                if [ ! -f $XVG  ]; then 
                    echo "Calculating $FNM ... "
                    gmx dump -f $XTC > $DMP 2> /dev/null
                    python $SCRIPTS_DIR=$(cd $(dirname $0) && pwd)/calc_psome_spha_spectra.py $DMP $XVG $RAVG $GRIDFAC $ITYPE &
                    NPJOBS=$((NPJOBS+1))
                else
                    echo "$FNM already done ... "
                fi

                # Wait for jobs
                if [ $NPJOBS -eq 3 ]; then
                    wait
                    NPJOBS=0
                fi        

            done
            wait

        done
    done
done

# Clean temp files
rm -r $TMPDIR

