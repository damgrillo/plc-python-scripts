#!/bin/bash

# Global vars
SCRIPTS_DIR=$(cd $(dirname $0) && pwd)

# Set prefix name
POLYNAME='PBD22-PEO14'
IDIR=psome
AVGWIN=250
LASTNS=1500
PMETHOD=lpmerged
INPDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/chpc/psome/inp # Input GRO files containing the systems coordinates
RAWDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/chpc/psome/res # Path to store the raw data from the simulations (trajectories, density and pressure profiles, etc.)
RESDIR=/Users/damiangrillo/Documents/PhD/Repos/plc-python-scripts/data/polymersomes # Path to store the results of the different properties calculated for the simulations
STRDIR=$RESDIR/structprops # Structural properties path
MECHDIR=$RESDIR/mechprops # Mechanical properties path
ELASTFILE=$MECHDIR/PRILO-elastprops-avgwin-${AVGWIN}ns.txt # Elastic properties file
mkdir -p $MECHDIR
rm $ELASTFILE

# Membrane mechanic properties
for PFX in $(ls $RAWDIR/densprofs | grep -v 'txt' | sort -V); do
    GRO=$INPDIR/$PFX.gro
    NCPOLY=$(cat $GRO | grep 'PLY' | awk '{print $1}' | sort -V | uniq | wc -l)
    DENSDIR=$RAWDIR/densprofs/$PFX # Density profiles for current system (raw data)
    PRESDIR=$RAWDIR/presprofs/$PFX # Pressure profiles for current system (raw data)
    SPHADIR=$RAWDIR/sphaspectra/$PFX # Spherical harmonics spectra for current system (raw data)
    DNSFILE=$STRDIR/$PFX-densprofs-last-${LASTNS}ns.txt # average density profile on last nanoseconds for current system
    INNENCFILE=$STRDIR/$PFX-encapsulated-PRIP-inner.txt # Encapsulated PRIP in inner leaflet for current system
    OUTENCFILE=$STRDIR/$PFX-encapsulated-PRIP-outer.txt # Encapsulated PRIP in outer leaflet for current system
    mkdir -p $MECHDIR/$PFX

    # # Calculate pressure properties using a time window of 250 ns
    # echo "Calculating presprops for $PFX ..."
    # TWIN=250
    # python3 $SCRIPTS_DIR/calc_psome_presprops.py $RESDIR $PRESDIR $PFX $TWIN $PMETHOD

    # # Calculate kbend parameters 
    # echo "Calculating kbend for $PFX ..."
    # TWIN=1500
    RAVGFILE=$MECHDIR/$PFX/ravg.txt
    KBENDFILE=$MECHDIR/$PFX/kbend.txt
    # PLOTSAMPLE=$MECHDIR/$PFX-kbend-fitsample.pdf
    # PRESMFILE=$MECHDIR/$PFX/presmoments-lpmerged-avgwin-250ns.txt
    # python $SCRIPTS_DIR/calc_psome_kbend.py $DNSFILE $SPHADIR $PRESMFILE $RAVGFILE $KBENDFILE $PLOTSAMPLE $TWIN
    cp $RAWDIR/sphaspectra/$PFX/$PFX-ravg.txt $RAVGFILE
    cp $RAWDIR/mechprops/$PFX/kbend.txt $KBENDFILE

    # Calculate elastic parameters
    echo "Calculating elastic properties for $PFX ..."
    RAVGFILE=$MECHDIR/$PFX/ravg.txt
    KBENDFILE=$MECHDIR/$PFX/kbend.txt
    PRESMFILE=$MECHDIR/$PFX/presmoments-lpmerged-avgwin-${AVGWIN}ns.txt
    python $SCRIPTS_DIR/calc_psome_elastprops.py $DNSFILE $PRESMFILE $RAVGFILE $KBENDFILE \
                                                 $INNENCFILE $OUTENCFILE $ELASTFILE $AVGWIN $LASTNS

done
