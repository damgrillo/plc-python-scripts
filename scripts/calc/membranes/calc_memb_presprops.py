import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip
from matplotlib import pyplot as plt

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
resdir  = str(sys.argv[1])
strdir  = str(sys.argv[2])
presdir = str(sys.argv[3])
pfxname = str(sys.argv[4])
twin    = int(sys.argv[5])
pmethod = str(sys.argv[6])

# Units conversion
# 1 mN/m     = 10 bar*nm
# 1 bar*nm3  = 1e-2 * 1e-20 J
# 1 kb*T     = 1e-3 * 1.3806 * Temp * 1e-20 J 
# 1 kb*T     = 1e-1 * 1.3806 * Temp * bar*nm^3

temp       = 300
kbt2barnm3 = 1.3806488 * 1e-1 * temp # Convert kbt to bar*nm^3
kbt2mNm    = 1.3806488 * 1e-2 * temp # Convert kbT to mN*m
kbt2J      = 1.3806488 * 1e-3 * temp # Convert kbT to 1e-20 J
nenc_hinfo = ('ne=Ne/Area', '[nm^-2]')

# Get pressure files for corresponding time window
presfiles = []
mintime   = None
for f in sorted([os.path.join(presdir, f) for f in os.listdir(presdir)]):
    if re.search("presprof-job", f) and re.search(pmethod, f):
        stime, etime = map(int, re.search('job-([0-9]+)-([0-9]+)', f).groups())
        if etime - stime + 1 == twin:
            presfiles.append(f)
        if (mintime == None) or (stime < mintime):
            mintime = stime

# Get lenc data
ledata = []
lefile = pfxname + '-encapsulated-PRIP-lower.txt'
lefile = os.path.join(strdir, lefile)
if os.path.isfile(lefile):
    fp     = open(lefile, 'r')
    fread  = [line.strip() for line in fp]
    fhead  = fread[0].lstrip('#').split()
    funit  = fread[1].lstrip('#').split()
    fdata  = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    ncol   = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == nenc_hinfo]
    ledata = fdata[fdata[:, 0] >= mintime][:, ncol]  

# Get uenc data
uedata = []
uefile = pfxname + '-encapsulated-PRIP-upper.txt'
uefile = os.path.join(strdir, uefile)
if os.path.isfile(uefile):
    fp     = open(uefile, 'r')
    fread  = [line.strip() for line in fp]
    fhead  = fread[0].lstrip('#').split()
    funit  = fread[1].lstrip('#').split()
    fdata  = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    ncol   = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == nenc_hinfo]
    uedata = fdata[fdata[:, 0] >= mintime][:, ncol]

# Calculate lenc, uenc, denc for each time window
encvals = []
for n in np.arange(len(ledata)/twin):
    i  = n*twin
    tf = mintime + i + twin
    le = np.mean(ledata[i:i+twin])
    ue = np.mean(uedata[i:i+twin])
    de = ue - le
    encvals.append((tf, le, ue, de)) 

# Get pressure profiles 
presprofs = []
for txt in presfiles:  
    finp  = open(txt, 'r')
    fread = [line.strip() for line in finp] 
    fdata = [line.split() for line in fread if not re.match('#|@', line)] 
    pprof = np.array([map(float, d) for d in fdata])
    presprofs.append(pprof)
    finp.close()

# Calculate lateral pressure properties
gamvals  = []
tauvals  = []
gauvals  = []
tf = mintime + twin
for prof in presprofs:
    proflow = prof[prof[:,0] < 0] 
    profupp = prof[prof[:,0] > 0]
    zlow, platlow = proflow[:, 0], proflow[:, 1]-proflow[:, 2]
    zupp, platupp = profupp[:, 0], profupp[:, 1]-profupp[:, 2]
    ztot, plattot = prof[:, 0],  prof[:, 1]-prof[:, 2]

    # Calculate zero moment (gamma), first moment (tau), and second moment (gau)
    pres_tan =  np.mean(prof[:, 1])
    pres_nor =  np.mean(prof[:, 2])
    pres_pl  =  pres_tan-pres_nor
    gam_low  = -np.trapz(y=platlow, x=zlow)
    gam_upp  = -np.trapz(y=platupp, x=zupp)
    gam_tot  = -np.trapz(y=plattot, x=ztot)
    tau_low  =  np.trapz(y=platlow*zlow, x=zlow)
    tau_upp  =  np.trapz(y=platupp*zupp, x=zupp)
    tau_tot  =  np.trapz(y=plattot*ztot, x=ztot)
    gau_tot  = -np.trapz(y=plattot*ztot*ztot, x=ztot)

    # Convert tau from bar*nm^2 to kb*T/nm
    tau_low /= kbt2barnm3
    tau_upp /= kbt2barnm3
    tau_tot /= kbt2barnm3

    # Convert gau from bar*nm^3 to kb*T
    gau_tot /= kbt2barnm3

    if len(ledata) == 0:
        encvals.append((tf, 0.0, 0.0, 0.0))
    gamvals.append((tf, pres_tan, pres_nor, pres_pl, gam_low, gam_upp, gam_tot))
    tauvals.append((tf, tau_low, tau_upp, tau_tot))
    gauvals.append((tf, gau_tot))
    tf += twin

# Calculate averages and stdevs
gamma_avg = np.mean(gamvals, axis=0)[1:]
gamma_std = np.std(gamvals, axis=0)[1:]
tau_avg   = np.mean(tauvals, axis=0)[1:]
tau_std   = np.std(tauvals, axis=0)[1:]
gau_avg   = np.mean(gauvals, axis=0)[1:]
gau_std   = np.std(gauvals, axis=0)[1:]

# Convert properties to dictionaries
encvals    = {int(tup[0]):tup[1:] for tup in encvals}
gamvals    = {int(tup[0]):tup[1:] for tup in gamvals}
tauvals    = {int(tup[0]):tup[1:] for tup in tauvals}
gauvals    = {int(tup[0]):tup[1]  for tup in gauvals}


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

write_enc = (len(encvals) > 0)
write_gam = True
write_tau = True
write_gau = True

# Write denc
propsdir = resdir + '/mechprops/' + pfxname
if not os.path.isdir(propsdir):
    os.makedirs(propsdir)
outfile = propsdir + '/denc-avgwin-' + str(twin) + 'ns.txt'

if write_enc:
    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('lenc')
    sout += '{:>12s} '.format('uenc')
    sout += '{:>12s} '.format('denc')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '\n'
    fout.write(sout)

    for time in sorted(encvals.keys()):
        lenc, uenc, denc = encvals[time]
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.4f} '.format(lenc)
        sout += '{0:>12.4f} '.format(uenc)
        sout += '{0:>12.4f} '.format(denc)
        sout += '\n'
        fout.write(sout)

# Write gamma
outfile = propsdir + '/gamma-' + pmethod + '-avgwin-' + str(twin) + 'ns.txt'

if write_gam:
    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('pres_tan')
    sout += '{:>12s} '.format('pres_nor')
    sout += '{:>12s} '.format('pres_lat')
    sout += '{:>12s} '.format('gamma_low')
    sout += '{:>12s} '.format('gamma_upp')
    sout += '{:>12s} '.format('gamma_tot')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[bar]')
    sout += '{:>12s} '.format('[bar]')
    sout += '{:>12s} '.format('[bar]')
    sout += '{:>12s} '.format('[bar*nm]')
    sout += '{:>12s} '.format('[bar*nm]')
    sout += '{:>12s} '.format('[bar*nm]')
    sout += '\n'
    fout.write(sout)

    for time in sorted(gamvals.keys()):
        ptan, pnor, plat, glow, gupp, gtot = gamvals[time]
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.4f} '.format(ptan)
        sout += '{0:>12.4f} '.format(pnor)
        sout += '{0:>12.4f} '.format(plat)
        sout += '{0:>12.4f} '.format(glow)
        sout += '{0:>12.4f} '.format(gupp)
        sout += '{0:>12.4f} '.format(gtot)
        sout += '\n'
        fout.write(sout)

#    ptan, pnor, plat, glow, gupp, gtot = gamma_avg
#    sout  = '{0:>12s} '.format("AVG")
#    sout += '{0:>12.4f} '.format(ptan)
#    sout += '{0:>12.4f} '.format(pnor)
#    sout += '{0:>12.4f} '.format(plat)
#    sout += '{0:>12.4f} '.format(glow)
#    sout += '{0:>12.4f} '.format(gupp)
#    sout += '{0:>12.4f} '.format(gtot)
#    sout += '\n'
#    fout.write(sout)

#    ptan, pnor, plat, glow, gupp, gtot = gamma_std
#    sout  = '{0:>12s} '.format("STD")
#    sout += '{0:>12.4f} '.format(ptan)
#    sout += '{0:>12.4f} '.format(pnor)
#    sout += '{0:>12.4f} '.format(plat)
#    sout += '{0:>12.4f} '.format(glow)
#    sout += '{0:>12.4f} '.format(gupp)
#    sout += '{0:>12.4f} '.format(gtot)
#    sout += '\n'
#    fout.write(sout)

    fout.close()

# Write tau
outfile = propsdir + '/tau-' + pmethod + '-avgwin-' + str(twin) + 'ns.txt'

if write_tau:

    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>14s} '.format('tau_low')
    sout += '{:>14s} '.format('tau_upp')
    sout += '{:>14s} '.format('tau_tot')
    sout += '{:>14s} '.format('tau_avg_mlay')
    sout += '{:>14s} '.format('tau_avg_mlay')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>14s} '.format('[kb*T/nm]')
    sout += '{:>14s} '.format('[kb*T/nm]')
    sout += '{:>14s} '.format('[kb*T/nm]')
    sout += '{:>14s} '.format('[kb*T/nm]')
    sout += '{:>14s} '.format('[1e-20 J/nm]')
    sout += '\n'
    fout.write(sout)

    for time in sorted(tauvals.keys()):
        tlow, tupp, ttot = tauvals[time]
        tavg  = 0.5 * (tupp - tlow)
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>14.4f} '.format(tlow)
        sout += '{0:>14.4f} '.format(tupp)
        sout += '{0:>14.4f} '.format(ttot)
        sout += '{0:>14.4f} '.format(tavg)
        sout += '{0:>14.4f} '.format(tavg * kbt2J)
        sout += '\n'
        fout.write(sout)

#    tlow, tupp, ttot = tau_avg
#    tavg  = 0.5 * (tupp - tlow)
#    sout  = '{0:>12s} '.format("AVG")
#    sout += '{0:>14.4f} '.format(tlow)
#    sout += '{0:>14.4f} '.format(tupp)
#    sout += '{0:>14.4f} '.format(ttot)
#    sout += '{0:>14.4f} '.format(tavg)
#    sout += '{0:>14.4f} '.format(tavg * kbt2J)
#    sout += '\n'
#    fout.write(sout)

#    tlow, tupp, ttot = tau_std
#    tavg  = 0.5 * (tupp - tlow)
#    sout  = '{0:>12s} '.format("STD")
#    sout += '{0:>14.4f} '.format(tlow)
#    sout += '{0:>14.4f} '.format(tupp)
#    sout += '{0:>14.4f} '.format(ttot)
#    sout += '{0:>14.4f} '.format(tavg)
#    sout += '{0:>14.4f} '.format(tavg * kbt2J)
#    sout += '\n'
#    fout.write(sout)

    fout.close()

# Write kgauss
outfile = propsdir + '/kgauss-' + pmethod + '-avgwin-' + str(twin) + 'ns.txt'

if write_gau:

    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>14s} '.format('gau_tot')
    sout += '{:>14s} '.format('gau_tot')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>14s} '.format('[kb*T]')
    sout += '{:>14s} '.format('[1e-20 J]')
    sout += '\n'
    fout.write(sout)

    for time in sorted(gauvals.keys()):
        gtot  = gauvals[time]
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>14.4f} '.format(gtot)
        sout += '{0:>14.4f} '.format(gtot * kbt2J)
        sout += '\n'
        fout.write(sout)

#    gtot  = gau_avg[0]
#    sout  = '{0:>12s} '.format("AVG")
#    sout += '{0:>14.4f} '.format(gtot)
#    sout += '{0:>14.4f} '.format(gtot * kbt2J)
#    sout += '\n'
#    fout.write(sout)

#    gtot  = gau_std[0]
#    sout  = '{0:>12s} '.format("STD")
#    sout += '{0:>14.4f} '.format(gtot)
#    sout += '{0:>14.4f} '.format(gtot * kbt2J)
#    sout += '\n'
#    fout.write(sout)

    fout.close()


