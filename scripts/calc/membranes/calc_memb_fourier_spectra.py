import re
import sys
import math
import cmath
import string
import numpy as np
from itertools import izip
from operator import itemgetter
from matplotlib import pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D


###########################################################################################################
################################################# FUNCTIONS ###############################################
###########################################################################################################

# Interpolate surface
def interpolate_surface(surf3d, grid2d, itype):
    rsurf = surf3d[:, :2]
    zsurf = surf3d[:, 2]
    zmean = np.mean(zsurf)
    zgrid = griddata(rsurf, zsurf, grid2d, method=itype, fill_value=zmean)
    return np.column_stack((grid2d, zgrid))

# Generate q positions
def gen_qpos(boxlen, nxmax, nymax, span='half'):
    nxmin  = (0 if span == 'half' else -nxmax)
    nxlist = np.arange(nxmin, nxmax+1)
    nylist = np.arange(-nymax, nymax+1)
    nqlist = np.array([(nx, ny) for nx in nxlist for ny in nylist]) #if nx != 0 or ny != 0])
    qpos   = 2*np.pi * nqlist / boxlen[:2]
    qabs   = np.sqrt(np.sum(qpos*qpos, axis=1))
    return (qpos, qabs)

# Calculate Fourier amplitudes in q-frequency domain
def calc_fourier_amps(crds, qpos):
    rpos  = crds[:, :2]
    zpos  = crds[:, 2]
    npos  = crds.shape[0]
    qrdot = np.dot(qpos, np.transpose(rpos))
    qrexp = np.exp(-1j*qrdot)
    fq    = np.dot(qrexp, zpos) / npos
    f2q   = fq.real*fq.real + fq.imag*fq.imag
    return (fq, f2q)

# Calculate Fourier height and thickness amplitudes in q-frequency domain
def calc_fourier_htamps(uppcrds, lowcrds, qpos, area):
    urpos  = uppcrds[:, :2]
    uzpos  = uppcrds[:, 2]
    lrpos  = lowcrds[:, :2]
    lzpos  = lowcrds[:, 2]
    npos   = uppcrds.shape[0]
    uqrdot = np.dot(qpos, np.transpose(urpos))
    lqrdot = np.dot(qpos, np.transpose(lrpos))
    uqrexp = np.exp(-1j*uqrdot)
    lqrexp = np.exp(-1j*lqrdot)
    hq     = (np.dot(uqrexp, uzpos) + np.dot(lqrexp, lzpos)) / (2*npos)
    tq     = (np.dot(uqrexp, uzpos) - np.dot(lqrexp, lzpos)) / (2*npos)
    h2q    = hq.real*hq.real + hq.imag*hq.imag
    t2q    = tq.real*tq.real + tq.imag*tq.imag
    return ((hq, tq), (h2q, t2q))

# Calculate Fourier coordinates in r-spatial domain
def calc_fourier_coords(crds, qpos, fq, span='half'):
    if len(fq) == 0:
        res = []
    else:
        rpos  = crds[:, [0, 1]]
        rqdot = np.dot(rpos, np.transpose(qpos))
        fz    = np.dot(np.exp(1j*rqdot), fq)
        if span == 'half':
            fz += np.conj(fz)
        res = np.column_stack((rpos, fz))
    return res

# Group Fourier amplitudes by q
def group_fourier_amps(qabs, fq, qres):
    qprev = 0
    fgrps = []
    famps = np.column_stack((qabs, fq))
    for q, f in famps[np.argsort(famps[:,0])]:
        if q - qprev > qres or qprev == 0:
            fgrps.append([q, f])
        else:
            fgrps[-1] += [f]
        qprev = q
    return np.array([(fg[0], np.mean(fg[1:])) for fg in fgrps])

# Write xvg file
def write_xvg_1spectrum(outfile, fspec):
    fout  = open(outfile, 'w')
    sout  = '#{0:>11s} {1:>12s}\n'.format('q', '|h(q)|^2')
    sout += '#{0:>11s} {1:>12s}\n'.format('[nm^-1]', '[nm^2]')
    fout.write(sout)
    for q, h2q in fspec[:, :2]:
        sout  = '{0:>12.4f} {1:>12.4e}\n'.format(q, h2q)
        fout.write(sout)
    fout.close()

# Write xvg file
def write_xvg_2spectra(outfile, fspec):
    fout  = open(outfile, 'w')
    sout  = '#{0:>11s} {1:>12s} {2:>12s}\n'.format('q', '|h(q)|^2', '|t(q)|^2')
    sout += '#{0:>11s} {1:>12s} {2:>12s}\n'.format('[nm^-1]', '[nm^2]', '[nm^2]')
    fout.write(sout)
    for q, h2q, t2q in fspec[:, :3]:
        sout  = '{0:>12.5f} {1:>12.4e} {2:>12.4e}\n'.format(q, h2q, t2q)
        fout.write(sout)
    fout.close()

# Write gro file
def write_gro_file(outfile, upporig, loworig, uppsurf, lowsurf, meansurf, uppfour, lowfour, meanfour, boxlen, ztype='height'):

    # Mean z positions
    uzmean  = np.mean(upporig[:, 2])
    lzmean  = np.mean(loworig[:, 2])
    bzmean  = (uzmean+lzmean) * 0.5
    mszmean = np.mean(meansurf[:, 2]).real
    mfzmean = np.mean(meanfour[:, 2]).real

    # Write header
    fout  = open(outfile, 'w')
    sout  = 'Evaluate Fourier\n'
    fout.write(sout)

    # Write original coordinates 
    ncrd = 0
    sout = ''
    for rx, ry, rz in upporig:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('1ORI', 'O', ncrd, rx, ry, rz)
    for rx, ry, rz in loworig:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('2ORI', 'O', ncrd, rx, ry, rz)

    # Write interpolated surface coordinates
    for rx, ry, rz in uppsurf:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('3UIP', 'I', ncrd, rx.real, ry.real, rz.real)
    for rx, ry, rz in lowsurf:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('4LIP', 'I', ncrd, rx.real, ry.real, rz.real)
    for rx, ry, rz in meansurf:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('5MIP', 'I', ncrd, rx.real, ry.real, rz.real+bzmean-mszmean)

    # Write Fourier coordinates
    for rx, ry, rz in uppfour:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('6UPF', 'F', ncrd, rx.real, ry.real, rz.real+uzmean)
    for rx, ry, rz in lowfour:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('7LWF', 'F', ncrd, rx.real, ry.real, rz.real+lzmean)
    for rx, ry, rz in meanfour:
        ncrd += 1
        sout += '{0:>8s}{1:>7s}{2:>5d}{3:>8.3f}{4:>8.3f}{5:>8.3f}\n'.format('8MNF', 'F', ncrd, rx.real, ry.real, rz.real+bzmean-mfzmean)

    fout.write(str(ncrd) + '\n')
    fout.write(sout)

    # Write box length
    sout  = '{0:>10.5f}{1:>10.5f}{2:>10.5f}\n'.format(boxlen[0], boxlen[1], boxlen[2])
    fout.write(sout)
    fout.close()

# Plot surfaces
def plot_surfaces(outfile, origcrds, intpcrds, sphacrds,  nrows, ncols):
    fig = plt.figure()
    intxgrid = np.reshape(intpcrds[:, 0], (nrows, ncols))
    intygrid = np.reshape(intpcrds[:, 1], (nrows, ncols))
    intzgrid = np.reshape(intpcrds[:, 2], (nrows, ncols))
    ax  = fig.add_subplot(111, projection='3d')
    ax.plot_wireframe(intxgrid, intygrid, intzgrid, linewidths=0.5, colors='y', rstride=1, cstride=1)
    ax.scatter(origcrds[:, 0], origcrds[:, 1], origcrds[:, 2], s=5, marker='o', facecolors='none', edgecolors='r', depthshade=False)
    ax.scatter(sphacrds[:, 0], sphacrds[:, 1], sphacrds[:, 2], s=5, marker='o', facecolors='none', edgecolors='g', depthshade=False)
    ax.view_init(elev=20, azim=30)
    fig.set_dpi(200)
    fig.set_size_inches(10, 10)
    fig.tight_layout(h_pad=0.0)
    fig.savefig(outfile) 


# Evaluate Fourier
def eval_fourier(outfile, pdffile, uppcrds, lowcrds, uppsurf, lowsurf, qpos, uppfq, lowfq, hgtfq, boxlen, nrows, ncols, span='half', ztype='height'):
    uppfour  = calc_fourier_coords(uppcrds, qpos, uppfq, span)
    lowfour  = calc_fourier_coords(lowcrds, qpos, lowfq, span)
    meanfour = calc_fourier_coords(np.vstack((uppcrds, lowcrds)), qpos, hgtfq, span)
    hgtsurf  = np.column_stack((uppsurf[:, :2], (uppsurf[:, 2] + lowsurf[:, 2]) * 0.5))
    thksurf  = np.column_stack((uppsurf[:, :2], (uppsurf[:, 2] - lowsurf[:, 2]) * 0.5))
    meansurf = (thksurf if ztype == 'thickness' else hgtsurf)
    write_gro_file(outfile, uppcrds, lowcrds, uppsurf, lowsurf, meansurf, uppfour, lowfour, meanfour, boxlen, ztype)
    #plot_surfaces(pdffile, 0.5*(uppcrds+lowcrds), meansurf, meanfour, nrows, ncols)


###########################################################################################################
################################################ MAIN PROGRAM #############################################
###########################################################################################################

# Input parameters
inpfile = str(sys.argv[1])
outfile = str(sys.argv[2])
gridfac = float(sys.argv[3])
itype   = str(sys.argv[4])

# Aditional global parameters
nqmax = 20
qspan = 'half'

# Numeric resolution
xres = 1e-5
qres = 1e-5

# Patterns
boxpatt = 'box\[\s*([0-2])\]=\{(.*), (.*), (.*)\}'
crdpatt = 'x\[.*\]=\{(.*),(.*),(.*)\}'

# Read file
frames = []
finp   = open(inpfile, 'r')
for line in finp:

    # Read new frame
    if re.search('step=', line):
        frames.append({'boxlen':{}, 'coords':[]}) 
        boxlen = frames[-1]['boxlen']
        coords = frames[-1]['coords']

    # Read box length
    elif re.search('box\[', line):
        bgrps = re.search(boxpatt, line).groups()
        axis  = int(bgrps[0])
        blen  = float(bgrps[axis+1])
        boxlen[axis] = blen

    # Read coordinates
    elif re.search('x\[', line):
        cgrps = re.search(crdpatt, line).groups()  
        coords.append(map(float, cgrps))

# Calculate Fourier spectra for each frame
for fr in frames:

    # Initialize frame data
    fr['coords'] = np.array(fr['coords'])
    fr['boxlen'] = np.array([fr['boxlen'][i] for i in range(3)])

    # Boxlen and coordinates
    boxlen         = fr['boxlen']
    coords         = fr['coords']
    coords[:, 0]  -= np.mean(coords[:, 0])
    coords[:, 1]  -= np.mean(coords[:, 1])
    coords[:, 2]  -= np.mean(coords[:, 2])

    # Separate into upper and lower interfaces
    fr['uppcrds']  = coords[coords[:, 2] > 0]
    fr['lowcrds']  = coords[coords[:, 2] < 0]
    uppcrds        = fr['uppcrds']
    lowcrds        = fr['lowcrds']
    uppcent        = np.copy(fr['uppcrds'])
    lowcent        = np.copy(fr['lowcrds'])
    uppcent[:, 2] -= np.mean(uppcent[:, 2]) 
    lowcent[:, 2] -= np.mean(lowcent[:, 2]) 
    
    # Test number of elements in upper and lower interfaces
    if uppcrds.shape[0] != uppcrds.shape[0]:
        msg  = "ERROR: Number of elements in upper interface ({:d}) ".format(uppcrds.shape[0])
        msg += "different from number of elements in lower interface ({:d}) ".format(lowcrds.shape[0])
        sys.exit(msg)
    else:
        ncrds = uppcrds.shape[0]

    # Calculate grid size
    ngrid = int(np.sqrt(ncrds)*gridfac) 

    # Interpolate surface to increase points density
    rpos     = coords[:, :2]
    upprmin  = np.amin(uppcrds[:, :2], axis=0)
    upprmax  = np.amax(uppcrds[:, :2], axis=0)
    lowrmin  = np.amin(uppcrds[:, :2], axis=0)
    lowrmax  = np.amax(uppcrds[:, :2], axis=0)
    rmin     = np.amin((upprmin, lowrmin), axis=0)
    rmax     = np.amax((upprmax, lowrmax), axis=0)
    xm, ym   = (np.amin(rpos, axis=0) if rmin is None else rmin)
    xM, yM   = (np.amax(rpos, axis=0) if rmax is None else rmax)
    xgrid    = np.arange(xm, xM, (xM-xm)/ngrid)
    ygrid    = np.arange(ym, yM, (yM-ym)/ngrid)
    rgrid    = np.array([(x, y) for x in xgrid for y in ygrid])
    upinterp = interpolate_surface(uppcrds, rgrid, itype)
    lwinterp = interpolate_surface(lowcrds, rgrid, itype)
    fr['upinterp'] = upinterp
    fr['lwinterp'] = lwinterp
    fr['xgrid']    = xgrid
    fr['ygrid']    = ygrid

    # Center interpolated surfaces
    upintcen        = np.copy(upinterp)
    lwintcen        = np.copy(lwinterp)
    upintcen[:, 2] -= np.mean(upintcen[:, 2]) 
    lwintcen[:, 2] -= np.mean(lwintcen[:, 2]) 

    # Generate q positions
    qpos, qabs = gen_qpos(boxlen, nqmax, nqmax, qspan)

    # Calculate Fourier amplitudes and spectrum for bilayer height
    area          = boxlen[0]*boxlen[1]
    hfq, hf2q     = calc_fourier_amps(0.5*(upintcen+lwintcen), qpos)
    fhgrps        = group_fourier_amps(qabs, hf2q, qres)
    fr['ifspec']  = np.copy(fhgrps)


###########################################################################################################
########################################### OUTPUT FILES ##################################################
###########################################################################################################

# Write average Fourier spectra for interpolated data
outf  = outfile
fsavg = np.mean([fr['ifspec'] for fr in frames], axis=0)
write_xvg_1spectrum(outf, np.absolute(fsavg))


## Write average Fourier spectra for raw data
#outf  = outfile.replace('fspectra.xvg', 'fspectra-rawdata.xvg')
#fsavg = np.absolute(np.mean([fr['rfspec'] for fr in frames], axis=0))
#write_xvg_2spectra(outf, fsavg)

## Test Fourier decomposition for interpolated data
#fr       = frames[0]
#xgrid    = fr['xgrid']
#ygrid    = fr['ygrid']
#upinterp = fr['upinterp']
#lwinterp = fr['lwinterp']
#famps    = fr['ifamps']
#qpos     = famps[0]
#uppfq    = famps[1]
#lowfq    = famps[2]
#hgtfq    = famps[3]
#nrows    = len(xgrid)
#ncols    = len(ygrid)
#outf     = outfile.replace('fspectra.xvg', 'eval-fourier.gro')
#pdff     = outfile.replace('fspectra.xvg', 'eval-fourier.pdf')
#eval_fourier(outf, pdff, uppcrds, lowcrds, upinterp, lwinterp, qpos, uppfq, lowfq, hgtfq, boxlen, nrows, ncols, qspan, 'height')

## Test Fourier decomposition for raw data
#famps = fr['rfamps']
#qpos  = famps[0]
#uppfq = famps[1]
#lowfq = famps[2]
#hgtfq = famps[3]
#thkfq = famps[4]
#outf  = outfile.replace('fspectra.xvg', 'eval-fourier-rawdata.gro')
#pdff  = outfile.replace('fspectra.xvg', 'eval-fourier-rawdata.pdf')
#eval_fourier(outf, pdff, uppcrds, lowcrds, upinterp, lwinterp, qpos, uppfq, lowfq, hgtfq, boxlen, nrows, ncols, qspan, 'height')


