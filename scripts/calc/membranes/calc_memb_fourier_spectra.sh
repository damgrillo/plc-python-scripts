#!/bin/bash

# Global vars
SCRIPTS_DIR=$(cd $(dirname $0) && pwd)
MEMB_OUTDIR= # Path to store output raw data from membrane simulations
FSPECTRA_DIR= # Path to store output Fourier spectra calculated in this script

# Set poly name and dir name
POLYNAME='PBD22-PEO14'
TMPDIR=$FSPECTRA_DIR/tmp
mkdir -p $TMPDIR

# Get Fourier spectra of membrane fluctuations
for PFX in $(ls $MEMB_OUTDIR | grep "$POLYNAME" | grep 'large'); do

    # Create temp dir
    for GRIDFAC in 2.0; do
        for ITYPE in linear; do

            OUTDIR=$MEMB_OUTDIR/$PFX
            FLSDIR=$FSPECTRA_DIR/$PFX
            mkdir -p $FLSDIR
            rm $TMPDIR/*
            echo $PFX-$ITYPE-interp-ngrid-${GRIDFAC}x

            # Loop on xtc files
            NPJOBS=0
            FNAMES=$(ls $OUTDIR | grep -o 'job-[0-9]\+' | sort -V | uniq | tail -10)
            for FNM in $FNAMES; do

                # Set files
                XTC=$OUTDIR/$FNM-pbdsurf.xtc
                DMP=$TMPDIR/$FNM-dump.txt
                XVG=$FLSDIR/$FNM-fspectra.xvg

                # Calculate amplitudes of Fourier spectra in parallel jobs
                if [[ ! -f $XVG ]]; then 
                    echo "Calculating $FNM ... "
                    gmx dump -f $XTC > $DMP 2> /dev/null
                    python $SCRIPTS_DIR/calc_memb_fourier_spectra.py $DMP $XVG $GRIDFAC $ITYPE &
                    NPJOBS=$((NPJOBS+1))
                else
                    echo "$FNM already done ... "
                fi

                # Wait for jobs
                if [ $NPJOBS -eq 4 ]; then
                    wait
                    NPJOBS=0
                fi        

            done
            wait

        done
    done
done

# Clean temp files
rm -r $TMPDIR

