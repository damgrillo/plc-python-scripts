import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'


###########################################################################################################
############################################## MAIN PROGRAM ###############################################
###########################################################################################################

# Input variables
elastfile = str(sys.argv[1])
mechdir   = str(sys.argv[2])
strdir    = str(sys.argv[3])
avgwin    = str(sys.argv[4])
lastns    = int(sys.argv[5])

# Units conversion
# 1 mN/m     = 10 bar*nm
# 1 bar*nm3  = 1e-2 * 1e-20 J
# 1 kb*T     = 1e-3 * 1.3806 * Temp * 1e-20 J 
# 1 kb*T     = 1e-1 * 1.3806 * Temp * bar*nm^3

# Convert kbT in 1e-20 J
temp  = 300
kbt2J = 1.3806488 * 1e-3 * temp

# Get mechanic properties files
mechflist = [os.path.join(mechdir, f) for f in os.listdir(mechdir)]
mechsdirs = [sd for sd in mechflist if os.path.isdir(sd)]

# Get lenc, uenc, tau and kbend files for each system
lencfiles  = {}
uencfiles  = {}
kbendfiles = {}
gammafiles = {}
taufiles   = {}
gaufiles   = {}
for sdir in mechsdirs:
    sname  = sdir.strip('/').split('/')[-1]
    lencf  = sname + '-encapsulated-PRIP-lower.txt'
    uencf  = sname + '-encapsulated-PRIP-upper.txt'
    kbendf = 'kbend-large.txt'
    gammaf = 'gamma-IKcont-avgwin-' + avgwin + 'ns.txt'
    tauf   = 'tau-IKcont-avgwin-' + avgwin + 'ns.txt'
    gauf   = 'kgauss-IKcont-avgwin-' + avgwin + 'ns.txt'
    lencfiles[sname]  = os.path.join(strdir, lencf)
    uencfiles[sname]  = os.path.join(strdir, uencf)
    kbendfiles[sname] = os.path.join(sdir, kbendf)   
    gammafiles[sname] = os.path.join(sdir, gammaf)  
    taufiles[sname]   = os.path.join(sdir, tauf)   
    gaufiles[sname]   = os.path.join(sdir, gauf)  

# Headers and units info
kcavg_hinfo = ('kbend_avg', '[kb*T]')
kcstd_hinfo = ('kbend_std', '[kb*T]')
glow_hinfo = ('gamma_low', '[bar*nm]')
gupp_hinfo = ('gamma_upp', '[bar*nm]')
gtot_hinfo = ('gamma_tot', '[bar*nm]')
taub_hinfo = ('tau_tot', '[kb*T/nm]')
taum_hinfo = ('tau_upp', '[kb*T/nm]')
gau_hinfo  = ('gau_tot', '[kb*T]')
nenc_hinfo = ('ne=Ne/Area', '[nm^-2]')

# Get mechanic properties for each system
lenc = {}
uenc = {}
denc = {}
glow = {}
gupp = {}
gtot = {}
kc   = {}
taum = {}
taub = {}
kg   = {}
c0m  = {}
c0b  = {}
em   = {}
eb   = {}
kgkc = {}
de   = {}
for sname in kbendfiles:

    # Set system label
    if re.search('PRIN', sname) and re.search('PRIP', sname):
        slabel  = 'N' + re.search("PRIN([0-9]+)", sname).group(1) + '-'
        slabel += 'P' + re.search("PRIP([0-9]+)", sname).group(1)
    elif re.search('PRIN', sname):
        slabel += 'N' + re.search("PRIN([0-9]+)", sname).group(1)
    elif re.search('PRIP', sname):
        slabel += 'P' + re.search("PRIP([0-9]+)", sname).group(1)
    else:
        slabel = 'Neat'
    if re.search('newrun', sname):
        slabel += '-NEW'  
    if re.search('extend', sname):
        slabel += '-EXT'  

    # Get lenc data
    if os.path.isfile(lencfiles[sname]):
        fp     = open(lencfiles[sname], 'r')
        fread  = [line.strip() for line in fp]
        fhead  = fread[0].lstrip('#').split()
        funit  = fread[1].lstrip('#').split()
        fdata  = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
        ncol   = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == nenc_hinfo]
        ledata = fdata[-lastns:, ncol]
    else:
        ledata = np.zeros(1)

    # Get uenc data
    if os.path.isfile(uencfiles[sname]):
        fp     = open(uencfiles[sname], 'r')
        fread  = [line.strip() for line in fp]
        fhead  = fread[0].lstrip('#').split()
        funit  = fread[1].lstrip('#').split()
        fdata  = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
        ncol   = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == nenc_hinfo]
        uedata = fdata[-lastns:, ncol]
    else:
        uedata = np.zeros(1)

    # Set lenc, uenc, denc
    lenc[slabel] = np.array((np.mean(ledata), np.std(ledata)))
    uenc[slabel] = np.array((np.mean(uedata), np.std(uedata)))
    denc[slabel] = np.array((np.mean(uedata-ledata), np.std(uedata-ledata)))

    # Get gamma data
    fp    = open(gammafiles[sname], 'r')
    fread = [line.strip() for line in fp]
    fhead = fread[0].lstrip('#').split()
    funit = fread[1].lstrip('#').split()
    fdata = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    nclow = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == glow_hinfo]
    ncupp = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == gupp_hinfo]
    nctot = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == gtot_hinfo]
    glow[slabel] = np.array((np.mean(fdata[:, nclow]), np.std(fdata[:, nclow]))) 
    gupp[slabel] = np.array((np.mean(fdata[:, ncupp]), np.std(fdata[:, ncupp]))) 
    gtot[slabel] = np.array((np.mean(fdata[:, nctot]), np.std(fdata[:, nctot]))) 

    # Get kbend data
    fp    = open(re.sub('-newrun|-extend', '', kbendfiles[sname]), 'r')
    fread = [line.strip() for line in fp]
    fhead = fread[0].lstrip('#').split()
    funit = fread[1].lstrip('#').split()
    fdata = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    nacol = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == kcavg_hinfo]
    kcavg = fdata[:, nacol]
    nacol = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == kcavg_hinfo]
    nscol = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == kcstd_hinfo]
    kcavg = np.mean(fdata[:, nacol])
    kcstd = np.mean(fdata[:, nscol])
    kc[slabel] = np.array((kcavg, kcstd))   
  
    # Get tau data
    fp    = open(taufiles[sname], 'r')
    fread = [line.strip() for line in fp]
    fhead = fread[0].lstrip('#').split()
    funit = fread[1].lstrip('#').split()
    fdata = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    ncolm = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == taum_hinfo]
    ncolb = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == taub_hinfo]
    taum[slabel] = np.array((np.mean(fdata[:, ncolm]), np.std(fdata[:, ncolm])))  
    taub[slabel] = np.array((np.mean(fdata[:, ncolb]), np.std(fdata[:, ncolb]))) 

    # Get gau data
    fp    = open(gaufiles[sname], 'r')
    fread = [line.strip() for line in fp]
    fhead = fread[0].lstrip('#').split()
    funit = fread[1].lstrip('#').split()
    fdata = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    ncol  = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == gau_hinfo]
    kg[slabel] = np.array((np.mean(fdata[:, ncol]), np.std(fdata[:, ncol])))  
     
    # Calculate c0 = 0.5*tau/kc
    taum_avg    = taum[slabel][0]
    taum_std    = taum[slabel][1]
    taub_avg    = taub[slabel][0]
    taub_std    = taub[slabel][1]
    kcm_avg     = kc[slabel][0]*0.5
    kcm_std     = kc[slabel][1]*0.5
    kcb_avg     = kc[slabel][0]
    kcb_std     = kc[slabel][1]
    c0m_avg     = 0.5*taum_avg / kcm_avg
    c0m_std     = 0.5*np.sqrt((taum_std / kcm_avg)**2 + (taum_avg * kcm_std / kcm_avg**2)**2)
    c0b_avg     = 0.5*taub_avg / kcb_avg
    c0b_std     = 0.5*np.sqrt((taub_std / kcb_avg)**2 + (taub_avg * kcb_std / kcb_avg**2)**2)
    c0m[slabel] = np.array((c0m_avg, c0m_std))
    c0b[slabel] = np.array((c0b_avg, c0b_std))

    # Calculate kg/kc
    kg_avg      = kg[slabel][0]
    kg_std      = kg[slabel][1]
    kc_avg      = kc[slabel][0]
    kc_std      = kc[slabel][1]
    kgkc_avg    = kg_avg / kc_avg
    kgkc_std    = np.sqrt((kg_std / kc_avg)**2 + (kg_avg * kc_std / kc_avg**2)**2)
    kgkc[slabel] = np.array((kgkc_avg, kgkc_std))

    # Calculate eb = 2*kc*c0*c0 = tau*c0
    # Calculate de  = -4*kc*c0
    taum_avg    = taum[slabel][0]
    taum_std    = taum[slabel][1]
    taub_avg    = taub[slabel][0]
    taub_std    = taub[slabel][1]
    c0m_avg     = c0m[slabel][0]
    c0m_std     = c0m[slabel][1]
    c0b_avg     = c0b[slabel][0]
    c0b_std     = c0b[slabel][1]
    em_avg     = taum_avg * c0m_avg
    em_std     = np.sqrt((0.5 * taum_std * c0m_avg)**2 + (0.5 * taum_avg * c0m_std)**2)
    eb_avg     = taub_avg * c0b_avg
    eb_std     = np.sqrt((0.5 * taub_std * c0b_avg)**2 + (0.5 * taub_avg * c0b_std)**2)
    de_avg     = -4 * kc_avg * c0b_avg
    de_std     = np.sqrt((8 * kc_std * c0b_avg)**2 + (8 * kc_avg * c0b_std)**2)
    em[slabel] = np.array((em_avg, em_std))
    eb[slabel] = np.array((eb_avg, eb_std))
    de[slabel] = np.array((de_avg, de_std))


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write elastic parameters
fout = open(elastfile, 'w')
sout  = '#{:>13s}'.format('system')
sout += '{:>14s}'.format('low_enc')
sout += '{:>14s}'.format('upp_enc')
sout += '{:>14s}'.format('diff_enc')
#sout += '{:>14s}'.format('gam_low')
#sout += '{:>14s}'.format('gam_upp')
sout += '{:>12s}'.format('gamma')
sout += '{:>12s}'.format('taum')
sout += '{:>12s}'.format('taub')
sout += '{:>14s}'.format('c0m')
sout += '{:>14s}'.format('c0b')
sout += '{:>12s}'.format('kc')
#sout += '{:>12s}'.format('kc')
sout += '{:>12s}'.format('kg')
#sout += '{:>12s}'.format('kg')
sout += '{:>12s}'.format('kg/kc')
sout += '{:>16s}'.format('em')
sout += '{:>16s}'.format('eb')
sout += '{:>16s}'.format('de')
#sout += '{:>16s}'.format('eb')
sout += '\n'
sout += '#{:>13s}'.format(' ')
sout += '{:>14s}'.format('[1/nm2]')
sout += '{:>14s}'.format('[1/nm2]')
sout += '{:>14s}'.format('[1/nm2]')
#sout += '{:>12s}'.format('[bar*nm]')
#sout += '{:>12s}'.format('[bar*nm]')
sout += '{:>12s}'.format('[bar*nm]')
sout += '{:>12s}'.format('[kbT/nm]')
sout += '{:>12s}'.format('[kbT/nm]')
sout += '{:>14s}'.format('[1/nm]')
sout += '{:>14s}'.format('[1/nm]')
sout += '{:>12s}'.format('[kbT]')
#sout += '{:>12s}'.format('[1e-20 J]')
sout += '{:>12s}'.format('[kbT]')
#sout += '{:>12s}'.format('[1e-20 J]')
sout += '{:>12s}'.format('[adim]')
sout += '{:>16s}'.format('[kbT/nm2]')
sout += '{:>16s}'.format('[kbT/nm2]')
sout += '{:>16s}'.format('[kbT/nm3]')
#sout += '{:>16s}'.format('[1e-20 J/nm^2]')
sout += '\n'
fout.write(sout)

syssort = ['Neat'] + sorted([sl for sl in gtot.keys() if sl != 'Neat'])
for sl in syssort:
    sout  = '{0:>14s}'.format(sl)
    sout += '{0:.3f}/{1:.3f}'.format(*lenc[sl]).rjust(14)
    sout += '{0:.3f}/{1:.3f}'.format(*uenc[sl]).rjust(14)
    sout += '{0:.3f}/{1:.3f}'.format(*denc[sl]).rjust(14)
    #sout += '{0:.3f}/{1:.2f}'.format(*glow[sl]).rjust(14)
    #sout += '{0:.3f}/{1:.2f}'.format(*gupp[sl]).rjust(14)
    sout += '{0:.1f}/{1:.1f}'.format(*gtot[sl]).rjust(12)
    sout += '{0:.2f}/{1:.2f}'.format(*taum[sl]).rjust(12)
    sout += '{0:.2f}/{1:.2f}'.format(*taub[sl]).rjust(12)
    sout += '{0:.3f}/{1:.3f}'.format(*c0m[sl]).rjust(14)
    sout += '{0:.3f}/{1:.3f}'.format(*c0b[sl]).rjust(14)
    sout += '{0:.1f}/{1:.1f}'.format(*kc[sl]).rjust(12)
    #sout += '{0:.1f}/{1:.1f}'.format(*(kc[sl]*kbt2J)).rjust(12)
    sout += '{0:.1f}/{1:.1f}'.format(*kg[sl]).rjust(12)
    #sout += '{0:.1f}/{1:.1f}'.format(*(kg[sl]*kbt2J)).rjust(12)
    sout += '{0:.1f}/{1:.1f}'.format(*kgkc[sl]).rjust(12)
    sout += '{0:.4f}/{1:.4f}'.format(*em[sl]).rjust(16)
    sout += '{0:.4f}/{1:.4f}'.format(*eb[sl]).rjust(16)
    sout += '{0:.4f}/{1:.4f}'.format(*de[sl]).rjust(16)
    #sout += '{0:.1f}/{1:.1f}'.format(*(eb[sl]*kbt2J)).rjust(16)
    sout += '\n'
    fout.write(sout)

fout.close()

