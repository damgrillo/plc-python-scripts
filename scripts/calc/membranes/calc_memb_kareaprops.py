import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip
from matplotlib import pyplot as plt

####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Input variables
resdir  = str(sys.argv[1])
strfile = str(sys.argv[2])
pfxname = str(sys.argv[3])
twin    = int(sys.argv[4])
lastns  = int(sys.argv[5])
ncpoly  = int(sys.argv[6])
systype = ('' if len(sys.argv) < 8 else str(sys.argv[7]))
nclayer = ncpoly / 2

# Temperature and normal pressure
temp = 300

# Convert kbT to mN*n
kbt2mNm = 1.3806488 * 1e-2 * temp

# Convert kbT in 1e-20 J
kbt2J = 1.3806488 * 1e-3 * temp

# Calculate area compressibility modulus
kaprops = []
finp    = open(strfile, 'r')
fread   = [line.strip() for line in finp] 
fdata   = [line.split() for line in fread if not re.match('#|@', line)] 
sprops  = np.array([map(float, d) for d in fdata])
totalns = int(sprops[-1, 0])
lastns  = (totalns if totalns < lastns else lastns)
nwin    = lastns / twin
tmin    = totalns - nwin*twin
row     = 0
while sprops[row, 0] <= tmin:
    row += 1
while row < sprops.shape[0]:
    tf        = sprops[row+twin-1, 0]
    area_data = sprops[row:row+twin , 1] * nclayer
    mthk_data = sprops[row:row+twin , 2]
    area_avg  = np.mean(area_data)
    area_std  = np.std(area_data)
    mthk_avg  = np.mean(mthk_data)
    mthk_std  = np.std(mthk_data)
    area_rate = area_std / area_avg 
    mthk_rate = mthk_std / mthk_avg
    karea     = kbt2mNm * area_avg / (area_std * area_std)
    row      += twin
    kaprops.append((tf, area_avg, area_std, mthk_avg, mthk_std, area_rate, mthk_rate, karea))
finp.close()

# Calculate averages and stdevs
kaprops_avg = np.mean(kaprops, axis=0)[1:]
kaprops_std = np.std(kaprops, axis=0)[1:]

# Convert properties to dictionaries
kaprops = {int(t[0]):t[1:] for t in kaprops}

###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write area compressibility modulus
propsdir = resdir + '/mechprops/' + pfxname
if not os.path.isdir(propsdir):
    os.makedirs(propsdir)
if systype == 'large':
    outfile = propsdir + '/karea-large-avgwin-' + str(twin) + 'ns.txt'
else:
    outfile = propsdir + '/karea-avgwin-' + str(twin) + 'ns.txt'

fout = open(outfile, 'w')
sout  = '#{:>11s} '.format('Time')
sout += '{:>12s} '.format('<A>')
sout += '{:>12s} '.format('sA')
sout += '{:>12s} '.format('<d>')
sout += '{:>12s} '.format('sd')
sout += '{:>12s} '.format('sA/A')
sout += '{:>12s} '.format('sd/d')
sout += '{:>12s} '.format('Ka')
sout += '{:>12s} '.format('Ka*d2')
sout += '{:>12s} '.format('Ka*d2')
sout += '\n'
sout += '#{:>11s} '.format('[ns]')
sout += '{:>12s} '.format('[nm^2]')
sout += '{:>12s} '.format('[nm^2]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[nm]')
sout += '{:>12s} '.format('[adim]')
sout += '{:>12s} '.format('[adim]')
sout += '{:>12s} '.format('[mN/m]')
sout += '{:>12s} '.format('[kb*T]')
sout += '{:>12s} '.format('[1e-20 J]')
sout += '\n'
fout.write(sout)

for time in sorted(kaprops.keys()):
    area_avg, area_std, mthk_avg, mthk_std, area_rate, mthk_rate, karea = kaprops[time]
    kad2  = 0.1 * karea * mthk_avg * mthk_avg
    sout  = '{0:>12.2f} '.format(time)
    sout += '{0:>12.5f} '.format(area_avg)
    sout += '{0:>12.5f} '.format(area_std)
    sout += '{0:>12.5f} '.format(mthk_avg)
    sout += '{0:>12.5f} '.format(mthk_std)
    sout += '{0:>12.5f} '.format(area_rate)
    sout += '{0:>12.5f} '.format(mthk_rate)
    sout += '{0:>12.5f} '.format(karea)
    sout += '{0:>12.5f} '.format(kad2/kbt2J)
    sout += '{0:>12.5f} '.format(kad2)
    sout += '\n'
    fout.write(sout)

#area_avg, area_std, mthk_avg, mthk_std, area_rate, mthk_rate, karea = kaprops_avg
#sout  = '{0:>12s} '.format("AVG")
#sout += '{0:>12.5f} '.format(area_avg)
#sout += '{0:>12.5f} '.format(area_std)
#sout += '{0:>12.5f} '.format(mthk_avg)
#sout += '{0:>12.5f} '.format(mthk_std)
#sout += '{0:>12.5f} '.format(area_rate)
#sout += '{0:>12.5f} '.format(mthk_rate)
#sout += '{0:>12.5f} '.format(karea)
#sout += '\n'
#fout.write(sout)

#area_avg, area_std, mthk_avg, mthk_std, area_rate, mthk_rate, karea = kaprops_std
#sout  = '{0:>12s} '.format("STD")
#sout += '{0:>12.5f} '.format(area_avg)
#sout += '{0:>12.5f} '.format(area_std)
#sout += '{0:>12.5f} '.format(mthk_avg)
#sout += '{0:>12.5f} '.format(mthk_std)
#sout += '{0:>12.5f} '.format(area_rate)
#sout += '{0:>12.5f} '.format(mthk_rate)
#sout += '{0:>12.5f} '.format(karea)
#sout += '\n'
#fout.write(sout)

fout.close()



