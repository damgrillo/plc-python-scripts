import os
import re
import sys
import math
import string
import random
import numpy as np
from operator import itemgetter
from itertools import izip
from matplotlib import pyplot as plt, rcParams, ticker
from scipy.optimize import leastsq, curve_fit

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20

###########################################################################################################
################################################## FUNCTIONS ##############################################
###########################################################################################################

# Get areas per chain
def get_achain(strfile, lastns):
    fstr  = open(strfile, 'r')
    fdata = [map(float, line.split()) for line in fstr if not re.match('#|@', line)]
    fdata = np.array(fdata) 
    fstr.close()
    return fdata[-lastns:, 1]

# Get Fourier spectrum
def get_fspectrum(fsfile, nclayer):
    ff = open(fsfile, 'r')
    fs = [map(float, line.split()) for line in ff if not re.match('#|@', line)] 
    fs = np.array(fs)
    fs[:, 1:] *= nclayer
    ff.close()
    return fs[fs[:, 0] > 0]

# Logaritmic Fourier spectrum
def logsq(q, par, kbt, ach, qdeg):
    return np.log(kbt/ach) - np.log(par) + qdeg * np.log(q)

# Calculate kbend from Fourier spectrum
# NOTE: If kbt = 1 ==> [kc] = kbT
def calc_kbend(fsavg, fsstd, kbt, ach, qcut):
    fsa   = fsavg[fsavg[:, 0] < qcut]
    fss   = fsstd[fsavg[:, 0] < qcut]
    q     = fsa[:, 0]
    lsqa  = np.log(fsa[:, 1])
    lsqs  = np.log(fss[:, 1])
    kcfit = curve_fit(lambda q, kc: logsq(q, kc, kbt, ach, -4), q, lsqa, p0=1.0) #, sigma=lsqs, absolute_sigma=True)
    return kcfit[0][0], np.sqrt(kcfit[1][0, 0])

# Calculate kbend from Fourier spectrum
# NOTE: If kbt = 1 ==> [gamma] = kbT/nm^2
def calc_gamma(fsavg, fsstd, kbt, ach, qcut):
    fsa   = fsavg[fsavg[:, 0] > qcut]
    fss   = fsstd[fsavg[:, 0] > qcut]
    q     = fsa[:, 0]
    lsqa  = np.log(fsa[:, 1])
    lsqs  = np.log(fss[:, 1])
    gmfit = curve_fit(lambda q, gm: logsq(q, gm, kbt, ach, -2), q, lsqa, p0=1.0) #, sigma=lsqs, absolute_sigma=True)
    return gmfit[0][0], np.sqrt(gmfit[1][0, 0])

# Plot fitted spectrum
def plot_fitted_fspectrum(pltfile, pltdata, kbt):
    nf = -1
    xlabel = '$q$ $\mathsf{(nm^{-1})}$'
    ylabel = '$S_u(q)$ $\mathsf{(nm^{2})}$'
    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True, squeeze=False)
    for t, ach, fsa, fss, kcfit, gmfit in pltdata:
        nf    += 1
        nr     = nf%2
        nc     = nf/2
        q      = fsa[:, 0]
        sqobs  = fsa[:, 1]
        sqerr  = fss[:, 1]
        sqfm4  = np.exp(logsq(q, kcfit[0], kbt, ach, -4))
        sqfm2  = np.exp(logsq(q, gmfit[0], kbt, ach, -2))
        #finfo  = '$t\;\;$ = ${{{0:0.0f}}}$'.format(t) + ' $\mathsf{ns}$' + '\n'
        #finfo  = '$S_u(q) = (k_{B}T)/(a k_{c} q^4)$' + '\n'
        finfo  = '$\kappa$ = ${{{0:0.1f}}} \pm {{{1:0.1f}}}$'.format(kcfit[0], kcfit[1]) + ' $k_{B}T$'
        trax   = axes[nr, nc].transAxes
        bbdic  = {'edgecolor':'k', 'facecolor':'wheat', 'boxstyle':'square', 'alpha':0.9, 'pad':0.3}
        axes[nr, nc].set_xscale("log")
        axes[nr, nc].set_yscale("log")
        axes[nr, nc].errorbar(q, sqobs, sqerr, color='k', ls='', marker='o', ms=5.0, elinewidth=1.2, capsize=1.8, label='Data') 
        axes[nr, nc].plot(q, sqfm4, color='r', ls='-', marker='', lw=2.0, label='Fit')
        #axes[nr, nc].plot(q, sqfm2, color='g', ls='-', marker='', lw=2.0, label='$q^2$')
        axes[nr, nc].legend(fontsize=18, numpoints=1)
        axes[nr, nc].text(0.06, 0.06, finfo, ha='left', va='bottom', size=22, transform=trax, bbox=bbdic)
        axes[nr, nc].grid(which='major', axis='both')
        axes[nr, nc].grid(which='minor', axis='x')
        axes[nr, nc].margins(0.03)
    axes[-1, 0].set_xlim(0.22, 14)
    axes[-1, 0].set_ylim(0.3e-4, 8e0)
    axes[-1, 0].set_xlabel(xlabel, size=23, labelpad=12)
    axes[ 0, 0].set_ylabel(ylabel, size=23, labelpad=15)
    #axes[-1, 1].set_xlabel(xlabel, size=23, labelpad=12)
    #axes[ 1, 0].set_ylabel(ylabel, size=23, labelpad=15)
    #axes[ 2, 0].set_ylabel(ylabel, size=23, labelpad=15)
    fig.set_dpi(200)
    fig.set_size_inches(9, 7)
    fig.tight_layout(h_pad=0.0, w_pad=0.0)
    fig.subplots_adjust()
    fig.savefig(pltfile)
    return


###########################################################################################################
################################################# MAIN PROGRAM ############################################
###########################################################################################################

# Input variables
fsdir   = str(sys.argv[1])
strfile = str(sys.argv[2])
kbfile  = str(sys.argv[3])
twin    = int(sys.argv[4])
ncpoly  = int(sys.argv[5])
pltfile = str(sys.argv[6])
nclayer = ncpoly / 2

# Units conversion
# 1 mN/m     = 10 bar*nm
# 1 bar*nm3  = 1e-2 * 1e-20 J
# 1 kb*T     = 1e-3 * 1.3806 * Temp * 1e-20 J 
# 1 kb*T     = 1e-1 * 1.3806 * Temp * bar*nm^3

# Global parameters
kbt       = 1
qcut      = 1.0
temp      = 300
kbtbarnm3 = 1.3806488 * 1e-1 * temp # Convert kbT in bar*nm^3
kbt2J     = 1.3806488 * 1e-3 * temp # Convert kbT in 1e-20 J

# Get Fourier spectrum and areas per chain
fsfiles = [os.path.join(fsdir, f) for f in sorted(os.listdir(fsdir)) if re.search('fspectra', f)]
fspecs  = [get_fspectrum(f, nclayer) for f in fsfiles]
times   = [int(re.search('job-([0-9]+)', f).group(1)) for f in fsfiles]
achain  = get_achain(strfile, len(times))

# Average spectrum and areas for each time window
tstart   = times[0]
avgtimes = np.arange(0, len(fspecs), twin)
avgareas = [np.mean(achain[t:t+twin]) for t in avgtimes]
avgspecs = [np.mean(fspecs[t:t+twin], axis=0) for t in avgtimes]
stdspecs = [np.std(fspecs[t:t+twin], axis=0) for t in avgtimes]

# Calculate kbend from Fourier spectrum
count   = 0
kbends  = []
pltdata = []
samples = sorted(set(random.sample(avgtimes, 1)))
#samples = [150.0, 424.0, 430.0, 1151.0, 1486.0, 1925.0]
#samples = [430.0, 1151.0, 1486.0, 1925.0]
for t, ach, fsa, fss in izip(avgtimes, avgareas, avgspecs, stdspecs):
    kcfit = calc_kbend(fsa, fss, kbt, ach, qcut) 
    gmfit = calc_gamma(fsa, fss, kbt, ach, qcut)
    if t in samples:
        pltdata.append((tstart+t+twin, ach, fsa, fss, kcfit, gmfit))
    kbends.append((t, kcfit)) 

# Plot fitted spectrum
plot_fitted_fspectrum(pltfile, pltdata, kbt)


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write kbends
fout = open(kbfile, 'w')
sout  = '#{:>11s} '.format('Time')
sout += '{:>12s} '.format('kbend_avg')
sout += '{:>12s} '.format('kbend_std')
sout += '{:>12s} '.format('kbend_avg')
sout += '{:>12s} '.format('kbend_std')
sout += '\n'
sout += '#{:>11s} '.format('[ns]')
sout += '{:>12s} '.format('[kb*T]')
sout += '{:>12s} '.format('[kb*T]')
sout += '{:>12s} '.format('[1e-20 J]')
sout += '{:>12s} '.format('[1e-20 J]')
sout += '\n'
fout.write(sout)

for t, kcfit in kbends:
    sout  = '{0:>12.2f} '.format(t)
    sout += '{0:>12.3f} '.format(kcfit[0])
    sout += '{0:>12.3f} '.format(kcfit[1])
    sout += '{0:>12.3f} '.format(kcfit[0]*kbt2J)
    sout += '{0:>12.3f} '.format(kcfit[1]*kbt2J)
    sout += '\n'
    fout.write(sout)

fout.close()


###########################################################################################################
################################################### BACKUP ################################################
###########################################################################################################

## NOTE: If kbt = 1 ==> [kbend] = kbT, [gamma] = kbT/nm

## Logaritmic Fourier spectrum, low-q range
#def logsq(par, q, kbt, ach, qdeg):
#    return np.log(kbt/ach) - np.log(par) + qdeg * np.log(q)

## Logaritmic Fourier spectrum difference
#def logsq_diff(par, q, sqobs, kbt, area, qdeg):
#    return logsq(par[0], q, kbt, ach, qdeg) - np.log(sqobs)

## Calculate kbend and gamma from Fourier spectrum
## NOTE: If kbt = 1 ==> [kc] = kbT, [gamma] = kbT/nm^2
#def calc_kbend_gamma(fspec, kbt, ach, qcut):
#    fsqm4 = fspec[fspec[:, 0] < qcut]
#    fsqm2 = fspec[fspec[:, 0] > qcut]
#    kc    = leastsq(logsq_diff, (1.0), args=(fsqm4[:, 0], fsqm4[:, 1], kbt, ach, -4))[0][0]
#    gamma = leastsq(logsq_diff, (1.0), args=(fsqm2[:, 0], fsqm2[:, 1], kbt, ach, -2))[0][0]
#    return kc, gamma

## Get areas
#def get_areas(strfile, nclayer, lastns):
#    fstr  = open(strfile, 'r')
#    fdata = [map(float, line.split()) for line in fstr if not re.match('#|@', line)]
#    fdata = np.array(fdata) 
#    fstr.close()
#    return fdata[-lastns:, 1] * nclayer

## Get Fourier spectrum
#def get_fspectrum(fsfile):
#    ff = open(fsfile, 'r')
#    fs = [map(float, line.split()) for line in ff if not re.match('#|@', line)] 
#    fs = np.array(fs)
#    ff.close()
#    return fs[fs[:, 0] > 0]

## Calculate kbend from Fourier spectrum
#def calc_kbend(fspec, kbt, area, show_plot=False):

#    # Fit parameters
#    q     = fspec[:, 0]
#    h2q   = fspec[:, 1] * area
#    t2q   = fspec[:, 2] * area
#    pars0 = (1.0, 1.0, 1.0, 0.0, 0.0)
#    fpars = leastsq(log_ht2diff, pars0, args=(q, h2q, t2q, kbt))[0]
#    print fpars

#    # Plot fitted spectrum
#    if show_plot:
#        h2f = np.exp(log_h2q(fpars, q, kbt))
#        t2f = np.exp(log_t2q(fpars, q, kbt))
#        fig, axes = plt.subplots(nrows=2, ncols=1, squeeze=True)
#        axes[0].loglog(q, h2q, color='k', ls='', marker='o', ms=4.0, label='h2q_obs') 
#        axes[0].loglog(q, h2f, color='r', ls='-', marker='', lw=2.0, label='h2q_fit')
#        axes[0].margins()
#        axes[0].legend()
#        axes[0].grid()
#        axes[1].loglog(q, t2q, color='k', ls='', marker='o', ms=4.0, label='t2q_obs') 
#        axes[1].loglog(q, t2f, color='r', ls='-', marker='', lw=2.0, label='t2q_fit')
#        axes[1].margins()
#        axes[1].legend()
#        axes[1].grid()
#        plt.show()    
#    return fpars[0]

## Logaritmic functional form for height Fourier amplitudes
## Reference: Brannigan and Brown, Biophysical Journal 90(5) 1501-1520
#def log_h2q(q, kc, kl, gl, zt0, kat02, kbt):
#    return np.log(kbt/(kc*q**4) + 0.5*kbt/(kl + gl*q**2))

## Logaritmic functional form for Fourier thickness amplitudes
## Reference: Brannigan and Brown, Biophysical Journal 90(5) 1501-1520
#def log_t2q(q, kc, kl, gl, zt0, kat02, kbt):
#    return np.log(kbt/(kc*q**4 - 4*kc*zt0*q**2 + kat02) + 0.5*kbt/(kl + gl*q**2))

#def log_ht2q(q, kc, kl, gl, zt0, kat02, kbt):
#    return log_h2q(q, kc, kl, gl, zt0, kat02, kbt)
#    #return np.concatenate((log_h2q(q, kc, kl, gl, zt0, kat02, kbt), log_t2q(q, kc, kl, gl, zt0, kat02, kbt)))

## Calculate parameters from Fourier spectrum, Brannigan and Brown
## NOTE: If kbt = 1 ==> [kc] = kbT
#def calc_fspars(fsavg, fsstd, kbt, ach, qcut):
#    fsa   = fsavg
#    fss   = fsstd
#    q     = fsa[:, 0]
#    lshqa = np.log(fsa[:, 1]*ach)
#    lstqa = np.log(fsa[:, 2]*ach)
#    lsqa  = lshqa #np.concatenate((lshqa, lstqa))
#    pars0 = (1.0, 1.0, 1.0, 1.0, 1.0)
#    pfit = curve_fit(lambda q, kc, kl, gl, zt0, kat02: log_ht2q(q, kc, kl, gl, zt0, kat02, kbt), q, lsqa, p0=pars0)
#    return pfit

## Plot fitted spectrum, Brannigan and Brown
#def plot_fitted_fspectrum(pltfile, pltdata, kbt):
#    nf = -1
#    xlabel = '$q$ $\mathsf{(nm^{-1})}$'
#    ylabel = '$S_u(q)$ $\mathsf{(nm^{2})}$'
#    fig, axes = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True, squeeze=False)
#    for t, ach, fsa, fss, fspars in pltdata:
#        nf    += 1
#        nr     = nf%2
#        nc     = nf/2
#        q      = fsa[:, 0]
#        sqobs  = fsa[:, 1]
#        sqerr  = fss[:, 1]
#        sqfit  = np.exp(log_h2q(q, fspars[0][0], fspars[0][1], fspars[0][2], fspars[0][3], fspars[0][4], kbt))
#        finfo  = '$k_c$ = ${{{0:0.1f}}} \pm {{{1:0.1f}}}$'.format(fspars[0][0], fspars[1][0, 0]) + ' $k_{B}T$'
#        trax   = axes[nr, nc].transAxes
#        bbdic  = {'edgecolor':'k', 'facecolor':'wheat', 'boxstyle':'square', 'alpha':0.9, 'pad':0.3}
#        axes[nr, nc].set_xscale("log")
#        axes[nr, nc].set_yscale("log")
#        axes[nr, nc].errorbar(q, sqobs, sqerr, color='k', ls='', marker='o', ms=5.0, elinewidth=1.2, capsize=1.8, label='Data') 
#        axes[nr, nc].plot(q, sqfit, color='r', ls='-', marker='', lw=2.0, label='Fit')
#        axes[nr, nc].legend(fontsize=18, numpoints=1)
#        axes[nr, nc].text(0.06, 0.06, finfo, ha='left', va='bottom', size=22, transform=trax, bbox=bbdic)
#        axes[nr, nc].grid(which='major', axis='both')
#        axes[nr, nc].grid(which='minor', axis='x')
#        axes[nr, nc].margins(0.03)
#    axes[-1, 0].set_xlim(0.22, 14)
#    axes[-1, 0].set_ylim(0.3e-4, 8e0)
#    axes[-1, 0].set_xlabel(xlabel, size=23, labelpad=12)
#    axes[ 0, 0].set_ylabel(ylabel, size=23, labelpad=15)
#    #axes[-1, 1].set_xlabel(xlabel, size=23, labelpad=12)
#    #axes[ 1, 0].set_ylabel(ylabel, size=23, labelpad=15)
#    #axes[ 2, 0].set_ylabel(ylabel, size=23, labelpad=15)
#    fig.set_dpi(200)
#    fig.set_size_inches(7, 7)
#    fig.tight_layout(h_pad=0.0, w_pad=0.0)
#    fig.subplots_adjust()
#    fig.savefig(pltfile)
#    return


