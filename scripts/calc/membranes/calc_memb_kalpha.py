import os
import re
import sys
import math
import string
import numpy as np
from operator import itemgetter
from itertools import izip, takewhile
from matplotlib import pyplot as plt, rcParams

rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
#rcParams['mathtext.default'] = 'regular'


###########################################################################################################
############################################## MAIN PROGRAM ###############################################
###########################################################################################################

# Show usage
if len(sys.argv) < 3:
    sys.exit('Usage: calc_memb_kalpha.py [kalpha_file] [mech_dir] [avgwin]\n')

# Input variables
kalpfile = str(sys.argv[1])
mechdir  = str(sys.argv[2])
avgwin   = str(sys.argv[3])

# Get mechanic properties files
mechflist    = [os.path.join(mechdir, f) for f in os.listdir(mechdir)]
mechsdirs    = [sd for sd in mechflist if os.path.isdir(sd)]

# Get ka*d2 and kbend files for each system
kad2files  = {}
kad2lfiles = {}
kbendfiles = {}
for sdir in mechsdirs:
    sname    = sdir.strip('/').split('/')[-1]
    kafname  = 'karea-avgwin-' + avgwin + 'ns.txt'
    kbfname  = 'kbend-large.txt'
    kad2files[sname]  = os.path.join(sdir, kafname)  
    kbendfiles[sname] = os.path.join(re.sub('-newrun|-extend', '', sdir), kbfname)  

# Headers and units info
kad2_hinfo  = ('Ka*d2', '[kb*T]')
kbavg_hinfo = ('kbend_avg', '[kb*T]')
kbstd_hinfo = ('kbend_std', '[kb*T]')

# Get kalpha for each system
kalpha  = {}
kalphal = {}
for sname in kad2files:

    # Set system label
    if re.search('PRIN', sname) and re.search('PRIP', sname):
        slabel  = 'N' + re.search("PRIN([0-9]+)", sname).group(1) + '-'
        slabel += 'P' + re.search("PRIP([0-9]+)", sname).group(1)
    elif re.search('PRIN', sname):
        slabel += 'N' + re.search("PRIN([0-9]+)", sname).group(1)
    elif re.search('PRIP', sname):
        slabel += 'P' + re.search("PRIP([0-9]+)", sname).group(1)
    else:
        slabel = 'Neat'
    if re.search('newrun', sname):
        slabel += '-NEW'   
    if re.search('extend', sname):
        slabel += '-EXT' 

    # Get ka*d2 data
    fa     = open(kad2files[sname], 'r')
    fread  = [line.strip() for line in fa]
    fhead  = fread[0].lstrip('#').split()
    funit  = fread[1].lstrip('#').split()
    fdata  = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    ncol   = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == kad2_hinfo]
    fdcol  = fdata[:, ncol]
    kastat = (np.mean(fdcol), np.std(fdcol))    

    # Get kbend data
    fb     = open(kbendfiles[sname], 'r')
    fread  = [line.strip() for line in fb]
    fhead  = fread[0].lstrip('#').split()
    funit  = fread[1].lstrip('#').split()
    fdata  = np.array([map(float, line.split()) for line in fread if not re.match('#|AVG|STD', line)])
    nacol  = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == kbavg_hinfo][0]
    nscol  = [nc for nc in np.arange(len(fhead)) if (fhead[nc], funit[nc]) == kbstd_hinfo][0]
    kbavg  = np.mean(fdata[:, nacol])
    kbstd  = np.mean(fdata[:, nscol])
    kbstat = (kbavg, kbstd)    
     
    # Calculate kalpha
    kalp_avg       = kastat[0]/kbstat[0]
    kalp_std       = np.sqrt((kastat[1]/kbstat[0])**2 + (kastat[0]*kbstat[1]/kbstat[0]**2)**2)
    kalpha[slabel] = (int(kalp_avg), int(kalp_std))


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

# Write kalpha
fout = open(kalpfile, 'w')
sout  = '#{:15s}'.format('system')
sout += '{:>10s}'.format('orig')
sout += '\n'
fout.write(sout)

syssort = ['Neat'] + sorted([sl for sl in kalpha.keys() if sl != 'Neat'])
for sl in syssort:
    sout  = ' {0:15s}'.format(sl)
    sout += '{0:d}/{1:d}'.format(*kalpha[sl]).rjust(10)
    sout += '\n'
    fout.write(sout)
fout.close()


