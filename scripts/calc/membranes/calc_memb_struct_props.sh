#!/bin/bash

# Global vars
SCRIPTS_DIR=$(cd $(dirname $0) && pwd)

# Set prefix name
POLYNAME='PBD22-PEO14'
INPDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/local/memb/runs/inp # Input GRO files containing the systems coordinates
RAWDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/local/memb/runs/res # Path to store the raw data from the simulations (trajectories, density and pressure profiles, etc.)
RESDIR=/Users/damiangrillo/Documents/PhD/Repos/plc-python-scripts/data/membranes # Path to store the results of the different properties calculated for the simulations
LASTNS=3000 # Use last nanoseconds of trajectory

# Membrane structural properties
for PFX in $(ls $RAWDIR/densprofs | grep -v 'large'); do
    echo "Calculating $PFX"  

    # Set files
    GRO=$INPDIR/$PFX.gro
    DENSDIR=$RAWDIR/densprofs/$PFX # Density profiles for current system (raw data)
    APCHDIR=$RAWDIR/apchainprofs/$PFX # Area per chain profiles for current system (raw data)

    # Calculate membrane structural properties
    if [ -d $DENSDIR ]; then
        NCPOLY=$(cat $GRO | grep '[0-9]\+PLY'  | awk '{print $1}' | uniq | wc -l)
        NMPRIP=$(cat $GRO | grep '[0-9]\+PRIP' | awk '{print $1}' | uniq | wc -l)
        NMWAT=$(cat $GRO  | grep '[0-9]\+PW'   | awk '{print $1}' | uniq | wc -l)
        NBCHAIN=$(cat $GRO | grep '^\s*1PLY' | awk '{print $1}' | wc -l)
        python $SCRIPTS_DIR/calc_memb_struct_props.py \
               $DENSDIR $APCHDIR $RESDIR $NCPOLY $NBCHAIN $NMPRIP $NMWAT $PFX $LASTNS
    fi

done


