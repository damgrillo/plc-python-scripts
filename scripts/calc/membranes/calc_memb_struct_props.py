import os
import re
import sys
import math
import string
import numpy as np
from itertools import izip
from operator import itemgetter

###############################################################################################
########################################## FUNCTIONS ##########################################
###############################################################################################

# Get parameters for equation of line y = m * x + b
def get_line_params(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    m = (y2 - y1) / (x2 - x1)
    b = y2 - m * x2
    return (m, b)

# Solve equation y = m * x + b for a given y
def solve_line(y, m, b):
    return (y - b) / m

# Get intersections at fraction height
def get_intersections(dprof, col, frac):    

    # Calculate half-height density value
    cprof     = dprof[:, [0, col]]
    max_dens  = np.amax(cprof[:, 1])
    high_dens = [dns for dns in cprof[:, 1] if dns > 0.9 * max_dens]
    frac_dens = frac * sum(high_dens) / len(high_dens)

    # Calculate center of the profile
    pos    = cprof[:, 0]
    dens   = cprof[:, 1]
    center = np.trapz(y=dens*pos, x=pos) / np.trapz(y=dens, x=pos) 

    # Calculate zcoord on the left side corresponding to the half-height density 
    lwleft = max((p for p in cprof if (p[0] < center and p[1] < frac_dens)), key=itemgetter(1))
    upleft = min((p for p in cprof if (p[0] < center and p[1] > frac_dens)), key=itemgetter(1))
    ml, bl = get_line_params(lwleft, upleft)
    fl     = solve_line(frac_dens, ml, bl)

    # Calculate zcoord on the right side corresponding to the half-height density 
    lwright = max((p for p in cprof if (p[0] > center and p[1] < frac_dens)), key=itemgetter(1))
    upright = min((p for p in cprof if (p[0] > center and p[1] > frac_dens)), key=itemgetter(1))
    mr, br  = get_line_params(lwright, upright)
    fr      = solve_line(frac_dens, mr, br)
    return fl, fr

# Get membrane area per chain using density, box area and slice thickness
def get_memb_area(dprof, col, nbtpoly):
    cprof = dprof[:, [0, col]]
    zsl   = cprof[1, 0] - cprof[0, 0]
    area  = nbtpoly / zsl / sum(cprof[:, 1])
    return area 

# Get mean area per chain using are profile
def get_mean_apchain(aprof, col):
    zpos    = aprof[:, 0]
    ncarea  = np.divide(1.0, aprof[:, col], out=np.zeros(aprof.shape[0]), where=(aprof[:, col] > 0))
    hl, hr  = get_intersections(np.column_stack((zpos, ncarea)), 1, 0.5) 
    apchain = aprof[(hl <= aprof[:, 0]) & (aprof[:, 0] <= hr)]
    apmean  = np.mean(apchain[:, col])  
    return apmean 
   
# Get membrane thickness
def get_memb_thick(dprof, col):    
    hl, hr = get_intersections(dprof, col, 0.5) 
    return hr-hl

# Get number of beads from number density profile
def get_nbeads(zp, dp, area):
    return np.trapz(y=dp, x=zp) * area    

# Get number of molecules in lower side
def get_nmol_lower(zp, dp, area, nbm):
    zlow = zp[zp < 0.0]
    dlow = dp[zp < 0.0]
    nlow = get_nbeads(zlow, dlow, area)/nbm
    return nlow

# Get number of molecules in upper side
def get_nmol_upper(zp, dp, area, nbm):
    zupp = zp[zp > 0.0]
    dupp = dp[zp > 0.0]
    nupp = get_nbeads(zupp, dupp, area)/nbm
    return nupp

# Get number of encapsulated pPLC molecules in lower leaflet
def get_pplc_lower(zp, dp, area):
    nbm  = 5
    zcut = zp[(-6.0 < zp) & (zp < -3.0)]
    dcut = dp[(-6.0 < zp) & (zp < -3.0)]
    zint = zcut[dcut.argmin()]
    zenc = zp[(zint < zp) & (zp < 0.0)]
    denc = dp[(zint < zp) & (zp < 0.0)]
    nenc = get_nbeads(zenc, denc, area)/nbm
    return nenc

# Get number of encapsulated pPLC molecules in upper leaflet
def get_pplc_upper(zp, dp, area):
    nbm  = 5
    zcut = zp[(3.0 < zp) & (zp < 6.0)]
    dcut = dp[(3.0 < zp) & (zp < 6.0)]
    zint = zcut[dcut.argmin()]
    zenc = zp[(0.0 < zp) & (zp < zint)]
    denc = dp[(0.0 < zp) & (zp < zint)]
    nenc = get_nbeads(zenc, denc, area)/nbm
    return nenc


###########################################################################################################
################################################ MAIN PROGRAM #############################################
###########################################################################################################

# Input variables
densdir = str(sys.argv[1])
areadir = str(sys.argv[2])
resdir  = str(sys.argv[3])
ncpoly  = int(sys.argv[4])
nbchain = int(sys.argv[5])
nmprip  = int(sys.argv[6])
nmwat   = int(sys.argv[7])
pfxname = str(sys.argv[8])
lastns  = int(sys.argv[9])
nbtpoly = ncpoly * nbchain
nclay   = ncpoly/2

# Number of beads per molecule
nbmol = {'PLY':36, 'PEO':14, 'PBD':22, 'PRIN':5, 'PRIP':5, 'PW':1}

# Get mass and number density files
massfiles = sorted([os.path.join(densdir, f) for f in os.listdir(densdir) if f.endswith("mass.xvg")])
numbfiles = sorted([os.path.join(densdir, f) for f in os.listdir(densdir) if f.endswith("numb.xvg")])
areafiles = sorted([os.path.join(areadir, f) for f in os.listdir(areadir) if f.endswith("apchain.xvg")])

# Membrane properties, densities and per bead for each layer
time      = 0.0
membprops = []
densities = []
npripdata  = {'LOWER':[], 'UPPER':[]}
for mtxt, ntxt, atxt in izip(massfiles, numbfiles, areafiles):  

    # Read mass density
    finp   = open(mtxt, 'r')
    fread  = [line.strip() for line in finp] 
    fdata  = [line.split() for line in fread if not re.match('#|@', line)] 
    lgrps  = [re.search('\"(.*)\"', line).group(1) for line in fread if re.match('@\s+s[0-9]+', line)]
    mgrps  = {grp:num for num, grp in enumerate(lgrps, 1)}
    dprofm = np.array([map(float, d) for d in fdata])
    densities.append(dprofm)
    finp.close()

    # Read number density
    finp   = open(ntxt, 'r')
    fread  = [line.strip() for line in finp] 
    fdata  = [line.split() for line in fread if not re.match('#|@', line)] 
    lgrps  = [re.search('\"(.*)\"', line).group(1) for line in fread if re.match('@\s+s[0-9]+', line)]
    ngrps  = {grp:num for num, grp in enumerate(lgrps, 1)}
    dprofn = np.array([map(float, d) for d in fdata])
    finp.close()

    # Read number density
    finp   = open(atxt, 'r')
    fread  = [line.strip() for line in finp] 
    fdata  = [line.split() for line in fread if not re.match('#|@', line)] 
    lgrps  = [re.search('\"(.*)\"', line).group(1) for line in fread if re.match('@\s+s[0-9]+', line)]
    agrps  = {grp:num for num, grp in enumerate(lgrps, 1)}
    aprof  = np.array([map(float, d) for d in fdata])
    finp.close()

    # Get area per chain, membrane thickness and volume
    time  += 1.0
    area   = get_memb_area(dprofn, ngrps['PLY'], nbtpoly)
    #achp   = get_mean_apchain(aprof, agrps['Total'])
    thk    = get_memb_thick(dprofm, ngrps['PBD'])
    vol    = area * thk
    membprops.append((time, area, thk, vol))

    # Calculate number of encapsulated PRIP
    if 'PRIP' in ngrps:
        nwatlow  = get_nmol_lower(dprofn[:, 0], dprofn[:, ngrps['PW']], area, nbmol['PW'])
        nwatupp  = get_nmol_upper(dprofn[:, 0], dprofn[:, ngrps['PW']], area, nbmol['PW'])
        npriplow = get_nmol_lower(dprofn[:, 0], dprofn[:, ngrps['PRIP']], area, nbmol['PRIP'])
        npripupp = get_nmol_upper(dprofn[:, 0], dprofn[:, ngrps['PRIP']], area, nbmol['PRIP'])
        npenclow = get_pplc_lower(dprofn[:, 0], dprofn[:, ngrps['PRIP']], area)
        npencupp = get_pplc_upper(dprofn[:, 0], dprofn[:, ngrps['PRIP']], area)
        npripdata['LOWER'].append((time, area, nwatlow, npriplow, npenclow))
        npripdata['UPPER'].append((time, area, nwatupp, npripupp, npencupp))

# Recalculate lastns when lastns > totalns
totalns = int(membprops[-1][0])
lastns  = (totalns if totalns < lastns else lastns)

# Calculate average densities for last nanosecs
count   = 0
start   = totalns - lastns
avgdens = np.copy(densities[start])
for dens in densities[start+1:]:
    avgdens += dens
    count   += 1
avgdens /= count


###########################################################################################################
################################################ OUTPUT RESULTS ###########################################
###########################################################################################################

write_sprops = True
write_dprofs = True

# Write membrane area, thickness and volume
structdir = resdir + '/structprops'
if not os.path.isdir(structdir):
    os.makedirs(structdir)
outfile = structdir + '/' + pfxname + '-structprops.txt'

if write_sprops:

    fout = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('MembArea')
    sout += '{:>12s} '.format('MembThick')
    sout += '{:>12s} '.format('MembVol')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[nm^2]')
    sout += '{:>12s} '.format('[nm]')
    sout += '{:>12s} '.format('[nm^3]')
    sout += '\n'
    fout.write(sout)

    for time, area, thk, vol in membprops:
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.3f} '.format(area)
        sout += '{0:>12.3f} '.format(thk)
        sout += '{0:>12.3f} '.format(vol)
        sout += '\n'
        fout.write(sout)
    fout.close()

# Write number of encapsulated PRIP molecules in lower leaflet
T = 300
R = 8.314472* 1e-3
pdata = npripdata['LOWER']
if len(pdata) > 0:
    outfile = structdir + '/' + pfxname + '-encapsulated-PRIP-lower.txt'
    fout  = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('Nt')
    sout += '{:>12s} '.format('Ne')
    sout += '{:>12s} '.format('Nw')
    sout += '{:>12s} '.format('PWt')
    sout += '{:>12s} '.format('Area')
    sout += '{:>12s} '.format('ne=Ne/Area')
    sout += '{:>12s} '.format('ct=Nt/PWt')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[nm^2]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '{:>12s} '.format('[adim]')
    sout += '\n'
    fout.write(sout)

    for n in range(len(pdata)):
        time  = pdata[n][0]
        area  = pdata[n][1]
        nwtot = pdata[n][2]
        nptot = pdata[n][3]
        npenc = pdata[n][4]
        npwat = nptot-npenc
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.2f} '.format(nptot)
        sout += '{0:>12.2f} '.format(npenc)
        sout += '{0:>12.2f} '.format(npwat)
        sout += '{0:>12.2f} '.format(nwtot)
        sout += '{0:>12.4f} '.format(area)
        sout += '{0:>12.4f} '.format(npenc/area)
        sout += '{0:>12.6f} '.format(1.0*nptot/nwtot)   
        sout += '\n'
        fout.write(sout)
    fout.close()

# Write number of encapsulated PRIP molecules in upper leaflet
T = 300
R = 8.314472* 1e-3
pdata = npripdata['UPPER']
if len(pdata) > 0:
    outfile = structdir + '/' + pfxname + '-encapsulated-PRIP-upper.txt'
    fout  = open(outfile, 'w')
    sout  = '#{:>11s} '.format('Time')
    sout += '{:>12s} '.format('Nt')
    sout += '{:>12s} '.format('Ne')
    sout += '{:>12s} '.format('Nw')
    sout += '{:>12s} '.format('PWt')
    sout += '{:>12s} '.format('Area')
    sout += '{:>12s} '.format('ne=Ne/Area')
    sout += '{:>12s} '.format('ct=Nt/PWt')
    sout += '\n'
    sout += '#{:>11s} '.format('[ns]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[adim]')
    sout += '{:>12s} '.format('[nm^2]')
    sout += '{:>12s} '.format('[nm^-2]')
    sout += '{:>12s} '.format('[adim]')
    sout += '\n'
    fout.write(sout)

    for n in range(len(pdata)):
        time  = pdata[n][0]
        area  = pdata[n][1]
        nwtot = pdata[n][2]
        nptot = pdata[n][3]
        npenc = pdata[n][4]
        npwat = nptot-npenc
        sout  = '{0:>12.2f} '.format(time)
        sout += '{0:>12.2f} '.format(nptot)
        sout += '{0:>12.2f} '.format(npenc)
        sout += '{0:>12.2f} '.format(npwat)
        sout += '{0:>12.2f} '.format(nwtot)
        sout += '{0:>12.4f} '.format(area)
        sout += '{0:>12.4f} '.format(npenc/area)
        sout += '{0:>12.6f} '.format(1.0*nptot/nwtot)   
        sout += '\n'
        fout.write(sout)
    fout.close()

## Write difference of encapsulated PRIP molecules between upper and lower leaflets
#T = 300
#R = 8.314472* 1e-3
#uppdata = npripdata['UPPER']
#lowdata = npripdata['LOWER']
#if len(uppdata) > 0:
#    outfile = structdir + '/' + pfxname + '-encapsulated-PRIP-diff.txt'
#    fout  = open(outfile, 'w')
#    sout  = '#{:>11s} '.format('Time')
#    sout += '{:>12s} '.format('Ne')
#    sout += '{:>12s} '.format('ne=Ne/Area')
#    sout += '\n'
#    sout += '#{:>11s} '.format('[ns]')
#    sout += '{:>12s} '.format('[adim]')
#    sout += '{:>12s} '.format('[nm^2]')
#    sout += '\n'
#    fout.write(sout)

#    for n in range(len(uppdata)):
#        time    = uppdata[n][0]
#        upparea = uppdata[n][1]
#        uppenc  = uppdata[n][4]
#        lowarea = lowdata[n][1]
#        lowenc  = lowdata[n][4]
#        sout  = '{0:>12.2f} '.format(time)
#        sout += '{0:>12.1f} '.format(uppenc-lowenc)
#        sout += '{0:>12.3f} '.format(uppenc/upparea - lowenc/lowarea)
#        sout += '\n'
#        fout.write(sout)
#    fout.close()

# Write density profiles
outfile = structdir + '/' + pfxname + '-densprofs-last-' + str(lastns) + 'ns.txt'

if write_dprofs:

    fout = open(outfile, 'w')
    grpsort = sorted(mgrps.items(), key=lambda x: x[1])
    sout = '#{:>11s} '.format('Z')
    for grp, num in grpsort:
        sout += '{:>12s} '.format(grp)
    sout += '\n'
    sout += '#{:>11s} '.format('[nm]')
    for grp, num in grpsort:
        sout += '{:>12s} '.format('[kg/m^3]')
    sout += '\n'
    fout.write(sout)

    for row in np.arange(avgdens.shape[0]):
        sout  = '{0:>12.4f} '.format(avgdens[row, 0])
        for grp, num in grpsort:
            sout += '{0:>12.4f} '.format(avgdens[row, num])
        sout += '\n'
        fout.write(sout)
    fout.close()

