#!/bin/bash

# Global vars
SCRIPTS_DIR=$(cd $(dirname $0) && pwd)

# Set prefix name
POLYNAME='PBD22-PEO14'
INPDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/local/memb/runs/inp # Input GRO files containing the systems coordinates
RAWDIR=/Users/damiangrillo/Documents/PhD/Workdir/runs/PLC/local/memb/runs/res # Path to store the raw data from the simulations (trajectories, density and pressure profiles, etc.)
RESDIR=/Users/damiangrillo/Documents/PhD/Repos/plc-python-scripts/data/membranes # Path to store the results of the different properties calculated for the simulations
STRDIR=$RESDIR/structprops # Structural properties path
MECHDIR=$RESDIR/mechprops # Mechanical properties path
ELASTFILE=$MECHDIR/PRILO-elastprops-avgwin-${AVGWIN}ns.txt # Elastic properties file
PMETHOD=IKcont
AVGWIN=500
LASTNS=3000
mkdir -p $MECHDIR

# Membrane mechanic properties
for PFX in $(ls $RAWDIR/densprofs | grep -v 'large' | grep -v 'txt'); do
    GRO=$INPDIR/$PFX.gro
    # GROL=$INPDIR/$PFX-large.gro
    NCPOLY=$(cat $GRO | grep 'PLY' | awk '{print $1}' | sort -V | uniq | wc -l)
    # NCPOLYL=$(cat $GROL | grep 'PLY' | awk '{print $1}' | sort -V | uniq | wc -l)
    DENSDIR=$RAWDIR/densprofs/$PFX # Density profiles for current system (raw data)
    PRESDIR=$RAWDIR/presprofs/$PFX # Pressure profiles for current system (raw data)
    # FSPECDIR=$RAWDIR/fspectra/$PFX-large # Fourier spectra for current system (raw data)
    STRFILE=$STRDIR/$PFX-structprops.txt # Structural properties file for current system
    # STRFILEL=$STRDIR/$PFX-large-structprops.txt # Structural properties file for current system (large)
    KBENDFILE=$MECHDIR/$PFX/kbend-large.txt # Bending modulus file for current system
    mkdir -p $MECHDIR/$PFX

    # # Calculate kbend
    # echo "Calculating kbend for $PFX ..."
    # TWIN=2000
    # PLOTSAMPLE=$MECHDIR/$PFX-large-kbend-fitsample-new.pdf
    # if [ $(echo $PFX | grep -o 'extend\|newrun' | wc -w) -eq 0 ]; then
    #     /usr/bin/python $SCRIPTS_DIR/calc_memb_kbend.py $FSPECDIR $STRFILEL $KBENDFILE $TWIN $NCPOLYL $PLOTSAMPLE
    # fi
    cp $RAWDIR/mechprops/$PFX/kbend-large.txt $KBENDFILE

    # Calculate karea, gamma, tau and kgauss using a time window of 500ns
    echo "Calculating karea and presprops for $PFX ..."
    TWIN=500
    python $SCRIPTS_DIR/calc_memb_kareaprops.py $RESDIR $STRFILE $PFX $TWIN $LASTNS $NCPOLY 'orig'
    python $SCRIPTS_DIR/calc_memb_presprops.py $RESDIR $STRDIR $PRESDIR $PFX $TWIN $PMETHOD

done

# Calculate kalpha 
echo "Calculating kalpha ..."
AVGWIN=500
KALPFILE=$MECHDIR/PRILO-kalpha-avgwin-${AVGWIN}ns.txt
python $SCRIPTS_DIR/calc_memb_kalpha.py $KALPFILE $MECHDIR $AVGWIN

# Calculate elastic parameters 
echo "Calculating elastic properties ..."
ELASTFILE=$MECHDIR/PRILO-elastprops-avgwin-${AVGWIN}ns.txt
python $SCRIPTS_DIR/calc_memb_elastprops.py $ELASTFILE $MECHDIR $STRDIR $AVGWIN $LASTNS

